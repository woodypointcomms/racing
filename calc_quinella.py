import configparser
import itertools
from pprint import pprint


def calculate_quinella_bet(prediction, budget, verbose=2, config=None):

    if config is None:

        config = configparser.ConfigParser()
        config.read('racing.ini')

    if prediction.empty:
        return []

    num_runners = len(prediction.index)

    if num_runners > int(config['quinella']['max_runners']):
        return []

    if num_runners < int(config['quinella']['min_runners']):
        return []

    if config['quinella'].getboolean('number_rules'):

        minimum_prob_option = "minimum_prob_%d" % num_runners
        minimum_payout_option = "minimum_payout_%d" % num_runners
        combination_limit_option = "combination_limit_%d" % num_runners
        box_option = "box_%d" % num_runners
        sort_options_option = "sort_options_%d" % num_runners
        sort_reverse_option = "sort_reverse_%d" % num_runners

    else:

        minimum_prob_option = "minimum_prob"
        minimum_payout_option = "minimum_payout"
        combination_limit_option = "combination_limit"
        box_option = "box"
        sort_options_option = "sort_options"
        sort_reverse_option = "sort_reverse"

    combination_limit = int(config['quinella'][combination_limit_option])
    prediction_culled = prediction[(prediction['prob_1'] > float(config['quinella'][minimum_prob_option]))]
    box_runners = config['quinella'].getint(box_option)
    # box_runners = 2

    if len(prediction_culled.index) < box_runners:
        if verbose >= 3:
            print("No %s way quinella bets possible, not %s runners selected!" % (box_runners, box_runners))
        return []

    if len(prediction_culled.index) < combination_limit:
        combination_limit = len(prediction_culled.index)

    bet_options = []

    combinations = list(itertools.combinations(range(0, combination_limit, 1), box_runners))

    for combination in combinations:

        # Get all the combinations in the box
        box_combinations = list(itertools.combinations(range(0, box_runners, 1), 2))

        box_combo_numbers = []

        box_combo_minimum = 0
        box_combo_maximum = 0
        box_combo_total = 0
        box_combo_avg = 0
        box_combo_count = 0

        for box_combo in box_combinations:

            quinella_combo = prediction_culled.iloc[list(box_combo)]

            first_win = quinella_combo.iloc[0]['win_odds']
            second_win = quinella_combo.iloc[1]['win_odds']

            if first_win == 0:
                continue

            if second_win == 0:
                continue

            first_number = quinella_combo.iloc[0]['runner_number']
            second_number = quinella_combo.iloc[1]['runner_number']

            estimated_dividend = ((first_win * second_win) / 2) * (budget / len(box_combinations))

            min_payout = float(config['quinella'][minimum_payout_option]) * budget

            if estimated_dividend < min_payout:

                if verbose >= 3:
                    print("Payout less that budget: No profitable bet possible!")

                continue

            if first_number not in box_combo_numbers:
                box_combo_numbers.append(first_number)

            if second_number not in box_combo_numbers:
                box_combo_numbers.append(second_number)

            box_combo_count += 1
            box_combo_total += estimated_dividend

            if box_combo_minimum == 0:
                box_combo_minimum = estimated_dividend

            if box_combo_maximum == 0:
                box_combo_maximum = estimated_dividend

            if estimated_dividend < box_combo_minimum:
                box_combo_minimum = estimated_dividend

            if estimated_dividend > box_combo_maximum:
                box_combo_maximum = estimated_dividend

        if box_combo_count > 0:
            box_combo_avg = box_combo_total / box_combo_count

        if verbose == 3:

            print("Combinations: %s" % box_combo_numbers)
            print("Box Combo Count: %s" % box_combo_count)
            print("Box Combo Avg: %s" % box_combo_avg)
            print("Box Combo Minimum: %s" % box_combo_minimum)
            print("Box Combo Maximum: %s" % box_combo_maximum)

        if len(box_combo_numbers) >= 2:

            bet = {'runners': box_combo_numbers,
                   'bet': budget,
                   'box_combo_count': box_combo_count,
                   'box_combo_avg': box_combo_avg,
                   'box_combo_minimum': box_combo_minimum,
                   'box_combo_maximum': box_combo_maximum,
                  }

            bet_options.append(bet)

    if len(bet_options) > 0:

        sort_selection = config['quinella']['sort_selection']

        if sort_selection == 1:

            if config['quinella'].getboolean('sort_options'):
                bet_options = sorted(bet_options, key=lambda x: x['box_combo_avg'],
                                     reverse=config['quinella'].getboolean('sort_reverse'))

        if sort_selection == 2:

            if config['quinella'].getboolean('sort_options'):
                bet_options = sorted(bet_options, key=lambda x: x['box_combo_minimum'],
                                     reverse=config['quinella'].getboolean('sort_reverse'))

        if sort_selection == 3:

            if config['quinella'].getboolean('sort_options'):
                bet_options = sorted(bet_options, key=lambda x: x['box_combo_maximum'],
                                     reverse=config['quinella'].getboolean('sort_reverse'))

        # print("Returning bet optoins: %s" % len(bet_options))

        return bet_options[0]

    else:

        return []

