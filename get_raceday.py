from datetime import *
from bs4 import BeautifulSoup
import urllib.request
import ssl
from datetime import datetime

import json


def get_raceday():

    today = date.today()
    year = today.strftime('%Y')
    month = today.strftime('%-m')
    day = today.strftime('%-d')
    race_day = today.strftime('%Y-%m-%d')

    url = "https://api.tatts.com/tbg/vmax/web/data/racing/%s/%s/%s/" % (year, month, day)
    gcontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1)  # Only for gangstars

    race_list = []

    with urllib.request.urlopen(url, context=gcontext) as response:
        jsondata = response.read()

        raceday = json.loads(jsondata.decode('utf-8'))

        racedata = raceday['Data']
        meetings = racedata[0]['Meetings']

        for meeting in meetings:

            meeting_code = meeting['MeetingCode']
            meeting_type = meeting['MeetingType']
            if meeting['Abandoned']:
                continue

            # if meeting_type != 'R':
            #     continue

            races = meeting['Races']

            for race in races:

                race_number = meeting_code + str(race['RaceNumber'])
                race_name = race['RaceName']
                race_time = race['RaceTime']

                race_list.append([race_number, race_name, race_time, race_day])

    race_list_sorted = sorted(
        race_list,
        key=lambda x: datetime.strptime(x[2], '%Y-%m-%dT%H:%M:%S')
    )

    return race_list_sorted


def print_race_day():
    print("Race List:\n")

    race_list = get_raceday()

    for race in race_list:
        race_time = datetime.strptime(race[2], '%Y-%m-%dT%H:%M:%S')
        race_formated = race_time.strftime('%H:%M')

        print("%s - %s - %s" % (race_formated, race[0], race[1]))


if __name__ == '__main__':
    print_race_day()


