import pandas as pd

from predict_race_rfc import predict_race

class HorseRacingPrediction:

    def __init__(self):

        self.race_code = ''
        self.race_name = ''
        self.prediction_set = {}
        self.prediction_result = {}

    def init_from_prediction_set(self, prediction_set):

        self.prediction_set = prediction_set

    def predict_race(self):

        self.prediction_result = predict_race(self.race_name, self.prediction_set)

    def get_prediction(self):

        return self.prediction_result


