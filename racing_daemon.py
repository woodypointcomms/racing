import datetime
import schedule
import time
import json
import requests
import threading
import os
import pandas as pd
from queue import Queue
import configparser

from get_raceday import get_raceday
from get_race import get_race, get_prediction
from get_race_results import get_race_results

from calc_place_bet_1way import calculate_place_bet_1way
from calc_place_bet import calculate_place_bet
from calc_place_bet_3way import calculate_place_bet_3way

from calc_win_bet import calculate_win_bet
from calc_win_bet_2way import calculate_win_bet_2way
from calc_win_bet_1way import calculate_win_bet_1way

from calc_eachway_bet_1way import calculate_eachway_bet_1way

from race_result import RaceResult

from bet_ledger import BetLedger

from ubet.login import login
from ubet.balance import get_balance
from ubet.lodge_win_bet import lodge_win_bet

from slack_notifier import SlackNotifier

config = configparser.ConfigParser()
config.read('racing.ini')

race_list = get_raceday()
# predict_list = Queue()
evaluate_list = Queue()
evaluate_list_win = Queue()
evaluate_list_eachway = Queue()
win_race_budget = config['win_betting'].getint('budget')
place_race_budget = config['place_betting'].getint('budget')
eachway_race_budget = config['eachway_betting'].getint('budget')

place_bet_list = []
place_ledger = BetLedger("PP")
place_ledger.set_balance(config['accounting']['opening_balance'])
place_profit_running = 0

win_bet_list = []
win_ledger = BetLedger("WW")
win_ledger.set_balance(config['accounting']['opening_balance'])
win_profit_running = 0

eachway_bet_list = []
eachway_ledger = BetLedger("WP")
eachway_ledger.set_balance(config['accounting']['opening_balance'])
eachway_profit_running = 0

eachway_target = config['accounting']['target']
eachway_stoploss = config['accounting']['stoploss']

place_target = config['accounting']['target']
place_stoploss = config['accounting']['stoploss']

win_target = config['accounting']['target']
win_stoploss = config['accounting']['stoploss']

slack = SlackNotifier()


def run_threaded(job_func, race_details=None):

    if race_details is None:
        job_thread = threading.Thread(target=job_func)
    else:
        job_thread = threading.Thread(target=job_func, args=[race_details])

    job_thread.start()

    return schedule.CancelJob


def get_pool_size(race_details, pool_code):

    race, racedata, meeting = get_race(race_details[0], race_details[3])

    pools = race['Pools']
    pool_size = 0

    for pool in pools:

        if pool['PoolType'] == pool_code:

            pool_size = pool['PoolTotal']
            return int(pool_size)

    return int(pool_size)


def predict_race(race_details):

    # global predict_list
    global evaluate_list
    global race_list
    global budget
    global place_bet_list
    global place_ledger

    config = configparser.ConfigParser()
    config.read('racing.ini')

    # Now getting this via the Schedule interface
    # race_details = predict_list.get()

    race_code = race_details[0]
    race_name = race_details[1]
    race_time_obj = datetime.datetime.strptime(race_details[2], '%Y-%m-%dT%H:%M:%S')
    race_date = race_details[3]
    race_time = race_time_obj.strftime('%H:%M')
    prediction = get_prediction(race_code, race_date)

    if prediction.empty:
        return

    slack.send_prediction(prediction, race_time)

    today = datetime.datetime.today()
    year = today.strftime('%Y')
    month = today.strftime('%m')
    day = today.strftime('%d')

    prediction_dir = "predictions/%s-%s-%s" % (year, month, day)
    bet_dir = "bets/%s-%s-%s" % (year, month, day)

    if not os.path.exists(prediction_dir):
        os.makedirs(prediction_dir)

    prediction.to_csv("%s/%s.csv" % (prediction_dir, race_code))

    if not os.path.exists(bet_dir):
        os.makedirs(bet_dir)

    # Place Bet handling
    place_bet = []
    win_bet = []
    eachway_bet = []

    # If pool is too small don't bet
    #win_pool_size = get_pool_size(race_details, 'WW')
    #place_pool_size = get_pool_size(race_details, 'PP')

    #if win_pool_size < 1000 or place_pool_size < 500:
    #    print("Not betting on %s as win pool is %d and place pool is %d" % (race_code, win_pool_size,
    #                                                                        place_pool_size))
    #    return

    if config['eachway_betting'].getboolean('enable_1way'):
        eachway_bet = calculate_eachway_bet_1way(prediction, eachway_race_budget)

    if config['place_betting'].getboolean('enable_3way'):
        place_bet = calculate_place_bet_3way(prediction, place_race_budget)

    if len(place_bet) == 0:

        if config['place_betting'].getboolean('enable_2way'):
            place_bet = calculate_place_bet(prediction, place_race_budget)

    if len(place_bet) == 0:

        if config['place_betting'].getboolean('enable_1way'):
            place_bet = calculate_place_bet_1way(prediction, place_race_budget)

    if config['win_betting'].getboolean('enable_3way'):
        win_bet = calculate_win_bet(prediction, win_race_budget)

    if len(win_bet) == 0:

        if config['win_betting'].getboolean('enable_2way'):
            win_bet = calculate_win_bet_2way(prediction, win_race_budget)

    if len(win_bet) == 0:

        if config['win_betting'].getboolean('enable_1way'):
            win_bet = calculate_win_bet_1way(prediction, win_race_budget)

    if len(place_bet) == 0:

        print("*%s:* No place bets recommended for race %s %s" % (race_code, race_code, race_name))

    else:

        slack.send_text("*%s:* Race %s %s at %s" % (race_code, race_code, race_name, race_time))

    # If pool is too small don't bet
    win_pool_size = get_pool_size(race_details, 'WW')
    min_win_pool_size = config['mode'].getint('min_win_pool')
    place_pool_size = get_pool_size(race_details, 'PP')
    min_place_pool_size = config['mode'].getint('min_place_pool')

    for place_bet_item in place_bet:

        runner_number = place_bet_item['runner']
        runner_name = place_bet_item['name']
        odds = place_bet_item['place_odds']
        bet = place_bet_item['bet']
        pay = place_bet_item['pay']

        # Only bet with sufficient pool size
        if win_pool_size >= min_win_pool_size and place_pool_size >= min_place_pool_size:

            if config['win_betting'].getboolean('live'):

                # Create bet
                global place_target
                global place_stoploss
                sess = login()
                balance = get_balance(sess)
                place_balance = place_ledger.get_balance()

                if bet < balance:

                    lodge_win_bet(sess, race_code, runner_number, 0, bet)
                    slack.send_text("*Attempted to place place bet on %s on ubet*" % race_code)

                else:

                    slack.send_text("*Not placing bet on %s on ubet as balance less than bet!*" % race_code)

                # place_ledger.create_bet(race_code, runner_number, "PP", odds, bet)

        slack.send_text("*%s:* Runner %d %s(%.2f): Place Bet: $%.2f Pays: $%.2f" % (race_code, runner_number, runner_name, odds, bet, pay))

    if len(place_bet) > 0:

        evaluate_time = (race_time_obj + datetime.timedelta(minutes=10)).strftime('%H:%M')
        print("*%s:* Scheduling evaluation for %s at %s" % (race_code, race_code, evaluate_time))

        #place_bet_list.append(place_bet)

        place_bet_df = pd.DataFrame(place_bet)
        place_bet_df.to_csv("%s/%s.csv" % (bet_dir, race_code))

        evaluate_list.put(race_details)
        schedule.every().day.at(evaluate_time).do(run_threaded, evaluate_race, place_bet)

    # End place bet handling

    # Win Bet handling
    if len(win_bet) == 0:

        print("*%s:* No win bets recommended for race %s %s" % (race_code, race_code, race_name))

    else:

        slack.send_text("*%s:* Race %s %s at %s" % (race_code, race_code, race_name, race_time))

    for win_bet_item in win_bet:
        runner_number = win_bet_item['runner']
        runner_name = win_bet_item['name']
        odds = win_bet_item['win_odds']
        bet = win_bet_item['bet']
        pay = win_bet_item['pay']

        # Create bet

        slack.send_text("*%s:* Runner %d %s(%.2f): Win Bet: $%.2f Pays: $%.2f" % (
            race_code, runner_number, runner_name, odds, bet, pay))

        # Only bet on Australian Races for now

        if win_pool_size >= min_win_pool_size and place_pool_size >= min_place_pool_size:

            if config['place_betting'].getboolean('live'):

                global place_target
                global place_stoploss
                sess = login()
                balance = get_balance(sess)
                win_balance = win_ledger.get_balance()

                if bet < balance:

                    lodge_win_bet(sess, race_code, runner_number, bet, 0)
                    slack.send_text("*Attempted to place win bet on %s on ubet*" % race_code)

                else:

                    slack.send_text("*Not placing bet on %s on ubet as balance less than bet!*" % race_code)

        # win_ledger.create_bet(race_code, runner_number, "WW", odds, bet)

    if len(win_bet) > 0:
        evaluate_time = (race_time_obj + datetime.timedelta(minutes=10)).strftime('%H:%M')
        print("*%s:* Scheduling evaluation for %s at %s" % (race_code, race_code, evaluate_time))

        #win_bet_list.append(win_bet)

        win_bet_df = pd.DataFrame(win_bet)
        win_bet_df.to_csv("%s/%s.csv" % (bet_dir, race_code))

        evaluate_list_win.put(race_details)
        schedule.every().day.at(evaluate_time).do(run_threaded, evaluate_race_win, win_bet)

    # End win bet handling

    # Each Way Bet handling
    if len(eachway_bet) == 0:

        print("*%s:* No each way bets recommended for race %s %s" % (race_code, race_code, race_name))

    else:

        slack.send_text("*%s:* Race %s %s at %s" % (race_code, race_code, race_name, race_time))

    for eachway_bet_item in eachway_bet:
        runner_number = eachway_bet_item['runner']
        runner_name = eachway_bet_item['name']
        win_odds = eachway_bet_item['win_odds']
        win_bet = eachway_bet_item['win_bet']
        win_pay = eachway_bet_item['win_pay']
        place_odds = eachway_bet_item['place_odds']
        place_bet = eachway_bet_item['place_bet']
        place_pay = eachway_bet_item['place_pay']
        total_bet = win_bet + place_bet

        win_bet = 1
        place_bet = 4

        amount = win_bet + place_bet

        # Create bet

        slack.send_text(
            "*%s:* Runner %d %s(%.2f - %.2f): Win Bet: $%.2f Place Bet: $%.2f Win Pay: $%.2f Place Pay: $%.2f" % (
                race_code, runner_number, runner_name, win_odds, place_odds, win_bet, place_bet, win_pay, place_pay))

        # Only bet on Australian Races for now
        if race_code[1] == 'R':

            global eachway_target
            global eachway_stoploss
            sess = login()
            balance = get_balance(sess)
            eachway_balance = eachway_ledger.get_balance()

            # if eachway_balance < eachway_target:
                # eachway_ledger.create_bet(race_code, runner_number, "WP", win_odds, amount)

            if total_bet < balance:

                lodge_win_bet(sess, race_code, runner_number, win_bet, place_bet)
                slack.send_text("*Attempted to place each way bet on %s on ubet*" % race_code)

            else:

                slack.send_text("*Not placing bet on %s on ubet as balance less than bet!*" % race_code)

    if len(eachway_bet) > 0:
        evaluate_time = (race_time_obj + datetime.timedelta(minutes=10)).strftime('%H:%M')
        print("*%s:* Scheduling evaluation for %s at %s" % (race_code, race_code, evaluate_time))

        #eachway_bet_list.append(eachway_bet)

        eachway_bet_df = pd.DataFrame(eachway_bet)
        eachway_bet_df.to_csv("%s/%s.csv" % (bet_dir, race_code))

        evaluate_list_eachway.put(race_details)
        schedule.every().day.at(evaluate_time).do(run_threaded, evaluate_race_eachway, eachway_bet)

    # End win bet handling

    # predict_list.task_done()


def evaluate_race(place_bet):

    global predict_list
    global evaluate_list
    global race_list
    global place_race_budget
    global place_bet_list
    global place_ledger

    print("Evaluate Place Bet")

    race_details = evaluate_list.get()
    race_code = race_details[0]
    race_date = race_details[3]

    results = []

    while len(results) < 1:
        results = get_race_results(race_code)
        time.sleep(60)

    today = datetime.datetime.today()
    year = today.strftime('%Y')
    month = today.strftime('%m')
    day = today.strftime('%d')

    results_dir = "results/%s-%s-%s" % (year, month, day)

    if not os.path.exists(results_dir):
        os.makedirs(results_dir)

    results_df = pd.DataFrame(results, columns=['Runner', 'Win', 'Place'])

    results_df.to_csv("%s/%s.csv" % (results_dir,race_code))

    # get place bet
    #place_bet = place_bet_list[0]
    #place_bet_list.remove(place_bet_list[0])

    profit_loss = -place_race_budget

    rr = RaceResult()
    rr.init_from_result_df(results_df)

    for bet in place_bet:

        dividend = rr.get_place_dividend(bet['runner'])

        paid = bet['bet'] * dividend
        profit_loss = profit_loss + paid

        # place_ledger.close_bet(race_code, bet['runner'], "PP", dividend, paid)

        slack.send_text("*%s:* runner number %d pays $%.2f, paid $%.2f" % (race_code, bet['runner'], dividend, paid))

    slack.send_text("*%s:* Place Profit or Loss: $%.2f" % (race_code, profit_loss))
    global place_profit_running
    place_profit_running = place_profit_running + profit_loss

    current_balance = place_ledger.get_balance()

    slack.send_text("*Current Place Balance:* $%.2f" % current_balance)

    slack.send_text("*Current Place Profit:* $%.2f" % place_profit_running)

    evaluate_list.task_done()


def evaluate_race_win(win_bet):

    global predict_list
    global evaluate_list_win
    global race_list
    global win_race_budget
    global win_bet_list
    global win_ledger

    print("Evaluate Win Bet")

    race_details = evaluate_list_win.get()
    race_code = race_details[0]

    results = []

    while len(results) < 1:
        results = get_race_results(race_code)
        time.sleep(60)

    today = datetime.datetime.today()
    year = today.strftime('%Y')
    month = today.strftime('%m')
    day = today.strftime('%d')

    results_dir = "results/%s-%s-%s" % (year, month, day)

    if not os.path.exists(results_dir):
        os.makedirs(results_dir)

    results_df = pd.DataFrame(results, columns=['Runner', 'Win', 'Place'])

    results_df.to_csv("%s/%s.csv" % (results_dir,race_code))

    # get place bet
    # win_bet = win_bet_list[0]
    # win_bet_list.remove(win_bet_list[0])

    profit_loss = -win_race_budget

    rr = RaceResult()
    rr.init_from_result_df(results_df)

    for bet in win_bet:

        dividend = rr.get_win_dividend(bet['runner'])

        paid = bet['bet'] * dividend
        profit_loss = profit_loss + paid

        # win_ledger.close_bet(race_code, bet['runner'], "WW", dividend, paid)

        slack.send_text("*%s:* runner number %d pays $%.2f, paid $%.2f" % (race_code, bet['runner'], dividend, paid))

    slack.send_text("*%s:* Win Profit or Loss: $%.2f" % (race_code, profit_loss))

    current_balance = win_ledger.get_balance()
    global win_profit_running
    win_profit_running = win_profit_running + profit_loss

    slack.send_text("*Current Win Balance:* $%.2f" % current_balance)

    slack.send_text("*Current Win Profit:* $%.2f" % win_profit_running)

    evaluate_list_win.task_done()


def evaluate_race_eachway(eachway_bet):

    global predict_list
    global evaluate_list_eachway
    global race_list
    global eachway_race_budget
    global eachway_bet_list
    global eachway_ledger

    print("Evaluate Each Way Bet")

    race_details = evaluate_list_eachway.get()
    race_code = race_details[0]

    results = []

    while len(results) < 1:
        results = get_race_results(race_code)
        time.sleep(60)

    today = datetime.datetime.today()
    year = today.strftime('%Y')
    month = today.strftime('%m')
    day = today.strftime('%d')

    results_dir = "results/%s-%s-%s" % (year, month, day)

    if not os.path.exists(results_dir):
        os.makedirs(results_dir)

    results_df = pd.DataFrame(results, columns=['Runner', 'Win', 'Place'])

    results_df.to_csv("%s/%s.csv" % (results_dir,race_code))

    # get Each Way bet
    #eachway_bet = eachway_bet_list[0]
    #eachway_bet_list.remove(eachway_bet_list[0])

    profit_loss = -eachway_race_budget

    rr = RaceResult()
    rr.init_from_result_df(results_df)

    for bet in eachway_bet:

        win_dividend = rr.get_win_dividend(bet['runner'])
        place_dividend = rr.get_place_dividend(bet['runner'])

        win_paid = bet['win_bet'] * win_dividend
        place_paid = bet['place_bet'] * place_dividend
        total_paid = win_paid + place_paid
        profit_loss = profit_loss + win_paid + place_paid

        # Create bet
        global eachway_target
        global eachway_stoploss
        eachway_balance = eachway_ledger.get_balance()

        # if eachway_balance < eachway_target:
            # eachway_ledger.close_bet(race_code, bet['runner'], "WP", win_dividend, total_paid)

        slack.send_text("*%s:* runner number %d pays $%.2f - $%.2f, paid $%.2f" % (race_code, bet['runner'], win_dividend, place_dividend, total_paid))

    slack.send_text("*%s:* Each Way Profit or Loss: $%.2f" % (race_code, profit_loss))

    current_balance = eachway_ledger.get_balance()
    global eachway_profit_running
    eachway_profit_running = eachway_profit_running + profit_loss

    slack.send_text("*Current Each Way Balance:* $%.2f" % current_balance)

    slack.send_text("*Current Each Way Profit:* $%.2f" % eachway_profit_running)

    evaluate_list_eachway.task_done()


def get_race_day():

    # Ensure schedule is clear at start
    schedule.clear()

    schedule.every().day.at("9:00").do(get_race_day)
    schedule.every().day.at("20:00").do(get_race_day)
    schedule.every().day.at("4:00").do(get_race_day)

    for race in race_list:

        race_time = race[2]
        race_code = race[0]
        dt_obj = datetime.datetime.strptime(race_time, '%Y-%m-%dT%H:%M:%S')

        if (dt_obj - datetime.timedelta(minutes=1)) > datetime.datetime.now():

            predict_time = (dt_obj - datetime.timedelta(minutes=1)).strftime('%H:%M')

            print("*%s:* Scheduling prediction for %s at %s" % (race_code, race_code, predict_time))
            schedule.every().day.at(predict_time).do(run_threaded, predict_race, race)
            # predict_list.put(race)

    slack.send_text("Scheduled %d races for prediction." % len(race_list))


get_race_day()
schedule.every().day.at("9:00").do(get_race_day)
schedule.every().day.at("20:00").do(get_race_day)
schedule.every().day.at("4:00").do(get_race_day)

while 1:
    schedule.run_pending()
    time.sleep(1)
