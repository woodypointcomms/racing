import pandas as pd
import tensorflow as tf
import csv


def input_fn(df_data, num_epochs, shuffle):

    return tf.estimator.inputs.pandas_input_fn(
        x=df_data,
        y=df_data['runner_points'],
        batch_size=100,
        num_epochs=num_epochs,
        shuffle=shuffle,
        num_threads=1
    )


def predict_race(race_name, prediction_set, prediction_array):

    form_c = tf.feature_column.categorical_column_with_vocabulary_list("form_c", ["T", "F"])
    form_d = tf.feature_column.categorical_column_with_vocabulary_list("form_d", ["T", "F"])
    form_t = tf.feature_column.categorical_column_with_vocabulary_list("form_t", ["T", "F"])
    form_w = tf.feature_column.categorical_column_with_vocabulary_list("form_w", ["T", "F"])

    last_start_1 = tf.feature_column.categorical_column_with_vocabulary_list("last_start_1",
                                                                             ["1", "2", "3", "4", "5", "6", "7", "8",
                                                                              "9", "0", "F", "L", "P", "N", "X", "Z"])
    last_start_2 = tf.feature_column.categorical_column_with_vocabulary_list("last_start_2",
                                                                             ["1", "2", "3", "4", "5", "6", "7", "8",
                                                                              "9", "0", "F", "L", "P", "N", "X", "Z"])
    last_start_3 = tf.feature_column.categorical_column_with_vocabulary_list("last_start_3",
                                                                             ["1", "2", "3", "4", "5", "6", "7", "8",
                                                                              "9", "0", "F", "L", "P", "N", "X", "Z"])

    weight = tf.feature_column.numeric_column("weight")
    barrier = tf.feature_column.numeric_column("barrier")

    feature_columns = [form_c, form_d, form_t, form_w, last_start_1, last_start_2, last_start_3, weight, barrier]

    if "MAIDEN" in race_name:
        model_dir = "models/maiden-plate-price"
    else:
        model_dir = "models/no-maiden-price"

    labels_output = ["TOP3", "NONE"]

    m = tf.estimator.LinearClassifier(model_dir=model_dir, feature_columns=feature_columns, n_classes=2,
                                      label_vocabulary=labels_output)

    y_pred = m.predict(input_fn(df_data=prediction_set, num_epochs=1, shuffle=False))

    out_file_name = "predictions.csv"

    out_file = open(out_file_name, "w")
    out_writer = csv.writer(out_file)

    cur_row = 0

    win_accuracy = 0
    num_runners = 0

    in_data_row = 0

    for pred in y_pred:

        if in_data_row > len(prediction_array) - 1:
            break

        output_label = pred
        num_runners = num_runners + 1

        try:
            in_data = prediction_array[in_data_row]
            in_data_row = in_data_row + 1
            race_number = in_data[1] + str(in_data[2])
            runner_number = in_data[5]
            runner_name = in_data[6]
            runner_position = in_data[7]

            win_odds = in_data[18]
            place_odds = in_data[19]

            in_label = in_data[8]
            out_label = str(output_label['classes'][0])
            out_label = out_label.replace('b', '')
            out_label = out_label.replace("'", '')

            if in_label == out_label:
                win_accuracy = win_accuracy + 1

            out_writer.writerow(
                [race_number, runner_number, runner_name, in_label, runner_position, out_label, output_label['probabilities'][0],
                 output_label['probabilities'][1], win_odds, place_odds])

        except(StopIteration) as err:

            out_file.close()

    out_file.close()

    prediction_list = pd.read_csv(out_file_name,
                                  names=['race_number', 'runner_number', 'runner_name', 'in_label', 'runner_position', 'out_label', 'top_3',
                                         'none', 'win_odds', 'place_odds'])

    result = prediction_list.sort_values(by=['top_3'], ascending=0)

    return result

