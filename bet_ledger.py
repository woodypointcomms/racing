import pandas as pd
from datetime import datetime
import os

from bet_ledger_item import BetLedgerItem


class BetLedger:

    def __init__(self, tag):

        self.tag = tag
        self.balance = 0
        self.bets = []
        self.ledger = pd.DataFrame(columns=['Race Code', 'Runner Number', 'Type', 'Bet Odds', 'Amount', 'Final Odds', 'Paid'])

        today = datetime.today()
        self.csv_file = "accounts/%s-%s.csv" % (today.strftime('%Y-%m-%d'), self.tag)

        # Load existing ledger if one exists
        if os.path.exists(self.csv_file):

            self.ledger = pd.read_csv(self.csv_file)

            for index, bet in self.ledger.iterrows():

                ledger_item = BetLedgerItem(bet['Race Code'], bet['Runner Number'], bet['Type'], bet['Bet Odds'], bet['Amount'])

                if bet['Final Odds'] is not None:

                    ledger_item.close_bet(bet['Final Odds'], bet['Paid'])

                self.bets.append(ledger_item)

    def set_balance(self, balance):

        self.balance = balance

    def get_balance(self):

        return self.balance

    def create_bet(self, race_code, runner_number, bet_type, bet_odds, amount):

        # Update Bet table
        ledger_item = BetLedgerItem(race_code, runner_number, bet_type, bet_odds, amount)
        self.bets.append(ledger_item)

        # Update Ledger
        ledger_row = {
                "Race Code": race_code,
                "Runner Number": runner_number,
                "Type": bet_type,
                "Bet Odds": bet_odds,
                "Amount": amount,
                "Final Odds": None,
                "Paid": None
            }

        i = len(self.ledger.index)

        for key in ledger_row.keys():
            self.ledger.loc[i, key] = ledger_row[key]

        self.ledger.to_csv(self.csv_file, index=False, na_rep="")

        # Update balance
        self.balance = self.balance - amount

    def close_bet(self, race_code, runner_number, bet_type, final_odds, paid):

        # Update Bet Table
        for bet in self.bets:

            if (bet.race_code == race_code) & (bet.runner_number == runner_number) & (bet.type == bet_type):

                bet.close_bet(final_odds, paid)

        # Update Ledger
        self.ledger.loc[
            ((self.ledger['Race Code'] == race_code) & (self.ledger['Runner Number'] == runner_number) & (self.ledger['Type'] == bet_type)), 'Final Odds'] = final_odds
        self.ledger.loc[
            ((self.ledger['Race Code'] == race_code) & (self.ledger['Runner Number'] == runner_number) & (self.ledger['Type'] == bet_type)), 'Paid'] = paid

        self.ledger.to_csv(self.csv_file, index=False, na_rep="")

        # Update Balance
        self.balance = self.balance + paid
