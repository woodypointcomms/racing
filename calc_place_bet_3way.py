import itertools
import configparser


def calculate_place_bet_3way(prediction, budget, verbose=2):

    config = configparser.ConfigParser()
    config.read('racing.ini')

    if prediction.empty:
        return []

    num_runners = len(prediction.index)

    if num_runners < 5:

        if verbose >= 2:
            print("No place betting available!")
        return []

    combination_limit = int(config['place_3way']['combination_limit'])

    prediction_culled = prediction[(prediction['prob_1'] > float(config['win_3way']['minimum_prob']))]

    if len(prediction_culled.index) < 3:
        if verbose >= 2:
            print("No 3 way place bets possible, not three runners selected!")
        return []

    if len(prediction_culled.index) < combination_limit:
        combination_limit = len(prediction_culled.index)

    bet_options = []

    combinations = list(itertools.combinations(range(0, combination_limit, 1), 3))

    for combination in combinations:

        first_three = prediction_culled.iloc[list(combination)]

        first_place = first_three.iloc[0]['place_odds']
        second_place = first_three.iloc[1]['place_odds']
        third_place = first_three.iloc[2]['place_odds']

        first_runner = first_three.iloc[0]['runner_name']
        second_runner = first_three.iloc[1]['runner_name']
        third_runner = first_three.iloc[2]['runner_name']

        first_number = first_three.iloc[0]['runner_number']
        second_number = first_three.iloc[1]['runner_number']
        third_number = first_three.iloc[2]['runner_number']

        first_recip = 1 / first_place
        second_recip = 1 / second_place
        third_recip = 1 / third_place
        recip_total = first_recip + second_recip + third_recip

        first_bet = (budget * first_recip) / recip_total
        second_bet = (budget * second_recip) / recip_total
        third_bet = (budget * third_recip) / recip_total

        first_pay = round((first_place * first_bet), 2)
        second_pay = round((second_place * second_bet), 2)
        third_pay = round((third_place * third_bet), 2)
        pay_total = first_pay + second_pay

        min_payout = float(config['place_3way']['minimum_payout'])

        if first_pay < (budget * min_payout) or second_pay < (budget * min_payout) or third_pay < (budget * min_payout):
            if verbose >= 2:
                print("Payout less that budget: No profitable bet possible!")

            return []

        bet = [
            {'runner': first_number,
             'bet': first_bet,
             'pay': first_pay,
             'name': first_runner,
             'place_odds': first_place,
             'pay_total': pay_total
             },
            {'runner': second_number,
             'bet': second_bet,
             'pay': second_pay,
             'name': second_runner,
             'place_odds': second_place,
             'pay_total': pay_total
             },
            {'runner': third_number,
             'bet': third_bet,
             'pay': third_pay,
             'name': third_runner,
             'place_odds': third_place,
             'pay_total': pay_total
             }
        ]

        if verbose >= 2:
            print("%d: %s - Bet: $%.2f Target: %.2f" % (first_number, first_runner, first_bet, first_pay))
            print("%d: %s - Bet: $%.2f Target: %.2f" % (second_number, second_runner, second_bet, second_pay))
            print("%d: %s - Bet: $%.2f Target: %.2f" % (third_number, third_runner, third_bet, third_pay))

        bet_options.append(bet)

    if len(bet_options) > 0:

        if config['place_3way'].getboolean('sort_options'):
            bet_options = sorted(bet_options, key=lambda x: x[0]['pay_total'],
                                 reverse=config['place_3way'].getboolean('sort_reverse'))

        return bet_options[0]

    else:

        return []
