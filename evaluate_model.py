import sys
import pandas as pd
import tensorflow as tf
import csv
import json

from predict_race import predict_race
from calc_place_bet import calculate_place_bet

test_set_file = sys.argv[1]

# test_df = pd.read_csv(test_set)
#
# CSV_COLUMNS = ["race_date", "meeting_code", "race_number", "race_name", "race_distance", "runner_number", "runner_name", "runner_position", "runner_points", "barrier", "weight", "form_c", "form_d", "form_t", "form_w", "last_start_1", "last_start_2", "last_start_3", "win_odds", "place_odds", "last_win_odds", "last_place_odds"]
#
# def input_fn(data_file, num_epochs, shuffle):
#
#     df_data = pd.read_csv(
#         tf.gfile.Open(data_file),
#         names=CSV_COLUMNS,
#         skipinitialspace=True,
#         engine="python",
#         skiprows=1
#     )
#
#     #df_data = df_data.droprows(how="any", axis=0)
#
#     #print("num_classes: %s" % (len(set(df_data['runner_points']))))
#
#     return tf.estimator.inputs.pandas_input_fn(
#         x=df_data,
#         y=df_data['runner_points'],
#         batch_size=100,
#         num_epochs=num_epochs,
#         shuffle=shuffle,
#         num_threads=5
#     )
#
# form_c = tf.feature_column.categorical_column_with_vocabulary_list("form_c", ["T", "F"])
# form_d = tf.feature_column.categorical_column_with_vocabulary_list("form_d", ["T", "F"])
# form_t = tf.feature_column.categorical_column_with_vocabulary_list("form_t", ["T", "F"])
# form_w = tf.feature_column.categorical_column_with_vocabulary_list("form_w", ["T", "F"])
#
# last_start_1 = tf.feature_column.categorical_column_with_vocabulary_list("last_start_1", ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "F", "L", "P", "N", "X", "Z"])
# last_start_2 = tf.feature_column.categorical_column_with_vocabulary_list("last_start_2", ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "F", "L", "P", "N", "X", "Z"])
# last_start_3 = tf.feature_column.categorical_column_with_vocabulary_list("last_start_3", ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "F", "L", "P", "N", "X", "Z"])
#
# weight = tf.feature_column.numeric_column("weight")
# barrier = tf.feature_column.numeric_column("barrier")
#
# feature_columns = [form_c, form_d, form_t, form_w, last_start_1, last_start_2, last_start_3, weight, barrier]
#
# model_dir = "models/no-maiden-price"
# labels_output = ["TOP3", "NONE"]
#
# m = tf.estimator.LinearClassifier(model_dir=model_dir, feature_columns=feature_columns, n_classes=2, label_vocabulary=labels_output)
#
# X_input = pd.read_csv(test_set)
#
# y_pred = m.predict(input_fn(data_file=test_set, num_epochs=1, shuffle=False))
#
# cur_row = 0
# num_rows = len(X_input.index)
#
# prediction_set = []
#
# for pred in y_pred:
#
#     if cur_row >= num_rows:
#         break
#
#     output_label = pred
#
#     race_date = X_input['race_date'][cur_row]
#     race_number = X_input['meeting_code'][cur_row] + str(X_input['race_number'][cur_row])
#     runner_number = X_input['runner_number'][cur_row]
#     runner_name = X_input['runner_name'][cur_row]
#
#     runner_position = X_input['runner_position'][cur_row]
#     runner_points = X_input['runner_points'][cur_row]
#     out_label = str(output_label['classes'][0])
#     out_label = out_label.replace('b', '')
#     out_label = out_label.replace("'", '')
#
#     runner_win_odds = X_input['win_odds'][cur_row]
#     runner_place_odds = X_input['place_odds'][cur_row]
#
#     prediction_set.append([race_date, race_number, runner_number, runner_name, runner_position, runner_points, out_label, output_label['probabilities'][0], output_label['probabilities'][1], runner_win_odds, runner_place_odds])
#
#     cur_row = cur_row + 1

# prediction_df = pd.DataFrame(prediction_set, columns=['race_date', 'race_number', 'runner_number', 'runner_name', 'runner_position', 'runner_points', 'out_label', 'probability_top3', 'probability_none', 'runner_win_odds', 'runner_place_odds'])



#prediction_df.to_csv("prediction_df.csv")

test_set = pd.read_csv(test_set_file)

top3_correct = 0
top3_winner = 0
top3_not_predicted = 0
top3_false = 0
total_races = 0

winner_pred_no1 = 0
winner_pred_no12 = 0
winner_pred_no123 = 0
winner_pred_no_place = 0

place_pred_no1 = 0
place_pred_no12 = 0
place_pred_no123 = 0
place_pred_no_place = 0

running_return = 0

dates_and_races = test_set[['race_date', 'meeting_code', 'race_number']]
dates_and_races.drop_duplicates(inplace=True)

number_of_races = len(dates_and_races.index)

default_budget = 10
chases_losses = 1
max_budget = default_budget * chases_losses

last_profit = 0
place_bets_made = 0

profit_this_race = 0

for index, dr in dates_and_races.iterrows():

    race_date = dr['race_date']
    meeting_code = dr['meeting_code']
    race_number = dr['race_number']

    race_data = test_set[(test_set['race_date'] == race_date) & (test_set['meeting_code'] == meeting_code)
                & (test_set['race_number'] == race_number)]

    print("\n%s - %s%s" % (race_date, meeting_code, race_number))

    total_races = total_races + 1

    race_array = race_data.as_matrix()
    race_results = predict_race("tst", race_data, race_array)
    # print(race_results)

    print("Last race profit was: %.2f" % last_profit)

    if last_profit > 0:
        race_budget = default_budget
    else:
        race_budget = default_budget - last_profit

    if race_budget > max_budget:
        race_budget = max_budget

    print("This race budget: %.2f" % race_budget)

    place_bets = calculate_place_bet(race_results, race_budget)

    # Calculate place bet winnings
    if len(place_bets) > 0:
        place_bets_made = place_bets_made + 1
        profit_this_race = 0

        runner_pred_count = 0

        for index, runner in race_results.iterrows():

            runner_position = runner['runner_position']
            runner_number = runner['runner_number']
            place_odds = runner['place_odds']
            predict_label = runner['out_label']

            if runner_position > 0:

                if predict_label == "TOP3":

                    top3_correct = top3_correct + 1

                else:

                    top3_not_predicted = top3_not_predicted + 1

                if runner_pred_count == 0:

                    place_pred_no1 = place_pred_no1 + 1
                    place_pred_no12 = place_pred_no12 + 1
                    place_pred_no123 = place_pred_no123 + 1

                if runner_pred_count == 1:

                    place_pred_no12 = place_pred_no12 + 1
                    place_pred_no123 = place_pred_no123 + 1

                if runner_pred_count == 2:

                    place_pred_no123 = place_pred_no123 + 1

            if runner_position == 1:

                if predict_label == "TOP3":

                    top3_winner = top3_winner + 1

                if runner_pred_count == 0:

                    winner_pred_no1 = winner_pred_no1 + 1
                    winner_pred_no12 = winner_pred_no12 + 1
                    winner_pred_no123 = winner_pred_no123 + 1

                if runner_pred_count == 1:

                    winner_pred_no12 = winner_pred_no12 + 1
                    winner_pred_no123 = winner_pred_no123 + 1

                if runner_pred_count == 2:

                    winner_pred_no123 = winner_pred_no123 + 1

            if runner_position == 0:

                if predict_label == "TOP3":

                    top3_false = top3_false + 1

                if runner_pred_count < 4:

                    place_pred_no_place = place_pred_no_place + 1

                if runner_pred_count == 0:

                    winner_pred_no_place = winner_pred_no_place + 1

            for bet in place_bets:

                bet_number = bet['runner']
                bet_amount = bet['bet']
                bet_pay = bet['pay']

                number_of_runners = len(race_results.index)

                if bet_number == runner_number:

                    running_return = running_return - bet_amount
                    profit_this_race = profit_this_race - bet_amount

                    if number_of_runners >= 8:

                        dividend_stop = 4

                    elif number_of_runners >= 5:

                        dividend_stop = 3
                        print("No third dividend!")

                    else:

                        dividend_stop = 2
                        print("No place betting!")

                    if 0 < runner_position < dividend_stop:

                        running_return = running_return + bet_pay
                        profit_this_race = profit_this_race + bet_pay

                        print("Bet $%.2f on number %d(%.2f), came %d returns $%.2f" % (bet_amount, runner_number, place_odds, runner_position, bet_pay))

                    else:

                        print("Bet$ %.2f on number %d(%.2f), came %d no pay" % (bet_amount, runner_number, place_odds, runner_position))

                    print("Running return: $%.2f" % running_return)

            runner_pred_count = runner_pred_count + 1

        last_profit = profit_this_race

print("Total Races: %d" % total_races)
print("Top3 Correct: %d" % top3_correct)
print("Top3 Winner: %d" % top3_winner)
print("Top3 Not Predicted: %d" % top3_not_predicted)
print("Top3 False: %d" % top3_false)

print("Total Races: %d" % total_races)
print("Winner in top position: %d" % winner_pred_no1)
print("Winner in top 2 positions: %d" % winner_pred_no12)
print("Winner in top 3 positions: %d" % winner_pred_no123)
print("Top position not winner: %d" % winner_pred_no_place)

print("Total Races: %d" % total_races)
print("Place in top position: %d" % place_pred_no1)
print("Place in top 2 positions: %d" % place_pred_no12)
print("Place in top 3 positions: %d" % place_pred_no123)
print("Non placers in top 3 positions: %d" % place_pred_no_place)
print("Place bets made: %d" % place_bets_made)

print("Return: $%.2f" % running_return)
