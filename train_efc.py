import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import pickle

from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn import preprocessing
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import classification_report, confusion_matrix, matthews_corrcoef
from sklearn.model_selection import GridSearchCV

from scipy import stats

train_df = pd.read_csv(sys.argv[1])
test_df = pd.read_csv(sys.argv[2])

def create_last_start_columns(input):

    column_prefix = ["last_start_1_", "last_start_2_", "last_start_3_"]
    column_list = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'F', 'L', 'P', 'X']
    #column_list = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
    #column_list = ['1', '2']

    column_names = []

    for prefix in column_prefix:

        for column_value in column_list:

            column_name = prefix + column_value
            column_names.append(column_name)

    last_start_array = []

    for index, row in input.iterrows():

        new_row = {}

        for i in [1, 2, 3]:

            for column_value in column_list:

                if str(row["last_start_" + str(i)]) == str(column_value):

                    new_row["last_start_" + str(i) + "_" + column_value] = 1

                else:

                    new_row["last_start_" + str(i) + "_" + column_value] = 0

        last_start_array.append(new_row)

    df = pd.DataFrame(last_start_array)

    return df


def create_weather_columns(input):

    column_prefix = "weather_"

    column_names = []

    for i in range(1, 13):

        column_name = column_prefix + str(i)
        column_names.append(column_name)

    weather_array = []

    for index, row in input.iterrows():

        new_row = {}

        for i in range(1, 13):

            if str(row["weather"]) == str(i):

                new_row["weather_" + str(i)] = 1

            else:

                new_row["weather_" + str(i)] = 0

        weather_array.append(new_row)

    df = pd.DataFrame(weather_array)

    return df


def create_track_rating_columns(input):

    column_prefix = "track_rating"

    column_names = []

    for i in range(1, 13):

        column_name = column_prefix + str(i)
        column_names.append(column_name)

    track_rating_array = []

    for index, row in input.iterrows():

        new_row = {}

        for i in range(1, 13):

            if str(row["track_rating"]) == str(i):

                new_row["track_rating_" + str(i)] = 1

            else:

                new_row["track_rating_" + str(i)] = 0

        track_rating_array.append(new_row)

    df = pd.DataFrame(track_rating_array)

    return df


X_train = train_df.drop(
    ['race_date', 'meeting_code', 'race_number', 'race_name', 'runner_number', 'runner_name', 'runner_position',
     'runner_points', 'last_win_odds', 'last_place_odds', 'race_distance', 'form_c', 'form_d', 'form_t', 'form_w'], axis=1)

#ls = create_last_start_columns(X_train)
# w = create_weather_columns(X_train)
# tr = create_track_rating_columns(X_train)

#dummies = pd.concat([X_train, ls], axis=1)
dummies = X_train.drop(['last_start_1', 'last_start_2', 'last_start_3', 'weather', 'track_rating'], axis=1)

standardised = preprocessing.scale(dummies['weight'])
dummies = dummies.drop('weight', axis=1)
dummies = dummies.assign(weight=standardised)

standardised = preprocessing.scale(dummies['barrier'])
dummies = dummies.drop('barrier', axis=1)
dummies = dummies.assign(barrier=standardised)

standardised = preprocessing.scale(dummies['win_odds'])
dummies = dummies.drop('win_odds', axis=1)
#dummies = dummies.assign(win_odds_std=standardised)

standardised = preprocessing.scale(dummies['place_odds'])
dummies = dummies.drop('place_odds', axis=1)
#dummies = dummies.assign(place_odds_std=standardised)

y_train = train_df['runner_points']

rfc = ExtraTreesClassifier(n_estimators=500, max_features=None, min_samples_leaf=5, min_samples_split=2, class_weight="balanced")

# parameters = {'min_samples_split': (2, 5, 10, 15, 20),
#               'min_samples_leaf': (5, 10, 15, 20, 25)}
#
# clf = GridSearchCV(rfc, parameters)
# clf.fit(dummies, y_train)
#
# print(sorted(clf.cv_results_))
# print(clf.best_score_)
# print(clf.best_params_)
# exit()

rfc.fit(dummies, y_train)

s = pickle.dump(rfc, open("efc-2018-08-06a.p", "wb"))

X_test = test_df.drop(
    ['race_date', 'meeting_code', 'race_number', 'race_name', 'runner_number', 'runner_name', 'runner_position',
     'runner_points', 'last_win_odds', 'last_place_odds', 'race_distance', 'form_c', 'form_d', 'form_t', 'form_w'], axis=1)

# ls = create_last_start_columns(X_test)
# w = create_weather_columns(X_test)
# tr = create_track_rating_columns(X_test)

# dummies = pd.concat([X_test, ls], axis=1)
dummies = X_test.drop(['last_start_1', 'last_start_2', 'last_start_3', 'weather', 'track_rating'], axis=1)

standardised = preprocessing.scale(dummies['weight'])
dummies = dummies.drop('weight', axis=1)
dummies = dummies.assign(weight=standardised)

standardised = preprocessing.scale(dummies['barrier'])
dummies = dummies.drop('barrier', axis=1)
dummies = dummies.assign(barrier=standardised)

standardised = preprocessing.scale(dummies['win_odds'])
dummies = dummies.drop('win_odds', axis=1)
#dummies = dummies.assign(win_odds_std=standardised)

standardised = preprocessing.scale(dummies['place_odds'])
dummies = dummies.drop('place_odds', axis=1)
#dummies = dummies.assign(place_odds_std=standardised)

y_test = test_df['runner_points']

predictions = rfc.predict(dummies)
predictions_prob = rfc.predict_proba(dummies)

predictions_df = pd.DataFrame(predictions, columns=['prediction'])
predictions_prob_df = pd.DataFrame(predictions_prob, columns=['prob_0', 'prob_1'])

positions = test_df['runner_position']

pred_plot_df = pd.concat([predictions_df, predictions_prob_df, y_test, positions], axis=1)

print(pred_plot_df.head())

print(classification_report(y_test,predictions))
print(confusion_matrix(y_test,predictions))

print(matthews_corrcoef(y_test,predictions))

print(stats.pearsonr(y_test, predictions))

imp_df = pd.DataFrame()
imp_df['columns'] = dummies.columns
imp_df['feature_importance'] = rfc.feature_importances_

print(imp_df)

def is_top4(row):
    if row['runner_position'] != 0 and row['runner_position'] <= 4:
        return 1
    else:
        return 0


pred_plot_df['top4'] = pred_plot_df.apply(is_top4, axis=1)

sns.set_style("whitegrid")
ax = sns.swarmplot('runner_position', 'prob_1', data=pred_plot_df, hue='runner_position', palette='rainbow')
#ax = sns.swarmplot('top4', 'prob_1', data=pred_plot_df)
ax.set(ylabel='Top 4 Probability', xlabel='Actual Position', title='Random Forest Classifier Race Prediction')
ax.set_ylim(0, 1)
plt.show()

