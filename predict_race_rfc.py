import pandas as pd
import pickle
from sklearn import preprocessing


def create_last_start_columns(input):

    column_prefix = ["last_start_1_", "last_start_2_", "last_start_3_"]
    #column_list = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'F', 'L', 'P', 'X']
    column_list = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']

    column_names = []

    for prefix in column_prefix:

        for column_value in column_list:

            column_name = prefix + column_value
            column_names.append(column_name)

    last_start_array = []

    for index, row in input.iterrows():

        new_row = {}

        for i in [1, 2, 3]:

            for column_value in column_list:

                if str(row["last_start_" + str(i)]) == str(column_value):

                    new_row["last_start_" + str(i) + "_" + column_value] = 1

                else:

                    new_row["last_start_" + str(i) + "_" + column_value] = 0

        last_start_array.append(new_row)

    df = pd.DataFrame(last_start_array)

    return df


def create_weather_columns(input):

    column_prefix = "weather_"

    column_names = []

    for i in range(1, 13):

        column_name = column_prefix + str(i)
        column_names.append(column_name)

    weather_array = []

    for index, row in input.iterrows():

        new_row = {}

        for i in range(1, 13):

            if str(row["weather"]) == str(i):

                new_row["weather_" + str(i)] = 1

            else:

                new_row["weather_" + str(i)] = 0

        weather_array.append(new_row)

    df = pd.DataFrame(weather_array)

    return df


def create_track_rating_columns(input):

    column_prefix = "track_rating"

    column_names = []

    for i in range(1, 13):

        column_name = column_prefix + str(i)
        column_names.append(column_name)

    track_rating_array = []

    for index, row in input.iterrows():

        new_row = {}

        for i in range(1, 13):

            if str(row["track_rating"]) == str(i):

                new_row["track_rating_" + str(i)] = 1

            else:

                new_row["track_rating_" + str(i)] = 0

        track_rating_array.append(new_row)

    df = pd.DataFrame(track_rating_array)

    return df


def predict_race(race_name, original_set):

    # if "MAIDEN" in race_name:
    #
    #     rfc = pickle.load(open("models/rfc-maiden-2018-07-11/rfc.p", "rb"))
    #
    # else:
    #
    #     rfc = pickle.load(open("models/rfc-no-maiden-2018-07-11/rfc.p", "rb"))

    zero_win_check = original_set[original_set['win_odds'] == 0]
    zero_place_check = original_set[original_set['place_odds'] == 0]

    if len(zero_win_check.index) > 0 or len(zero_place_check.index) > 0:
        print("Unable to return valid prediction as some prices are zero!")
        return pd.DataFrame

    rfc = pickle.load(open("efc-2018-08-03a.p", "rb"))

    prediction_set = original_set.drop(
        ['race_date', 'meeting_code', 'race_number', 'race_name', 'runner_number', 'runner_name', 'runner_position',
         'runner_points', 'last_win_odds', 'last_place_odds', 'race_distance', 'form_c', 'form_d', 'form_t', 'form_w'],
        axis=1)

    #ls = create_last_start_columns(prediction_set)
    # w = create_weather_columns(prediction_set)
    # tr = create_track_rating_columns(prediction_set)

    #prediction_set = pd.concat([prediction_set, ls], axis=1)
    prediction_set = prediction_set.drop(['last_start_1', 'last_start_2', 'last_start_3', 'weather', 'track_rating'], axis=1)

    standardised = preprocessing.scale(prediction_set['weight'])
    prediction_set = prediction_set.drop('weight', axis=1)
    # prediction_set = prediction_set.assign(weight=standardised)

    standardised = preprocessing.scale(prediction_set['barrier'])
    prediction_set = prediction_set.drop('barrier', axis=1)
    # prediction_set = prediction_set.assign(barrier=standardised)

    standardised = preprocessing.scale(prediction_set['win_odds'])
    # prediction_set = dummies.drop('win_odds', axis=1)
    prediction_set = prediction_set.assign(win_odds_std=standardised)

    standardised = preprocessing.scale(prediction_set['place_odds'])
    # prediction_set = prediction_set.drop('place_odds', axis=1)
    prediction_set = prediction_set.assign(place_odds_std=standardised)

    predictions = rfc.predict(prediction_set)
    predictions_prob = rfc.predict_proba(prediction_set)

    predictions_df = pd.DataFrame(predictions, columns=['prediction'])
    predictions_prob_df = pd.DataFrame(predictions_prob, columns=['prob_0', 'prob_1'])

    output_set = pd.concat([original_set, predictions_df, predictions_prob_df], axis=1)
    output_set = output_set.sort_values(by=['prob_1'], ascending=0)

    return output_set
