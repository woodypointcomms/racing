import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

import pickle

from sklearn import preprocessing

import tensorflow.estimator as learn

import tensorflow as tf

train_df = pd.read_csv(sys.argv[1])
test_df = pd.read_csv(sys.argv[2])

def input_fn(x_in, y_in, num_epochs, shuffle, num_threads):

    return tf.estimator.inputs.pandas_input_fn(
        x=x_in,
        y=y_in,
        batch_size=100,
        num_epochs=num_epochs,
        shuffle=shuffle,
        num_threads=num_threads
    )


training_set = train_df.drop(['race_date', 'meeting_code', 'race_number', 'race_name', 'runner_number', 'runner_name', 'runner_position', 'runner_points', 'last_win_odds', 'last_place_odds'], axis=1)

standardised = preprocessing.scale(training_set['weight'])
training_set = training_set.drop('weight', axis=1)
training_set = training_set.assign(weight=standardised)

standardised = preprocessing.scale(training_set['barrier'])
training_set = training_set.drop('barrier', axis=1)
training_set = training_set.assign(barrier=standardised)

standardised = preprocessing.scale(training_set['win_odds'])
#training_set = training_set.drop('win_odds', axis=1)
training_set = training_set.assign(win_odds_std=standardised)

standardised = preprocessing.scale(training_set['place_odds'])
#training_set = training_set.drop('place_odds', axis=1)
training_set = training_set.assign(place_odds_std=standardised)

y_train = train_df['runner_points']

form_c = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list("form_c", [1, 0]))
form_d = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list("form_d", [1, 0]))
form_t = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list("form_t", [1, 0]))
form_w = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list("form_w", [1, 0]))

#weather = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list("weather", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]))
#track_rating = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list("track_rating", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]))

last_start_1 = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list("last_start_1", ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "F", "L", "P", "N", "X", "Z"]))
last_start_2 = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list("last_start_2", ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "F", "L", "P", "N", "X", "Z"]))
last_start_3 = tf.feature_column.indicator_column(tf.feature_column.categorical_column_with_vocabulary_list("last_start_3", ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "F", "L", "P", "N", "X", "Z"]))

weight = tf.feature_column.numeric_column("weight")
barrier = tf.feature_column.numeric_column("barrier")
win_odds = tf.feature_column.numeric_column("win_odds")
win_odds_std = tf.feature_column.numeric_column("win_odds_std")
place_odds = tf.feature_column.numeric_column("place_odds")
place_odds_std = tf.feature_column.numeric_column("place_odds_std")

feature_columns = [form_c, form_d, form_t, form_w, last_start_1, last_start_2, last_start_3, weight, barrier, win_odds, win_odds_std, place_odds, place_odds_std]

dnn = learn.DNNClassifier(model_dir="models/dnn", hidden_units=[60, 120, 240], dropout=0.2, n_classes=2, feature_columns=feature_columns)

dnn.train(input_fn(x_in=training_set, y_in=y_train, num_epochs=10, shuffle=True, num_threads=1), steps=1000)

testing_set = test_df.drop(['race_date', 'meeting_code', 'race_number', 'race_name', 'runner_number', 'runner_name', 'runner_position', 'runner_points', 'last_win_odds', 'last_place_odds'], axis=1)

standardised = preprocessing.scale(testing_set['weight'])
testing_set = testing_set.drop('weight', axis=1)
testing_set = testing_set.assign(weight=standardised)

standardised = preprocessing.scale(testing_set['barrier'])
testing_set = testing_set.drop('barrier', axis=1)
testing_set = testing_set.assign(barrier=standardised)

standardised = preprocessing.scale(testing_set['win_odds'])
#testing_set = testing_set.drop('win_odds', axis=1)
testing_set = testing_set.assign(win_odds_std=standardised)

standardised = preprocessing.scale(testing_set['place_odds'])
#testing_set = testing_set.drop('place_odds', axis=1)
testing_set = testing_set.assign(place_odds_std=standardised)

y_test = test_df['runner_points']

predictions = list(dnn.predict(input_fn(x_in=testing_set, y_in=y_test, num_epochs=1, shuffle=True, num_threads=1)))

pred_list = []

for pred in predictions:

    pred_class = pred['class_ids'][0]
    pred_list.append(pred_class)

confusion = tf.confusion_matrix(labels=y_test, predictions=pred_list, num_classes=2)

with tf.Session():
   print('Confusion Matrix: \n\n', tf.Tensor.eval(confusion,feed_dict=None, session=None))


