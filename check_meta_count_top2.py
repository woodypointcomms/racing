import pickle
import pandas as pd

from get_prediction_summary import get_prediction_summary


def check_meta_count_top2(predictions):

    linmodel = pickle.load(open("meta_first_pred_top2_top2.p", "rb"))

    if len(predictions.index) < 1:
        return 0

    summary = get_prediction_summary(predictions)
    meta_pred = linmodel.predict(summary)

    return meta_pred

