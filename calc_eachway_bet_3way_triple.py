import configparser
import itertools
import math


def calculate_eachway_bet_3way_triple(prediction, budget, verbose=2):

    config = configparser.ConfigParser()
    config.read('racing.ini')

    if prediction.empty:
        return []

    num_runners = len(prediction.index)

    if num_runners < 5:
        if verbose >= 2:
            print("No place betting available!")
        return []

    combination_limit = int(config['eachway_3way_triple']['combination_limit'])

    prediction_culled = prediction[(prediction['prob_1'] > float(config['eachway_3way_triple']['minimum_prob']))]

    if len(prediction_culled.index) < 3:
        if verbose >= 2:
            print("No 3 way each way double bets possible, not three runners selected!")
        return []

    first_three = prediction_culled.head(3)
    first_win = first_three.iloc[0]['win_odds']
    first_place = first_three.iloc[0]['place_odds']
    first_runner = first_three.iloc[0]['runner_name']
    first_number = first_three.iloc[0]['runner_number']
    first_total = first_win + first_place

    second_win = first_three.iloc[1]['win_odds']
    second_place = first_three.iloc[1]['place_odds']
    second_runner = first_three.iloc[1]['runner_name']
    second_number = first_three.iloc[1]['runner_number']
    second_total = second_win + second_place

    third_win = first_three.iloc[2]['win_odds']
    third_place = first_three.iloc[2]['place_odds']
    third_runner = first_three.iloc[2]['runner_name']
    third_number = first_three.iloc[2]['runner_number']
    third_total = second_win + second_place

    #print("First win odds: %.2f First Place odds: %.2f" % (first_win, first_place))
    #print("Second win odds: %.2f Second Place odds: %.2f" % (second_win, second_place))
    #print("Third win odds: %.2f Third Place odds: %.2f" % (third_win, third_place))

    if first_place == 0 or second_place == 0 or third_place == 0:
        if verbose >= 2:
            print("First, second or third place odds are 0")
        return []

    first_place_bet = math.ceil((budget * float(config['eachway_3way_triple']['minimum_place_payout'])) / first_place)
    first_win_bet = math.floor(budget - first_place_bet)

    second_place_bet = math.ceil((budget * float(config['eachway_3way_triple']['minimum_place_payout'])) / second_place)
    second_win_bet = math.floor(budget - second_place_bet)

    third_place_bet = math.ceil(
        (budget * float(config['eachway_3way_triple']['minimum_place_payout'])) / third_place)
    third_win_bet = math.floor(budget - third_place_bet)

    budget = 5

    first_win_bet = 1
    second_win_bet = 1
    third_win_bet = 1

    first_place_bet = 4
    second_place_bet = 4
    third_place_bet = 4

    # if first_win_bet < 1 or second_win_bet < 1 or third_win_bet < 1:
    #     continue

    first_win_pay = round((first_win * first_win_bet), 2)
    first_place_pay = round((first_place * first_place_bet), 2)
    first_pay_total = first_win_pay + first_place_pay

    second_win_pay = round((second_win * second_win_bet), 2)
    second_place_pay = round((second_place * second_place_bet), 2)
    second_pay_total = second_win_pay + second_place_pay

    third_win_pay = round((third_win * third_win_bet), 2)
    third_place_pay = round((third_place * third_place_bet), 2)
    third_pay_total = third_win_pay + third_place_pay

    #print("First win pay: %.2f First place pay: %.2f" % (first_win_pay, first_place_pay))
    #print("Second win pay: %.2f Second place pay: %.2f" % (second_win_pay, second_place_pay))
    #print("Third win pay: %.2f Third place pay: %.2f" % (third_win_pay, third_place_pay))

    min_win_payout = float(config['eachway_3way_triple']['minimum_win_payout'])
    min_place_payout = float(config['eachway_3way_triple']['minimum_place_payout'])

    if first_win_pay < (budget * min_win_payout) or first_place_pay < (budget * min_place_payout) \
            or second_win_pay < (budget * min_win_payout) or second_place_pay < (budget * min_place_payout)\
            or third_win_pay < (budget * min_win_payout) or third_place_pay < (budget * min_place_payout):
        if verbose >= 2:
            print("3 Runner Each way triple Payout less that budget: No profitable bet possible!")

        return []

    bet = [
        {'runner': first_number,
         'win_bet': first_win_bet,
         'place_bet': first_place_bet,
         'name': first_runner,
         'win_odds': first_win,
         'win_pay': first_win_pay,
         'place_odds': first_place,
         'place_pay': first_place_pay,
         'pay_total': first_pay_total
         },
        {
            'runner': second_number,
            'win_bet': second_win_bet,
            'place_bet': second_place_bet,
            'name': second_runner,
            'win_odds': second_win,
            'win_pay': second_win_pay,
            'place_odds': second_place,
            'place_pay': second_place_pay,
            'pay_total': second_pay_total
        }
        ,
        {
            'runner': third_number,
            'win_bet': third_win_bet,
            'place_bet': third_place_bet,
            'name': third_runner,
            'win_odds': third_win,
            'win_pay': third_win_pay,
            'place_odds': third_place,
            'place_pay': third_place_pay,
            'pay_total': third_pay_total
        }
    ]

    if verbose >= 2:
        print("3 Runner Each Way Triple %d: %s - Win Bet: $%.2f Place Bet: $%.2f" % (first_number, first_runner, first_win_bet, first_place_bet))
        print("3 Runner Each Way Triple %d: %s - Win Bet: $%.2f Place Bet: $%.2f" % (second_number, second_runner, second_win_bet, second_place_bet))
        print("3 Runner Each Way Tripe %d: %s - Win Bet: $%.2f Place Bet: $%.2f" % (
            third_number, third_runner, third_win_bet, third_place_bet))

    return bet
