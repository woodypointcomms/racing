from datetime import *
from bs4 import BeautifulSoup
import urllib.request
import ssl
import sys
import pandas as pd
import json

from predict_race_rfc import predict_race
from calc_place_bet import calculate_place_bet
from calc_win_bet import calculate_win_bet
from calc_win_bet_2way import calculate_win_bet_2way
from get_prediction_summary import get_prediction_summary

from check_meta_first_pred_top3 import check_meta_first_pred_top3


def get_pool_size(race_details, pool_code):

    pools = race_details['Pools']
    pool_size = 0

    for pool in pools:

        if pool['PoolType'] == pool_code:

            pool_size = pool['PoolTotal']
            return pool_size

    return pool_size


def get_prediction(race_code, race_date=None):

    race, racedata, meeting = get_race(race_code, race_date)

    if race['Status'] == "ABANDONED":
        return pd.DataFrame()

    # If pool is too small don't bet
    # win_pool_size = get_pool_size(race, 'WW')
    # place_pool_size = get_pool_size(race, 'PP')
    #
    # if win_pool_size < 1000 or place_pool_size < 500:
    #     print("Not predicting on %s as win pool is %d and place pool is %d" % (race_code, win_pool_size,
    #                                                                        place_pool_size))
    #     return pd.DataFrame()

    race_date = racedata['MeetingDate']
    meeting_code = meeting['MeetingCode']
    race_number = race['RaceNumber']
    race_name = race['RaceName']
    race_distance = race['Distance']

    track_rating = race['TrackRating']
    weather = race['WeatherConditionLevel']

    # results = race['Results']
    runners = race['Runners']

    prediction_array = []

    for runner in runners:

        if runner['Scratched']:
            continue

        if runner['LateScratching']:
            continue

        runner_number = runner['RunnerNumber']
        runner_name = runner['RunnerName']

        if len(runner['LastThreeStarts']) == 3:
            last_start_1 = runner['LastThreeStarts'][0].upper()
            last_start_2 = runner['LastThreeStarts'][1].upper()
            last_start_3 = runner['LastThreeStarts'][2].upper()
        elif len(runner['LastThreeStarts']) == 2:
            last_start_1 = runner['LastThreeStarts'][0].upper()
            last_start_2 = runner['LastThreeStarts'][1].upper()
            last_start_3 = "Z"
        elif len(runner['LastThreeStarts']) == 1:
            last_start_1 = runner['LastThreeStarts'][0].upper()
            last_start_2 = "Z"
            last_start_3 = "Z"
        else:
            last_start_1 = "Z"
            last_start_2 = "Z"
            last_start_3 = "Z"

        if "C" in runner['Form']:
            form_c = "1"
        else:
            form_c = "0"

        if "D" in runner['Form']:
            form_d = "1"
        else:
            form_d = "0"

        if "T" in runner['Form']:
            form_t = "1"
        else:
            form_t = "0"

        if "W" in runner['Form']:
            form_w = "1"
        else:
            form_w = "0"

        win_odds = runner['WinOdds']
        place_odds = runner['PlaceOdds']

        last_win_odds = runner['LastWinOdds']
        last_place_odds = runner['LastPlaceOdds']

        weight = runner['Weight']
        barrier = runner['Barrier']

        row = [race_date, meeting_code, race_number, race_name, race_distance, runner_number, runner_name,
               "", "", barrier, weight, form_c, form_d, form_t, form_w, last_start_1,
               last_start_2, last_start_3, win_odds, place_odds, last_win_odds, last_place_odds, track_rating,
               weather]

        prediction_array.append(row)

    CSV_COLUMNS = ["race_date", "meeting_code", "race_number", "race_name", "race_distance", "runner_number",
                   "runner_name", "runner_position", "runner_points", "barrier", "weight", "form_c", "form_d", "form_t",
                   "form_w", "last_start_1", "last_start_2", "last_start_3", "win_odds", "place_odds", "last_win_odds",
                   "last_place_odds", "track_rating", "weather"]

    prediction_set = pd.DataFrame(prediction_array, columns=CSV_COLUMNS)
    result_prediction = predict_race(race_name, prediction_set)

    return result_prediction


def get_race(race_code, race_date = None):

    if race_date is None:

        today = date.today()

    else:

        today = datetime.strptime(race_date, '%Y-%m-%d')

    year = today.strftime('%Y')
    month = today.strftime('%-m')
    day = today.strftime('%-d')

    print("Get Race %s" % race_code)

    url = "https://api.tatts.com/tbg/vmax/web/data/racing/%s/%s/%s/%s" % (year, month, day, race_code)
    gcontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1)  # Only for gangstars

    with urllib.request.urlopen(url, context=gcontext) as response:
        jsondata = response.read()

        races = json.loads(jsondata.decode('utf-8'))

        if races['Success'] == False:
            print("Error getting race %s" % race_code)
            return pd.DataFrame()

        racedata = races['Data'][0]

        meeting = racedata['Meetings'][0]

        if meeting['Abandoned']:
            return pd.DataFrame()

        race = meeting['Races'][0]

        return race, racedata, meeting


if __name__ == '__main__':

    budget = 10

    if len(sys.argv) > 2:

        race_date = sys.argv[2]
        result = get_race(sys.argv[1], race_date)

    else:

        result = get_race(sys.argv[1])

    if not result.empty:
        print(result[['runner_number', 'prob_1']].head())

        check_meta_first_pred_top3(result)

        calculate_win_bet_2way(result, budget)
        calculate_place_bet(result, budget)
