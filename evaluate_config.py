import os
import configparser
import progressbar
import pandas as pd
import glob
import time

from multiprocessing import Pool, Queue, Value

from evaluate_date import EvaluateDate

tasks = Queue()
num_tasks = Value('i', 0)
tasks_done = Value('i', 0)

out = Queue()


def individual_config():

    while True:

        config_path, dates, test_section, test_type = tasks.get(True)

        # print("Got %s" % config_path)

        # s = time.time()

        config = configparser.ConfigParser()
        config.read(config_path)

        running_win_bet_on = 0
        running_win_profit = 0
        running_win_avg_profit = 0
        running_places_bet_on = 0
        running_place_profit = 0
        running_place_avg_profit = 0
        running_eachway_bet_on = 0
        running_eachway_profit = 0
        running_eachway_avg_profit = 0
        running_quinella_bet_on = 0
        running_quinella_profit = 0
        running_quinella_avg_profit = 0

        total_win_profit = 0
        total_place_profit = 0
        total_eachway_profit = 0
        total_quinella_profit = 0

        running_total_races = 0
        running_positive = 0
        running_negative = 0

        max_profit = 0
        min_profit = 0

        spend = 0

        for race_date in dates:

            evaldate = EvaluateDate(race_date, config=config)

            evaldate.evaluate_date(verbose=0)

            total_races = evaldate.total_races
            win_bet_on = evaldate.win_bet_on
            win_profit = evaldate.win_profit
            win_avg_profit = evaldate.win_avg_profit
            places_bet_on = evaldate.places_bet_on
            place_profit = evaldate.place_profit
            place_avg_profit = evaldate.place_avg_profit
            eachway_bet_on = evaldate.eachway_bet_on
            eachway_profit = evaldate.eachway_profit
            eachway_avg_profit = evaldate.eachway_avg_profit
            quinella_bet_on = evaldate.quinella_bet_on
            quinella_profit = evaldate.quinella_profit
            quinella_avg_profit = evaldate.quinella_avg_profit

            running_win_bet_on = running_win_bet_on + win_bet_on
            running_win_profit = running_win_profit + win_profit
            running_win_avg_profit = running_win_avg_profit + win_avg_profit

            if win_profit > 0:
                running_positive = running_positive + 1

                if win_profit > max_profit:
                    max_profit = win_profit

                if win_profit < min_profit:
                    min_profit = win_profit

            if win_profit < 0:
                running_negative = running_negative + 1

                if win_profit > max_profit:
                    max_profit = win_profit

                if win_profit < min_profit:
                    min_profit = win_profit

            running_places_bet_on = running_places_bet_on + places_bet_on
            running_place_profit = running_place_profit + place_profit
            running_place_avg_profit = running_place_avg_profit + place_avg_profit

            if place_profit > 0:
                running_positive = running_positive + 1

                if place_profit > max_profit:
                    max_profit = place_profit

                if place_profit < min_profit:
                    min_profit = place_profit

            if place_profit < 0:
                running_negative = running_negative + 1

                if place_profit > max_profit:
                    max_profit = place_profit

                if place_profit < min_profit:
                    min_profit = place_profit

            running_eachway_bet_on = running_eachway_bet_on + eachway_bet_on
            running_eachway_profit = running_eachway_profit + eachway_profit
            running_eachway_avg_profit = running_eachway_avg_profit + eachway_avg_profit

            if eachway_profit > 0:
                running_positive = running_positive + 1

                if eachway_profit > max_profit:
                    max_profit = eachway_profit

                if eachway_profit < min_profit:
                    min_profit = eachway_profit

            if eachway_profit < 0:
                running_negative = running_negative + 1

                if eachway_profit > max_profit:
                    max_profit = eachway_profit

                if eachway_profit < min_profit:
                    min_profit = eachway_profit

            running_quinella_bet_on = running_quinella_bet_on + quinella_bet_on
            running_quinella_profit = running_quinella_profit + quinella_profit
            running_quinella_avg_profit = running_quinella_avg_profit + quinella_avg_profit

            if quinella_profit > 0:
                running_positive = running_positive + 1

                if quinella_profit > max_profit:
                    max_profit = quinella_profit

                if quinella_profit < min_profit:
                    min_profit = quinella_profit

            if quinella_profit < 0:
                running_negative = running_negative + 1

                if quinella_profit > max_profit:
                    max_profit = quinella_profit

                if quinella_profit < min_profit:
                    min_profit = quinella_profit

            running_total_races = running_total_races + total_races

        running_win_avg_profit = running_win_profit / len(dates)
        running_place_avg_profit = running_place_profit / len(dates)
        running_eachway_avg_profit = running_eachway_profit / len(dates)
        running_quinella_avg_profit = running_quinella_profit / len(dates)

        if running_win_bet_on == 0:
            win_avg_profit = 0
        else:
            win_avg_profit = running_win_profit / running_win_bet_on

        if running_places_bet_on == 0:
            place_avg_profit = 0
        else:
            place_avg_profit = running_place_profit / running_places_bet_on

        if running_eachway_bet_on == 0:
            eachway_avg_profit = 0
        else:
            eachway_avg_profit = running_eachway_profit / running_eachway_bet_on

        if running_quinella_bet_on == 0:
            quinella_avg_profit = 0
        else:
            quinella_avg_profit = running_quinella_profit / running_quinella_bet_on

        win_section = [running_win_bet_on, running_win_profit, running_win_avg_profit,
                       win_avg_profit]
        place_section = [running_places_bet_on, running_place_profit, running_place_avg_profit,
                         place_avg_profit]
        eachway_section = [running_eachway_bet_on, running_eachway_profit, running_eachway_avg_profit,
                           eachway_avg_profit]
        quinella_section = [running_quinella_bet_on, running_quinella_profit, running_quinella_avg_profit,
                           quinella_avg_profit]

        row = [running_total_races, running_positive, running_negative, min_profit, max_profit]

        if test_type == "win":
            for item in win_section:
                row.append(item)

        if test_type == "place":
            for item in place_section:
                row.append(item)

        if test_type == "eachway":
            for item in eachway_section:
                row.append(item)
                
        if test_type == "quinella":
            for item in quinella_section:
                row.append(item)

        for value in config[test_section].items():
            row.append(value[1])

        # f = time.time()
        # secs = f - s

        out.put_nowait(row)
        # tasks.task_done()

        # print("Task done")


print("Starting %d workers." % os.cpu_count())

worker_pool = Pool(int(os.cpu_count()), individual_config)
# worker_pool = Pool(int(1), individual_config)


class EvaluateConfig:

    def __init__(self, test_section, test_type, dates, description=""):
        self.config_dir = "configs"

        self.test_section = test_section
        self.test_type = test_type
        self.description = description
        self.output_csv = "config_%s_%s.csv" % (self.test_section, self.description)

        self.dates = dates

        self.output_array = []

    def evaluate_config(self):

        i = 0
        num_steps = len(glob.glob1(self.config_dir, "*.ini"))

        last_file_name = ""
        for root, dirs, files in os.walk(self.config_dir):

            for file_name in files:
                config_path = "%s/%s" % (self.config_dir, file_name)

                tasks.put_nowait([config_path, self.dates, self.test_section, self.test_type])

                last_file_name = file_name

        global out

        i = 0

        with progressbar.ProgressBar(max_value=num_steps) as bar:

            bar.update(i)

            while i < num_steps:

                self.output_array.append(out.get(True))
                i += 1
                bar.update(i)

        win_section_cols = ['running_win_bet_on', 'running_win_profit', 'running_win_avg_profit',
               'win_avg_profit']
        place_section_cols = ['running_places_bet_on', 'running_place_profit', 'running_place_avg_profit',
               'place_avg_profit']
        eachway_section_cols = ['running_eachway_bet_on', 'running_eachway_profit', 'running_eachway_avg_profit',
               'eachway_avg_profit']
        quinella_section_cols = ['running_quinella_bet_on', 'running_quinella_profit', 'running_quinella_avg_profit',
               'quinella_avg_profit']

        CSV_COLUMNS = ['races', 'positive_days', 'negative_days', 'min_profit', 'max_profit']

        if self.test_type == "win":
            for col in win_section_cols:
                CSV_COLUMNS.append(col)

        if self.test_type == "place":
            for col in place_section_cols:
                CSV_COLUMNS.append(col)

        if self.test_type == "eachway":
            for col in eachway_section_cols:
                CSV_COLUMNS.append(col)

        if self.test_type == "quinella":
            for col in quinella_section_cols:
                CSV_COLUMNS.append(col)

        config = configparser.ConfigParser()
        # tpl_name = glob.glob1(self.config_dir,"*.tpl")
        config.read("%s/%s" % (self.config_dir, last_file_name))

        for key in config[self.test_section]:
            CSV_COLUMNS.append(key)

        output_df = pd.DataFrame(self.output_array, columns=CSV_COLUMNS)
        output_df.to_csv(self.output_csv, index=False)


if __name__ == "__main__":

    dates = ["2018-07-08", "2018-07-09", "2018-07-10", "2018-07-11", "2018-07-12", "2018-07-13", "2018-07-14",
             "2018-07-15", "2018-07-16", "2018-07-18",
             "2018-07-19", "2018-07-20", "2018-07-21", "2018-07-22", "2018-07-23", "2018-07-24", "2018-07-25",
             "2018-07-26",
             "2018-07-27", "2018-07-28", "2018-07-29", "2018-07-30", "2018-07-31",
             "2018-08-01", "2018-08-03", "2018-08-04", "2018-08-05"]

    test_section = "place_1way"
    test_type = "place"
    description = "5_runners"

    evaluate_config = EvaluateConfig(test_section, test_type, dates, description)
    evaluate_config.evaluate_config()
