from datetime import *
from bs4 import BeautifulSoup
import urllib.request
import ssl
from datetime import datetime

import sys
import json


def get_race_results(race_code, race_date=None):

    if race_date is None:
        race_date = date.today()
    else:
        race_date = race_date.strptime('%Y-%m-%d')

    year = race_date.strftime('%Y')
    month = race_date.strftime('%-m')
    day = race_date.strftime('%-d')

    url = "https://api.tatts.com/tbg/vmax/web/data/racing/%s/%s/%s/%s" % (year, month, day, race_code)
    gcontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1)  # Only for gangstars

    return_result = []

    with urllib.request.urlopen(url, context=gcontext) as response:
        jsondata = response.read()

        races = json.loads(jsondata.decode('utf-8'))
        racedata = races['Data'][0]

        meeting = racedata['Meetings'][0]

        race = meeting['Races'][0]

        meeting_code = meeting['MeetingCode']
        race_number = race['RaceNumber']
        race_name = race['RaceName']
        race_distance = race['Distance']

        status = race['Status']

        if status != "PAYING":

            print("Race %s%s Not paying yet!" % (meeting_code, race_number))
            return []

        runners = race['Runners']
        results = race['Results']
        pools = race['Pools']

        for result in results:

            runner_number = result['RunnerNumber']
            position = result['Position']

            #print("%s: %s" % (runner_number, position))

        for pool in pools:

            if pool['PoolType'] == "WW":

                dividends = pool['Dividends']
                dividend = dividends[0]
                amount = dividend['Amount']
                dividend_results = dividend['DividendResults']
                runner = dividend_results[0]['RunnerNumber']

                #print("Runner number %d pays $%.2f for the win" % (runner, amount))

                return_result.append([runner, amount, 0])

            if pool['PoolType'] == "PP":

                for dividend in pool['Dividends']:

                    amount = dividend['Amount']
                    dividend_results = dividend['DividendResults']
                    runner = dividend_results[0]['RunnerNumber']

                    #print("Runner number %d pays $%.2f for the place" % (runner, amount))

                    return_result.append([runner, 0, amount])

    return return_result


if __name__ == '__main__':

    get_race_results(sys.argv[1])