"""
Module implements a model for a race venue
"""

from sqlalchemy import Column, Integer, DateTime, Float, ForeignKey, String
from sqlalchemy.orm import relationship

# Components
from db.database_manager import Base


class Venue(Base):

    __tablename__ = "venues"

    id = Column(Integer, primary_key=True)
    venue_name = Column(String, nullable=False)
    meeting_type_id = Column(Integer, ForeignKey("meeting_types.id"), nullable=False)
    meeting_type = relationship("MeetingType", foreign_keys=[meeting_type_id], lazy="joined")
    latitude = Column(Float, nullable=True)
    longitude = Column(Float, nullable=True)
    altitude = Column(Integer, nullable=True)
