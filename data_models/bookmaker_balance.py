"""
Module implements a model for a bookmaking balance data agency
"""

from sqlalchemy import Column, Integer, DateTime, Float, ForeignKey
from sqlalchemy.orm import relationship

# Components
from db.database_manager import Base


class BookmakerBalance(Base):

    __tablename__ = "bookmaker_balances"

    id = Column(Integer, primary_key=True)
    bookmaker_id = Column(Integer, ForeignKey('bookmakers.id'), nullable=False)
    bookmaker = relationship("Bookmaker", foreign_keys=[bookmaker_id], lazy='joined')
    ts = Column(DateTime, nullable=False)
    balance = Column(Float, nullable=False)
