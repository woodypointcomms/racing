"""
Module implements a model for a race
"""

from sqlalchemy import Column, Integer, DateTime, Float, ForeignKey, String, Boolean
from sqlalchemy.orm import relationship

# Components
from db.database_manager import Base


class Race(Base):

    __tablename__ = "race"

    id = Column(Integer, primary_key=True)
    meeting_id = Column(Integer, ForeignKey("meeting.id"), nullable=False)
    meeting = relationship("Meeting", foreign_keys=[meeting_id])
    race_number = Column(Integer, nullable=False)
    race_name = Column(String, nullable=False)
    distance = Column(Integer, nullable=False)
    start_time = Column(DateTime, nullable=False)
    weather = Column(Integer, nullable=True)
    track_rating = Column(Integer, nullable=True)
    abandoned = Column(Boolean, nullable=True)
    race_time = Column(Float, nullable=True)
