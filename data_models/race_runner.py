"""
Module implements a model for a race runner
"""

from sqlalchemy import Column, Integer, DateTime, Float, ForeignKey, String, Boolean
from sqlalchemy.orm import relationship

# Components
from db.database_manager import Base


class RaceRunner(Base):

    __tablename__ = "race_runner"

    id = Column(Integer, primary_key=True)
    race_id = Column(Integer, ForeignKey("race.id"), nullable=False)
    race = relationship("Race", foreign_keys=[race_id], nullable=False)
    runner_number = Column(Integer, nullable=False)
    starting_position = Column(Integer, nullable=False)
    weight = Column(Float, nullable=False)
    jockey_id = Column(Integer, ForeignKey("jockeys.id"), nullable=False)
    jockey = relationship("Jockey", foreign_keys=[jockey_id])
    runner_type = Column(Integer, nullable=True)
    finish_place = Column(Integer, nullable=True)
    margin = Column(Float, nullable=True)
    age = Column(Integer, nullable=True)
