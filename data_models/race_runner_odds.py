"""
Module implements a model for a race runner odds
"""

from sqlalchemy import Column, Integer, DateTime, Float, ForeignKey, String, Boolean
from sqlalchemy.orm import relationship

# Components
from db.database_manager import Base


class RaceRunnerOdds(Base):

    __tablename__ = "race_runner_odds"

    id = Column(Integer, primary_key=True)
    bookmaker_id = Column(Integer, ForeignKey("race_runner_odds.id"), nullable=False)
    bookmaker = relationship("Bookmaker", foreign_keys=[bookmaker_id])
    race_runner_id = Column(Integer, ForeignKey("race_runner.id"), nullable=False)
    race_runner = relationship("RaceRunner", foreign_keys=[race_runner_id])
    ts = Column(DateTime, nullable=False)
    win_odds = Column(Float, nullable=False)
    place_odds = Column(Float,nullable=False)
