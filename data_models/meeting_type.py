"""
Module implements a model for meeting types
"""

from sqlalchemy import Column, Integer,String
from sqlalchemy.orm import relationship

# Components
from db.database_manager import Base


class MeetingType(Base):

    __tablename__ = "meeting_types"

    id = Column(Integer, primary_key=True)
    meeting_type = Column(String, nullable=False)
