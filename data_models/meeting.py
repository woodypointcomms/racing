"""
Module implements a model for a race meeting
"""

from sqlalchemy import Column, Integer, DateTime, Float, ForeignKey, String
from sqlalchemy.orm import relationship

# Components
from db.database_manager import Base


class Meeting(Base):

    __tablename__ = "meeting"

    id = Column(Integer, primary_key=True)
    meeting_type_id = Column(Integer, ForeignKey("meeting_types.id"), nullable=False)
    meeting_type = relationship("MeetingType", foreign_keys=[meeting_type_id], lazy="joined")
    venue_id = Column(Integer, ForeignKey("venues.id"), nullable=False)
    venue = relationship("Venue", foreign_keys=[venue_id], lazy="joined")
    meeting_date = Column(DateTime, nullable=False)
