"""
Module implements a model for a bet type
"""

from sqlalchemy import Column, Integer, DateTime, Float, ForeignKey, String
from sqlalchemy.orm import relationship

# Components
from db.database_manager import Base


class BetType(Base):

    __tablename__ = "bet_types"

    id = Column(Integer, primary_key=True)
    bet_type_name = Column(String, nullable=False)
