"""
Module implements a model for a deposit
"""

from sqlalchemy import Column, Integer, DateTime, Float, ForeignKey, String
from sqlalchemy.orm import relationship

# Components
from db.database_manager import Base


class Deposit(Base):

    __tablename__ = "deposits"

    id = Column(Integer, primary_key=True)
    bookmaker_id = Column(Integer, ForeignKey("bookmakers.id"), nullable=False)
    bookmaker = relationship("Bookmaker", foreign_keys=[bookmaker_id], lazy="joined")
    ts = Column(DateTime, nullable=False)
    amount = Column(Float, nullable=False)
    comment = Column(String, nullable=True)
