"""
Module implements a model for a jockey
"""

from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

# Components
from db.database_manager import Base


class Jockey(Base):

    __tablename__ = "jockeys"

    id = Column(Integer, primary_key=True)
    surname = Column(String, nullable=False)
    first_name = Column(String, nullable=False)
