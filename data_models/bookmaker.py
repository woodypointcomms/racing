"""
Module implements a model for a bookmaking agency
"""

from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

# Components
from db.database_manager import Base


class Bookmaker(Base):

    __tablename__ = "bookmakers"

    id = Column(Integer, primary_key=True)
    bookmaker_name = Column(String, nullable=False)
    website = Column(String, nullable=True)