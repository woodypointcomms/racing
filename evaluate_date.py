from datetime import *
from pprint import pprint
import pandas as pd
import json
import os
import sys
import configparser
from pathlib import Path
import time
import itertools

from calc_place_bet_1way import calculate_place_bet_1way
from calc_place_bet import calculate_place_bet
from calc_place_bet_3way import calculate_place_bet_3way

from calc_win_bet import calculate_win_bet
from calc_win_bet_2way import calculate_win_bet_2way
from calc_win_bet_1way import calculate_win_bet_1way

from calc_eachway_bet_1way import calculate_eachway_bet_1way
from calc_eachway_bet_2way_double import calculate_eachway_bet_2way_double
from calc_eachway_bet_3way_triple import calculate_eachway_bet_3way_triple

from calc_quinella import calculate_quinella_bet

from check_meta_winner_top2 import check_meta_winner_top2
from check_meta_winner_top3 import check_meta_winner_top3

from race_result import RaceResult
from prediction_service import PredictionService, prediction_service
from race_result_service import RaceResultService, result_service


class EvaluateDate:

    def __init__(self, race_date, config=None):

        self.race_date = race_date

        if prediction_service is None:
            self.prediction_service = PredictionService()
        else:
            self.prediction_service = prediction_service

        self.prediction_service.load_predictions_for_dates([race_date])

        if result_service is None:
            self.result_service = RaceResultService()
        else:
            self.result_service = result_service

        self.result_service.load_results_for_dates([race_date])

        if config is None:

            self.config = configparser.ConfigParser()
            self.config.read('racing.ini')

        else:

            self.config = config

        self.total_races = 0
        self.win_bet_on = 0
        self.win_profit = 0
        self.win_avg_profit = 0
        self.places_bet_on = 0
        self.place_profit = 0
        self.place_avg_profit = 0
        self.eachway_bet_on = 0
        self.eachway_profit = 0
        self.eachway_avg_profit = 0

        self.quinella_bet_on = 0
        self.quinella_profit = 0
        self.quinella_avg_profit = 0

    def evaluate_date(self, verbose=0):
        today = datetime.strptime(self.race_date, '%Y-%m-%d')

        # print(self.race_date)

        place_budget = 5
        place_max_budget = 5
        place_balance = 100
        place_starting_balance = place_balance

        win_budget = 5
        win_max_budget = 5
        win_balance = 100
        win_starting_balance = win_balance

        eachway_budget = 5
        eachway_max_budget = 5
        eachway_balance = 100
        eachway_starting_balance = eachway_balance

        quinella_budget = 5
        quinella_max_budget = 5
        quinella_balance = 100
        quinella_starting_balance = quinella_balance

        total_races = 0
        places_bet_on = 0
        win_bet_on = 0
        eachway_bet_on = 0
        quinella_bet_on = 0

        predictions = self.prediction_service.get_races(self.race_date)

        for race_code, prediction_obj in predictions.items():

            prediction = prediction_obj.get_prediction_df()

            place_profit_loss_this_race = 0
            win_profit_loss_this_race = 0
            eachway_profit_loss_this_race = 0
            quinella_profit_loss_this_race = 0

            if int(verbose) >= 3:
                print("Processing %s" % race_code)

            # Load Result and check we have a result available, if not move to next prediction
            rr = self.result_service.get_result(self.race_date, race_code)
            
            if rr is None:
                continue

            place_bet = []
            win_bet = []
            eachway_bet = []
            quinella_bet = []

            if self.config['eachway_betting'].getboolean('enable_3way_triple'):

                eachway_bet = calculate_eachway_bet_3way_triple(prediction, eachway_budget, verbose)

            if len(eachway_bet) == 0:

                if self.config['eachway_betting'].getboolean('enable_2way_double'):
                    eachway_bet = calculate_eachway_bet_2way_double(prediction, eachway_budget, verbose)

            if len(eachway_bet) == 0:

                if self.config['eachway_betting'].getboolean('enable_1way'):
                    eachway_bet = calculate_eachway_bet_1way(prediction, eachway_budget, verbose)

            if self.config['place_betting'].getboolean('enable_3way'):

                place_bet = calculate_place_bet_3way(prediction, place_budget, verbose)

            if len(place_bet) == 0:

                if self.config['place_betting'].getboolean('enable_2way'):
                    place_bet = calculate_place_bet(prediction, place_budget, verbose=verbose, config=self.config)

            if len(place_bet) == 0:

                if self.config['place_betting'].getboolean('enable_1way'):
                    place_bet = calculate_place_bet_1way(prediction, place_budget, verbose=verbose,
                                                         config=self.config)

                    num_bets = len(place_bet)

                    if num_bets > 0:
                        if verbose >= 3:
                            print("Received %d place bet options" % num_bets)

            if self.config['win_betting'].getboolean('enable_3way'):
                win_bet = calculate_win_bet(prediction, win_budget, verbose=verbose, config=self.config)

            if len(win_bet) == 0:

                if self.config['win_betting'].getboolean('enable_2way'):
                    win_bet = calculate_win_bet_2way(prediction, win_budget, verbose=verbose, config=self.config)

            if len(win_bet) == 0:

                if self.config['win_betting'].getboolean('enable_1way'):
                    win_bet = calculate_win_bet_1way(prediction, win_budget, verbose=verbose, config=self.config)

            if self.config['exotic_betting'].getboolean('enable_quinella'):
                quinella_bet = calculate_quinella_bet(prediction, quinella_budget, verbose=verbose, config=self.config)

            total_races = total_races + 1

            if len(place_bet) == 0:
                if verbose >= 3:
                    print("%s: No place bets recommended for race %s" % (race_code, race_code))
            else:

                places_bet_on = places_bet_on + 1

                # print(place_bet)

                for place_bet_item in place_bet:
                    bet = place_bet_item['bet']

                    place_balance = place_balance - bet
                    place_profit_loss_this_race = place_profit_loss_this_race - bet

                for place_bet_item in place_bet:
                    runner_number = place_bet_item['runner']
                    bet = place_bet_item['bet']

                    place_dividend = rr.get_place_dividend(runner_number)

                    paid = bet * place_dividend

                    if verbose >= 2:
                        print("%s: Place bet on runner %d for amount $%.2f paid $%.2f" % (race_code, runner_number, bet, paid))

                    place_balance = place_balance + paid
                    place_profit_loss_this_race = place_profit_loss_this_race + paid

                if verbose >= 2:
                    print("Place Balance: $%.2f" % place_balance)

                    print("Place Profit this race: $%.2f" % place_profit_loss_this_race)

            if len(win_bet) == 0:

                if verbose >= 3:
                    print("%s: No win bets recommended for race %s" % (race_code, race_code))

            else:

                win_bet_on = win_bet_on + 1

                for win_bet_item in win_bet:
                    bet = win_bet_item['bet']

                    win_balance = win_balance - bet

                    win_profit_loss_this_race = win_profit_loss_this_race - bet

                    runner_number = win_bet_item['runner']
                    bet = win_bet_item['bet']

                    win_dividend = rr.get_win_dividend(runner_number)

                    paid = bet * win_dividend

                    if verbose >= 2:
                        print("%s: Win bet on runner %d for amount $%.2f paid $%.2f" % (race_code, runner_number, bet, paid))

                    win_balance = win_balance + paid
                    win_profit_loss_this_race = win_profit_loss_this_race + paid

                if verbose >= 2:
                    print("Win Balance: $%.2f" % win_balance)

                    print("Win Profit this race: $%.2f" % win_profit_loss_this_race)

                if win_profit_loss_this_race > (-win_budget):
                    win = 1
                else:
                    win = 0

            if len(eachway_bet) == 0:

                if verbose >= 3:
                    print("%s: No each way bets recommended for race %s" % (race_code, race_code))

            else:

                eachway_bet_on = eachway_bet_on + 1

                for eachway_bet_item in eachway_bet:

                    win_bet = eachway_bet_item['win_bet']
                    place_bet = eachway_bet_item['place_bet']

                    eachway_balance = eachway_balance - win_bet - place_bet

                    eachway_profit_loss_this_race = eachway_profit_loss_this_race - win_bet - place_bet

                    runner_number = eachway_bet_item['runner']
                    win_bet = eachway_bet_item['win_bet']
                    place_bet = eachway_bet_item['place_bet']

                    win_dividend = rr.get_win_dividend(runner_number)
                    place_dividend = rr.get_place_dividend(runner_number)

                    win_paid = win_bet * win_dividend
                    place_paid = place_bet * place_dividend

                    if verbose >= 2:
                        print("%s: Each Way Win bet on runner %d for amount $%.2f paid $%.2f" % (
                        race_code, runner_number, win_bet, win_paid))
                        print("%s: Each Way Place bet on runner %d for amount $%.2f paid $%.2f" % (
                            race_code, runner_number, place_bet, place_paid))

                    eachway_balance = eachway_balance + win_paid + place_paid
                    eachway_profit_loss_this_race = eachway_profit_loss_this_race + win_paid + place_paid
                    # print("Win Profit this race: $%.2f" % win_profit_loss_this_race)

                if verbose >= 2:
                    print("Each Way Balance: $%.2f" % eachway_balance)

                    print("Each Way Profit this race: $%.2f" % eachway_profit_loss_this_race)

                if eachway_profit_loss_this_race > (-eachway_budget):
                    win = 1
                else:
                    win = 0

            if len(quinella_bet) == 0:

                if verbose >= 3:
                    print("%s: No quinella bets recommended for race %s" % (race_code, race_code))

            else:

                quinella_bet_on = quinella_bet_on + 1

                bet = quinella_bet['bet']

                quinella_balance = quinella_balance - bet

                quinella_profit_loss_this_race = quinella_profit_loss_this_race - bet

                runners = quinella_bet['runners']

                combos = len(list(itertools.combinations(runners, 2)))

                # print("Len runners %s, len combos %s" % (runners, combos))

                quinella_dividend = rr.get_quinella_dividend(runners=runners)

                if combos > 0:
                    paid = (bet * quinella_dividend) / combos
                elif combos == 1:
                    paid = bet * quinella_dividend
                else:
                    paid = 0

                if verbose >= 3:
                    print("%s: Quinella bet on runners %s for amount $%.2f paid $%.2f"
                          % (race_code, runners, bet, paid))

                quinella_balance = quinella_balance + paid
                quinella_profit_loss_this_race = quinella_profit_loss_this_race + paid

                if verbose >= 2:
                    print("Quinella Balance: $%.2f" % quinella_balance)

                    print("Quinella Profit this race: $%.2f" % quinella_profit_loss_this_race)

                if quinella_profit_loss_this_race > (-bet):
                    quinella = 1
                else:
                    quinella = 0

        if verbose >= 1:
            print("Race Date: %s" % today)
            print("Total Races: %d" % total_races)
            print("Places bet on: %d" % places_bet_on)
            print("Wins bet on: %d" % win_bet_on)
            print("Each Way bet on: %d" % eachway_bet_on)
            print("Quinellas bet on: %d" % quinella_bet_on)

        place_profit = place_balance - place_starting_balance

        if places_bet_on == 0:
            place_avg_profit = 0
            place_avg_total_profit = 0
        else:
            place_avg_profit = place_profit / places_bet_on
            place_avg_total_profit = place_profit / total_races

        if verbose >= 1:
            print("Place Profit: $%.2f" % place_profit)
            print("Place Average Profit: $%.2f" % place_avg_profit)
            print("Place Total Average Profit: $%.2f" % place_avg_total_profit)

        win_profit = win_balance - win_starting_balance

        if win_bet_on == 0:
            win_avg_profit = 0
            win_avg_total_profit = 0
        else:
            win_avg_profit = win_profit / win_bet_on
            win_avg_total_profit = win_profit / total_races

        if verbose >= 1:
            print("Win Profit: $%.2f" % win_profit)
            print("Win Average Profit: $%.2f" % win_avg_profit)
            print("Win Total Average Profit: $%.2f" % win_avg_total_profit)

        eachway_profit = eachway_balance - eachway_starting_balance

        if eachway_bet_on == 0:
            eachway_avg_profit = 0
            eachway_avg_total_profit = 0
        else:
            eachway_avg_profit = eachway_profit / eachway_bet_on
            eachway_avg_total_profit = eachway_profit / total_races

        if verbose >= 1:
            print("Eachway Profit: $%.2f" % eachway_profit)
            print("Eachway Average Profit: $%.2f" % eachway_avg_profit)
            print("Eachway Total Average Profit: $%.2f" % eachway_avg_total_profit)

        quinella_profit = quinella_balance - quinella_starting_balance

        if quinella_bet_on == 0:
            quinella_avg_profit = 0
            quinella_avg_total_profit = 0
        else:
            quinella_avg_profit = quinella_profit / quinella_bet_on
            quinella_avg_total_profit = quinella_profit / total_races

        if verbose >= 1:
            print("Quinella Profit: $%.2f" % quinella_profit)
            print("Quinella Average Profit: $%.2f" % quinella_avg_profit)
            print("Quinella Total Average Profit: $%.2f" % quinella_avg_total_profit)

        self.total_races = total_races
        self.win_bet_on = win_bet_on
        self.win_profit = win_profit
        self.win_avg_profit = win_avg_profit
        self.places_bet_on = places_bet_on
        self.place_profit = place_profit
        self.place_avg_profit = place_avg_profit
        self.eachway_bet_on = eachway_bet_on
        self.eachway_profit = eachway_profit
        self.eachway_avg_profit = eachway_avg_profit
        self.quinella_bet_on = quinella_bet_on

        # print("quinella_bet_on = %s" % quinella_bet_on)

        self.quinella_profit = quinella_profit
        self.quinella_avg_profit = quinella_avg_profit


if __name__ == "__main__":

    starttime = time.time()

    if len(sys.argv) > 1:

        race_date = sys.argv[1]

        evaluate = EvaluateDate(race_date)
        evaluate.evaluate_date(verbose=3)

    else:

        evaluate_date = date.today() - timedelta(days=1)

        evaluate = EvaluateDate(evaluate_date)
        evaluate.evaluate_date(verbose=3)

    endtime = time.time()
    executiontime = endtime - starttime

    print ("Execution time: %f seconds" % executiontime)
