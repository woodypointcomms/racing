import unittest
from datetime import datetime
import os
import csv

from bet_ledger import BetLedger


class TestBetLedger(unittest.TestCase):

    def setUp(self):

        self.ledger = BetLedger('TT')
        self.tag = "TT"

    def test_isLedgerFileCreated(self):

        today = datetime.today()
        csv_file = "accounts/%s-%s.csv" % (today.strftime('%Y-%m-%d'), self.tag)

        self.ledger.create_bet('TR1', '1', 'TT', 2.5, 9)

        # Load existing ledger if one exists
        self.assertTrue(os.path.exists(csv_file))

    def test_isBetCreated(self):

        today = datetime.today()
        csv_file = "accounts/%s-%s.csv" % (today.strftime('%Y-%m-%d'), self.tag)

        self.ledger.create_bet('TR1', '1', 'TT', 2.5, 9)

        with open(csv_file, 'r') as csv_fh:

            csv_reader = csv.reader(csv_fh)

            row_count = 0

            for row in csv_reader:

                # if row_count == 0:
                #
                #     assert row == ['Race Code','Runner Number','Type','Bet Odds','Amount','Final Odds','Paid']

                if row_count == 1:

                    self.assertListEqual(row, ['TR1', '1', 'TT', '2.5', '9', '', ''])

                row_count = row_count + 1

            self.assertEqual(row_count, 2)

    def test_isBetClosed(self):

        today = datetime.today()
        csv_file = "accounts/%s-%s.csv" % (today.strftime('%Y-%m-%d'), self.tag)

        self.ledger.create_bet('TR1', '1', 'TT', 2.5, 9)
        self.ledger.close_bet('TR1', '1', 'TT', 2, 6)

        with open(csv_file, 'r') as csv_fh:

            csv_reader = csv.reader(csv_fh)

            row_count = 0

            for row in csv_reader:

                # if row_count == 0:
                #
                #     assert row == ['Race Code','Runner Number','Type','Bet Odds','Amount','Final Odds','Paid']

                if row_count == 1:

                    self.assertListEqual(row, ['TR1', '1', 'TT', '2.5', '9', '2', '6'])

                row_count = row_count + 1

            self.assertEqual(row_count, 2)

    def tearDown(self):

        today = datetime.today()
        csv_file = "accounts/%s-%s.csv" % (today.strftime('%Y-%m-%d'), self.tag)

        # Load existing ledger if one exists
        if os.path.exists(csv_file):

            os.remove(csv_file)


if __name__ == '__main__':
    unittest.main()
