import configparser
import pandas as pd


class Prediction:

    def __init__(self, race_date="", race_code=""):

        self.race_date = race_date
        self.race_code = race_code
        self.race_name = ""

        config = configparser.ConfigParser()
        config.read('racing.ini')

        self.prediction_dir = config['directories']['prediction_dir']

        self.prediction = None

    def load_from_file(self):

        prediction_file = "%s/%s/%s.csv" % (self.prediction_dir, self.race_date, self.race_code)
        self.prediction = pd.read_csv(prediction_file, index_col=0)

    def get_prediction_df(self):

        return self.prediction

