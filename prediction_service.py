import configparser
import os
from pprint import pprint
from prediction import Prediction


class PredictionService:

    def __init__(self):

        self.predictions = {}
        self.race_dates = []

        config = configparser.ConfigParser()
        config.read('racing.ini')
        self.prediction_dir = config['directories']['prediction_dir']

    def load_predictions_for_dates(self, dates):

        for race_date in dates:

            # Don't reload the predictions for this date if we've already loaded them
            if race_date in self.race_dates:
                return

            self.predictions[race_date] = {}

            prediction_date_dir = "%s/%s" % (self.prediction_dir, race_date)

            for root, dirs, files in os.walk(prediction_date_dir):

                for file_name in files:

                    if file_name.split('.')[1] != "csv":
                        continue

                    race_code = file_name.split('.')[0]

                    current_prediction = Prediction(race_date, race_code)
                    current_prediction.load_from_file()

                    self.predictions[race_date][race_code] = current_prediction

            self.race_dates.append(race_date)

        # pprint(self.predictions)

    def get_races(self, race_date):

        # print("get_races")
        # print(self.predictions[race_date])

        return self.predictions[race_date]

    def get_race(self, race_date, race_code):

        print("prediction_service get race")

        # for race in self.predictions[race_date]:
        #
        #     if race.race_code == race_code:
        #
        #         return race

        return self.predictions[race_date][race_code]


prediction_service = PredictionService()
