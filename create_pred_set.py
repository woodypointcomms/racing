from datetime import *
from get_prediction_summary import get_prediction_summary
from pathlib import Path

from predict_race_rfc import predict_race
from race_result import RaceResult

import json
import os
import csv

import pandas as pd
import numpy as np

verbose = 2

target = 3

prediction_dir = "predictions"
new_prediction_dir = "new_predictions"
data_dir = "data"
# dates = ["2018-07-08", "2018-07-09", "2018-07-10", "2018-07-11", "2018-07-12", "2018-07-13", "2018-07-14",
#          "2018-07-15", "2018-07-16", "2018-07-18",
#          "2018-07-19", "2018-07-20", "2018-07-21", "2018-07-22", "2018-07-23", "2018-07-24", "2018-07-25", "2018-07-26",
#          "2018-07-27", "2018-07-28", "2018-07-29", "2018-07-30", "2018-07-31"]

dates = ["2018-08-01", "2018-08-03"]

CSV_COLUMNS = ["race_date", "meeting_code", "race_number", "race_name", "race_distance", "runner_number", "runner_name",
               "runner_position", "runner_points", "barrier", "weight", "form_c", "form_d", "form_t", "form_w",
               "last_start_1", "last_start_2", "last_start_3", "win_odds", "place_odds", "last_win_odds",
               "last_place_odds", "track_rating", "weather"]

output_df = pd.DataFrame()

for race_date in dates:

    prediction_date_dir = "%s/%s" % (prediction_dir, race_date)

    for root, dirs, files in os.walk(prediction_date_dir):

        for file_name in files:

            if file_name.split('.')[1] != "csv":
                continue

            file_path = os.path.join(root, file_name)
            race_code = file_name.split('.')[0]

            if verbose >= 2:
                print("Opening %s" % file_path)

            prediction = pd.read_csv(file_path, index_col=0)

            prediction_array = []

            json_path = "%s/%s/%s.json" % (data_dir, race_date, race_code)

            if not Path(json_path).is_file():
                continue

            with open(json_path, 'r') as json_file:

                response_data = json.load(json_file)
                data_set = response_data['Data'][0]
                meeting = data_set['Meetings'][0]
                race = meeting['Races'][0]

                rr = RaceResult()
                rr.init_from_race_dict(race)

                if race['Status'] == "ABANDONED":
                    continue

                meeting_code = meeting['MeetingCode']
                race_number = race['RaceNumber']
                race_name = race['RaceName']
                race_distance = race['Distance']

                track_rating = race['TrackRating']
                weather = race['WeatherConditionLevel']

                # results = race['Results']
                runners = race['Runners']

                prediction_array = []

                for runner in runners:

                    if runner['Scratched']:
                        continue

                    if runner['LateScratching']:
                        continue

                    runner_number = runner['RunnerNumber']

                    runner_set = prediction[prediction['runner_number'] == runner_number]

                    if len(runner_set.index) == 0:
                        continue

                    win_odds_1min = runner_set.iloc[0]['win_odds']
                    place_odds_1min = runner_set.iloc[0]['place_odds']

                    runner_name = runner['RunnerName']

                    if len(runner['LastThreeStarts']) == 3:
                        last_start_1 = runner['LastThreeStarts'][0].upper()
                        last_start_2 = runner['LastThreeStarts'][1].upper()
                        last_start_3 = runner['LastThreeStarts'][2].upper()
                    elif len(runner['LastThreeStarts']) == 2:
                        last_start_1 = runner['LastThreeStarts'][0].upper()
                        last_start_2 = runner['LastThreeStarts'][1].upper()
                        last_start_3 = "Z"
                    elif len(runner['LastThreeStarts']) == 1:
                        last_start_1 = runner['LastThreeStarts'][0].upper()
                        last_start_2 = "Z"
                        last_start_3 = "Z"
                    else:
                        last_start_1 = "Z"
                        last_start_2 = "Z"
                        last_start_3 = "Z"

                    if "C" in runner['Form']:
                        form_c = "1"
                    else:
                        form_c = "0"

                    if "D" in runner['Form']:
                        form_d = "1"
                    else:
                        form_d = "0"

                    if "T" in runner['Form']:
                        form_t = "1"
                    else:
                        form_t = "0"

                    if "W" in runner['Form']:
                        form_w = "1"
                    else:
                        form_w = "0"

                    win_odds = win_odds_1min
                    place_odds = place_odds_1min

                    last_win_odds = runner['LastWinOdds']
                    last_place_odds = runner['LastPlaceOdds']

                    weight = runner['Weight']
                    barrier = runner['Barrier']

                    runner_position = rr.get_runner_position(runner_number)

                    row = [race_date, meeting_code, race_number, race_name, race_distance, runner_number, runner_name,
                           runner_position, "", barrier, weight, form_c, form_d, form_t, form_w, last_start_1,
                           last_start_2, last_start_3, win_odds, place_odds, last_win_odds, last_place_odds,
                           track_rating,
                           weather]

                    prediction_array.append(row)

                prediction_set = pd.DataFrame(prediction_array, columns=CSV_COLUMNS)
                new_prediction = predict_race(race_code, prediction_set)

                new_prediction_date_dir = "%s/%s" % (new_prediction_dir,race_date)

                if not os.path.exists(new_prediction_date_dir):
                    os.makedirs(new_prediction_date_dir)

                new_prediction.to_csv("%s/%s.csv" % (new_prediction_date_dir, race_code))

                pred_summary = get_prediction_summary(new_prediction)

                new_prediction['count'] = pred_summary.iloc[0]['count']
                new_prediction['mean'] = pred_summary.iloc[0]['mean']
                new_prediction['std'] = pred_summary.iloc[0]['std']
                new_prediction['min'] = pred_summary.iloc[0]['min']
                new_prediction['25'] = pred_summary.iloc[0]['25']
                new_prediction['50'] = pred_summary.iloc[0]['50']
                new_prediction['75'] = pred_summary.iloc[0]['75']
                new_prediction['max'] = pred_summary.iloc[0]['max']

                output_df = pd.concat([output_df, new_prediction], axis=0)

output_df['target'] = output_df.apply(
    lambda x: 1 if x['runner_position'] != 0 and x['runner_position'] <= target else 0,
    axis=1)

output_df.to_csv("predictions_efc-2018-08-04.csv", index=False)
