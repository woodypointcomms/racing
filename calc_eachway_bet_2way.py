import configparser
import itertools


def calculate_eachway_bet_2way(prediction, budget, verbose=2):

    config = configparser.ConfigParser()
    config.read('racing.ini')

    if prediction.empty:
        return []

    num_runners = len(prediction.index)

    if num_runners < 5:
        if verbose >= 2:
            print("No place betting available!")
        return []

    combination_limit = int(config['eachway_2way']['combination_limit'])

    prediction_culled = prediction[(prediction['prob_1'] > float(config['eachway_2way']['minimum_prob']))]

    if len(prediction_culled.index) < 2:
        if verbose >= 2:
            print("No 2 way each way bets possible, not two runners selected!")
        return []

    if len(prediction_culled.index) < combination_limit:
        combination_limit = len(prediction_culled.index)

    bet_options = []

    combinations = list(itertools.combinations(range(0, combination_limit, 1), 2))

    for combination in combinations:

        first_three = prediction_culled.iloc[list(combination)]
        first_win = first_three.iloc[0]['win_odds']
        first_place = first_three.iloc[0]['place_odds']
        first_runner = first_three.iloc[0]['runner_name']
        first_number = first_three.iloc[0]['runner_number']
        first_total = first_win + first_place

        second_win = first_three.iloc[1]['win_odds']
        second_place = first_three.iloc[1]['place_odds']
        second_runner = first_three.iloc[1]['runner_name']
        second_number = first_three.iloc[1]['runner_number']
        second_total = second_win + second_place

        first_recip = 1 / first_total
        second_recip = 1 / second_total
        recip_total = first_recip + second_recip

        first_total_bet = round(((budget * first_recip) / recip_total), 2)
        second_total_bet = round(((budget * second_recip) / recip_total), 2)

        print("First Total Bet: %.2f Second Total Bet: %.2f" % (first_total_bet, second_total_bet))

        if first_place == 0 or second_place == 0:
            continue

        print("First Place Odds: %.2f Second Place Odds: %.2f" % (first_place, second_place))

        first_place_bet = round((budget / first_place))
        second_place_bet = round((budget / second_place))

        print("First Place Bet: %.2f Second Place Bet: %.2f" % (first_place_bet, second_place_bet))

        first_win_bet = round((first_total_bet - first_place_bet))
        second_win_bet = round((second_total_bet - second_place_bet))

        print ("First Win Bet: %.2f Second Win Bet: %.2f" % (first_win_bet, second_win_bet))

        if first_win_bet < 1 or second_win_bet < 1:
            continue

        first_win_pay = round((first_win * first_win_bet), 2)
        first_place_pay = round((first_place * first_place_bet), 2)
        first_pay_total = first_win_pay + first_place_pay
        
        second_win_pay = round((second_win * second_win_bet), 2)
        second_place_pay = round((second_place * second_place_bet), 2)
        second_pay_total = second_win_pay + second_place_pay

        print("%.2f - %.2f" % (first_pay_total, second_pay_total))

        min_win_payout = float(config['eachway_2way']['minimum_win_payout'])
        min_place_payout = float(config['eachway_2way']['minimum_place_payout'])

        if first_win_pay < (budget * min_win_payout) or first_place_pay < (budget * min_place_payout) \
                or second_win_pay < (budget * min_win_payout) or second_place_pay < (budget * min_place_payout):
            if verbose >= 2:
                print("2 Runner Each way Payout less that budget: No profitable bet possible!")

            return []

        bet = [
            {'runner': first_number,
             'win_bet': first_win_bet,
             'place_bet': first_place_bet,
             'name': first_runner,
             'win_odds': first_win,
             'win_pay': first_win_pay,
             'place_odds': first_place,
             'place_pay': first_place_pay,
             'pay_total': first_pay_total
             },
            {
                'runner': second_number,
                'win_bet': second_win_bet,
                'place_bet': second_place_bet,
                'name': second_runner,
                'win_odds': second_win,
                'win_pay': second_win_pay,
                'place_odds': second_place,
                'place_pay': second_place_pay,
                'pay_total': second_pay_total
            }
        ]

        if verbose >= 2:
            print("2 Runner Each Way %d: %s - Win Bet: $%.2f Place Bet: $%.2f" % (first_number, first_runner, first_win_bet, first_place_bet))
            print("2 Runner Each Way %d: %s - Win Bet: $%.2f Place Bet: $%.2f" % (second_number, second_runner, second_win_bet, second_place_bet))

        bet_options.append(bet)

    print(bet_options)

    if len(bet_options) > 0:

        if config['eachway_2way'].getboolean('sort_options'):
            bet_options = sorted(bet_options, key=lambda x: x[0]['pay_total'],
                                 reverse=config['eachway_2way'].getboolean('sort_reverse'))

        return bet_options[0]

    else:

        return []
