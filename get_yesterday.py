from datetime import *
import urllib.request
import ssl

import json
import os
import sys

data_dir = "data"

if len(sys.argv) > 0:

    today = datetime.strptime(sys.argv[1], '%Y-%m-%d')

else:
    today = date.today() - timedelta(days=1)

year = today.strftime('%Y')
month = today.strftime('%-m')
month_pad = today.strftime('%m')
day = today.strftime('%-d')
day_pad = today.strftime('%d')

# selected_meetings = ['BR', 'ER', 'CR', 'SR', 'OR', 'MR', 'AR', 'PR', 'TR', 'DR', 'XR', 'ZR', 'QR', 'NR', 'VR', 'CR']

url = "https://api.tatts.com/tbg/vmax/web/data/racing/%s/%s/%s/" % (year, month, day)
gcontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1)  # Only for gangstars

with urllib.request.urlopen(url, context=gcontext) as response:
    jsondata = response.read()

    raceday = json.loads(jsondata)
    racedata = raceday['Data']
    meetings = racedata[0]['Meetings']

    for meeting in meetings:

        meeting_code = meeting['MeetingCode']
        meeting_type = meeting['MeetingType']
        # meeting_date = meeting['MeetingDate']

        races = meeting['Races']

        for race in races:

            race_number = race['RaceNumber']
            race_name = race['RaceName']
            race_status = race['Status']

            print("%s%s: %s" % (meeting_code, race_number, race_name))

            if race_status != "PAYING":
                continue

            url2 = "https://api.tatts.com/tbg/vmax/web/data/racing/%s/%s/%s/%s%s" % (year, month, day, meeting_code, race_number)

            with urllib.request.urlopen(url2, context=gcontext) as response:
                jsondata2 = response.read()

                race_json = json.loads(jsondata2)

                json_path = "%s/%s-%s-%s/" % (data_dir, year, month_pad, day_pad)

                if not os.path.exists(json_path):
                    os.makedirs(json_path)

                json_file = "%s%s%s.json" % (json_path, meeting_code, race_number)

                with open(json_file, 'w') as outfile:
                    json.dump(race_json, outfile)
