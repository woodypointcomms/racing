from slack_notifier import SlackNotifier
import pandas as pd

prediction = pd.read_csv("predictions/2018-08-08/AR5.csv", index_col=0)

slack = SlackNotifier()
slack.send_prediction(prediction)
