import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pickle

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.metrics import classification_report, confusion_matrix, matthews_corrcoef

df = pd.read_csv("predictions_efc-2018-08-03.csv")

df_culled = df.drop(["race_date", "meeting_code", "race_number", "race_name", "race_distance", "runner_number", "runner_name",
               "runner_position", "runner_points", "barrier", "weight", "form_c", "form_d", "form_t", "form_w",
               "last_start_1", "last_start_2", "last_start_3", "win_odds", "place_odds", "last_win_odds",
               "last_place_odds", "track_rating", "weather", "prediction", "prob_0"], axis=1)

X_train, X_test, y_train, y_test = train_test_split(df_culled.drop('target', axis=1),
                                                    df_culled['target'], test_size=0.3, random_state=101)

rfc = ExtraTreesClassifier(class_weight='balanced')
rfc.fit(X_train, y_train)

s = pickle.dump(rfc, open("meta_runner_in_top2_rfc.p", "wb"))

predictions = rfc.predict(X_test)
predictions_prob = rfc.predict_proba(X_test)

predictions_df = pd.DataFrame(predictions, columns=['prediction'])
predictions_prob_df = pd.DataFrame(predictions_prob, columns=['prob_0', 'prob_1'])

print(predictions_prob_df.head())

print(classification_report(y_test,predictions))

print(matthews_corrcoef(y_test, predictions))

print(confusion_matrix(y_test, predictions))

print(df_culled.drop('target', axis=1).columns)
print(rfc.feature_importances_)

print(y_test.describe())

pred_plot_df = pd.concat([predictions_df, predictions_prob_df, y_test], axis=1)

print(pred_plot_df.head(30))

sns.set_style("whitegrid")
ax = sns.swarmplot('target', 'prob_1', data=pred_plot_df, hue='target', palette='rainbow')
#ax = sns.swarmplot('top4', 'prob_1', data=pred_plot_df)
ax.set(ylabel='Top 4 Probability', xlabel='Actual Position', title='Random Forest Classifier Race Prediction')
ax.set_ylim(0, 1)
plt.show()