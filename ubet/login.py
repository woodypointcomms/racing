import urllib.request
import ssl
import json
import configparser


def login():

    config = configparser.ConfigParser()
    config.read('../auth.ini')

    username = config['ubet']['username']
    password = config['ubet']['password']
    dob = config['ubet']['dob']
    device_name = config['ubet']['device_name']
    device_key = config['ubet']['device_key']

    login_request = {
        "Username": username,
        "Password": password,
        "Dob": dob,
        "DeviceName": device_name,
        "DeviceKey": device_key,
        "Referrer": "",
        "WebClientId": 0
    }

    login_json = json.dumps(login_request)
    login_json_bytes = login_json.encode('utf-8')

    url = "https://api.tatts.com/sales/vmax/web/account/login"
    gcontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1)  # Only for gangstars

    req = urllib.request.Request(url)
    req.add_header('Content-Type', 'application/json')

    req.add_header('Content-Length', len(login_json_bytes))

    with urllib.request.urlopen(req, context=gcontext, data=login_json_bytes) as response:
        json_data = response.read()

        login_data = json.loads(json_data.decode('utf-8'))
        customer_session = login_data['CustomerSession']

        return customer_session
