from login import login
from balance import get_balance
from lodge_win_bet import lodge_win_bet

import sys

customer_session = login()

balance = get_balance(sess=customer_session)

race_code = sys.argv[1]
runner_number = int(sys.argv[2])
win = float(sys.argv[3])
place = float(sys.argv[4])

print("Bet on %s, runner %d, win $%.2f, place $%.2f" % (race_code, runner_number, win, place))

print("Available balance: $%.2f" % balance)

after_bet_balance = balance - win - place

print("Balance after bet: $%.2f" % after_bet_balance)

lodge_win_bet(sess=customer_session, race_code=race_code, runner=runner_number, win=win, place=place)
