import urllib.request
import ssl
import json


def get_accounts(sess):
    """
    Gets a list of all Bank Accounts registered on UBet

    :returns list of dicts of all bank accounts from from UBet Account/Banking/Options
    """

    sess_json = json.dumps(sess)
    sess_json_bytes = sess_json.encode('utf-8')

    url = "https://api.tatts.com/sales/vmax/web/account/banking/options"
    gcontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1)  # Only for gangstars

    req = urllib.request.Request(url)
    req.add_header('Content-Type', 'application/json')

    req.add_header('Content-Length', len(sess_json_bytes))

    with urllib.request.urlopen(req, context=gcontext, data=sess_json_bytes) as response:

        json_data = response.read()

        accounts_data = json.loads(json_data.decode('utf-8'))

        accounts = accounts_data['BankAccountDetails']

        return accounts


def find_bank_account_id(bsb=0, account_number=0, account_name=""):
    """
    Finds the bank account Id number on UBet for the provided account.
    Matches on only BSB and Account Number, but not Account Name

    :returns integer of BankAccountId field from UBet Account/Banking/Options
    """

    if bsb == 0:
        print("BSB must be set!")
        return None

    if account_number == 0:
        print("Account Number must be set!")
        return None

    accounts = get_accounts(sess=session)

    for account in accounts:

        if str(account['AccountNumber']) == str(account_number) \
           and str(account['BSBNumber']) == str(bsb):
            return int(account['BankAccountId'])

    return None


# Test function for when called from command line
if __name__ == "__main__":

    from login import login
    import sys

    session = login()

    if len(sys.argv) > 1:

        bsb = sys.argv[1]
        account_number = sys.argv[2]

        account_id = find_bank_account_id(bsb=bsb, account_number=account_number)
        print("BankAccountId: %s" % account_id)

    else:

        accounts = get_accounts(sess=session)

        for account in accounts:

            print("BankAccountId: %s" % account['BankAccountId'])
            print("AccountNumber: %s" % account['AccountNumber'])
            print("BSBNumber: %s" % account['BSBNumber'])
            print("AccountName: %s" % account['AccountName'])
