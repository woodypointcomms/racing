import urllib.request
import ssl
import json


def withdrawal(sess, bank_account_id=0, amount=0):

    if bank_account_id == 0:
        print("Bank account ID not provided!")
        return False

    if amount == 0:
        print("Amount not provided")
        return False

    rounded_amount = round(amount, 2)

    if rounded_amount < 10:
        print("Minimum withdrawal amount not met")
        return False

    sess_json = json.dumps(sess)
    sess_json_bytes = sess_json.encode('utf-8')

    url = "https://api.tatts.com/sales/vmax/web/account/funds/withdraw/bank"
    gcontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1)  # Only for gangstars

    req = urllib.request.Request(url)
    req.add_header('Content-Type', 'application/json')

    req.add_header('Content-Length', len(sess_json_bytes))

    with urllib.request.urlopen(req, context=gcontext, data=sess_json_bytes) as response:

        json_data = response.read()

        accounts_data = json.loads(json_data.decode('utf-8'))

        accounts = accounts_data['BankAccountDetails']
