import urllib.request
import ssl
import json


def get_balance(sess):

    sess_json = json.dumps(sess)
    sess_json_bytes = sess_json.encode('utf-8')

    url = "https://api.tatts.com/sales/vmax/web/account/funds/balance"
    gcontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1)  # Only for gangstars

    req = urllib.request.Request(url)
    req.add_header('Content-Type', 'application/json')

    req.add_header('Content-Length', len(sess_json_bytes))

    with urllib.request.urlopen(req, context=gcontext, data=sess_json_bytes) as response:

        json_data = response.read()

        balance_data = json.loads(json_data.decode('utf-8'))

        balance = balance_data['Balance']
        shared_balance = float(balance['SharedBalance'])

        print ("Balance: $%.2f" % shared_balance)

        return shared_balance
