import urllib.request
import ssl
import json


def lodge_treble_bet(sess, race_code, runner, win=0, place=0):

    url = "https://api.tatts.com/sales/vmax/web/Account/Wagering/Tote/Bet/Sell"
    gcontext = ssl.SSLContext(ssl.PROTOCOL_TLSv1)  # Only for gangstars

    req = urllib.request.Request(url)
    req.add_header('Content-Type', 'application/json')

    meeting_code = race_code[:2]
    race_number = int(race_code[2:])

    bet = {
        "CustomerSession": sess,
        "Bets": [
            {
                "BetType": "WP",
                "MeetingCode": meeting_code,
                "IsPresale": False,
                "RaceNumber": race_number,
                "IsRover": False,
                "Selections": [
                    {
                        "Field": False,
                        "Runners": [int(runner)]
                    }
                ],
                "WinInvestment": float(win),
                "PlaceInvestment": float(place)
            }
        ]
    }

    bet_json = json.dumps(bet)
    bet_json_bytes = bet_json.encode('utf-8')

    req.add_header('Content-Length', len(bet_json_bytes))

    with urllib.request.urlopen(req, context=gcontext, data=bet_json_bytes) as response:

        json_data = response.read()

        bet_data = json.loads(json_data)

        print(bet_data)
