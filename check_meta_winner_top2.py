import pickle
import pandas as pd

from get_prediction_summary import get_prediction_summary


def check_meta_winner_top2(predictions):

    logmodel = pickle.load(open("meta_winner_first.p", "rb"))

    if len(predictions.index) < 1:
        return 0, 0

    summary = get_prediction_summary(predictions)
    meta_pred = logmodel.predict(summary)
    meta_pred_prob_1 = logmodel.predict_proba(summary)[0][1]

    return meta_pred, meta_pred_prob_1


