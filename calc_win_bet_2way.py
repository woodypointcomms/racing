import configparser
import itertools


def calculate_win_bet_2way(prediction, budget, verbose=0, config=None):

    if config is None:

        config = configparser.ConfigParser()
        config.read('racing.ini')

    if prediction.empty:
        return []

    num_runners = len(prediction.index)

    if num_runners > int(config['win_2way']['max_runners']):
        return []

    if num_runners < int(config['win_2way']['min_runners']):
        return []

    if config['win_2way'].getboolean('number_rules'):

        minimum_prob_option = "minimum_prob_%d" % num_runners
        minimum_payout_option = "minimum_payout_%d" % num_runners
        num_bets_options = "number_bets_%d" % num_runners
        combination_limit_option = "combination_limit_%d" % num_runners
        sort_options_option = "sort_options_%d" % num_runners
        sort_reverse_option = "sort_reverse_%d" % num_runners

    else:

        minimum_prob_option = "minimum_prob"
        minimum_payout_option = "minimum_payout"
        num_bets_options = "number_bets"
        combination_limit_option = "combination_limit"
        sort_options_option = "sort_options"
        sort_reverse_option = "sort_reverse"

    combination_limit = int(config['win_2way'][combination_limit_option])

    prediction_culled = prediction[(prediction['prob_1'] > float(config['win_2way'][minimum_prob_option]))]

    if len(prediction_culled.index) < 2:
        # print("No 2 way win bets possible, not two runners selected!")
        return []

    if len(prediction_culled.index) < combination_limit:
        combination_limit = len(prediction_culled.index)

    bet_options = []

    combinations = list(itertools.combinations(range(0, combination_limit, 1), 2))

    for combination in combinations:

        first_two = prediction_culled.iloc[list(combination)]

        first_win = first_two.iloc[0]['win_odds']
        second_win = first_two.iloc[1]['win_odds']

        if first_win == 0:
            continue

        if second_win == 0:
            continue

        first_runner = first_two.iloc[0]['runner_name']
        second_runner = first_two.iloc[1]['runner_name']

        first_number = first_two.iloc[0]['runner_number']
        second_number = first_two.iloc[1]['runner_number']

        first_recip = 1 / first_win
        second_recip = 1 / second_win
        recip_total = first_recip + second_recip

        first_bet = round((budget * first_recip) / recip_total)
        second_bet = round((budget * second_recip) / recip_total)

        first_pay = round((first_win * first_bet), 2)
        second_pay = round((second_win * second_bet), 2)
        pay_total = first_pay + second_pay

        min_payout = float(config['win_2way'][minimum_payout_option])

        if first_pay < (budget * min_payout) or second_pay < (budget * min_payout):
            continue

        bet = [
            {'runner': first_number,
             'bet': first_bet,
             'pay': first_pay,
             'name': first_runner,
             'win_odds': first_win,
             'pay_total': pay_total
             },
            {'runner': second_number,
             'bet': second_bet,
             'pay': second_pay,
             'name': second_runner,
             'win_odds': second_win,
             'pay_total': pay_total
             },
        ]

        if verbose >= 2:
            print("%d: %s - Win Bet: $%.2f Target: %.2f" % (first_number, first_runner, first_bet, first_pay))
            print("%d: %s - Win Bet: $%.2f Target: %.2f" % (second_number, second_runner, second_bet, second_pay))

        bet_options.append(bet)

    if len(bet_options) > 0:

        if config['win_2way'].getboolean(sort_options_option):

            bet_options = sorted(bet_options, key=lambda x: x[0]['pay_total'],
                                 reverse=config['win_2way'].getboolean(sort_reverse_option))

        return bet_options[0]

    else:

        return []
