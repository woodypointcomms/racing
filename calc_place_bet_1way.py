import configparser


def calculate_place_bet_1way(prediction, budget, verbose=2, config=None):

    if config is None:

        config = configparser.ConfigParser()
        config.read('racing.ini')

    if prediction.empty:
        return []

    # verbose = 3

    num_runners = len(prediction.index)

    if num_runners < 5:
        if verbose >= 2:
            print("No place betting available!")
        return []

    if num_runners > int(config['place_1way']['max_runners']):
        return []

    if num_runners < int(config['place_1way']['min_runners']):
        return []

    if config['place_1way'].getboolean('number_rules'):

        minimum_prob_option = "minimum_prob_%d" % num_runners
        minimum_payout_option = "minimum_payout_%d" % num_runners
        num_bets_options = "number_bets_%d" % num_runners
        combination_limit_option = "combination_limit_%d" % num_runners

    else:

        minimum_prob_option = "minimum_prob"
        minimum_payout_option = "minimum_payout"
        num_bets_options = "number_bets"
        combination_limit_option = "combination_limit"

    prediction_culled = prediction[(prediction['prob_1'] > float(config['place_1way'][minimum_prob_option]))]

    if not config['place_1way'].getboolean('ntd'):
        if num_runners < 8:
            if verbose >= 2:
                print("No betting on 2 place dividend races!")
            return []

    if len(prediction_culled.index) < 1:
        if verbose >= 2:
            print("No place bets possible, not one runners chosen!")
        return []

    bet_options = []

    combination_limit = int(config['place_1way'][combination_limit_option])

    if len(prediction_culled.index) < combination_limit:
        combination_limit = len(prediction_culled.index)

    for i in range(0, combination_limit):

        first_place = prediction_culled.iloc[i]['place_odds']

        if first_place == 0:
            continue

        first_runner = prediction_culled.iloc[i]['runner_name']
        first_number = prediction_culled.iloc[i]['runner_number']
        first_bet = budget
        first_pay = round((first_place * first_bet), 2)
        pay_total = first_pay

        # print("Considering %s with pay %.2f" % (first_runner, first_pay))

        min_payout = float(config['place_1way'][minimum_payout_option])

        if first_pay < (budget * min_payout):
            continue

        bet = {'runner': first_number,
             'bet': first_bet,
             'pay': first_pay,
             'name': first_runner,
             'place_odds': first_place,
             'pay_total': pay_total
             }

        if verbose >= 2:
            print("%d: %s - Place Bet: $%.2f Target: %.2f" % (first_number, first_runner, first_bet, first_pay))

        bet_options.append(bet)

    if len(bet_options) > 0:

        if config['place_1way'].getboolean('sort_options'):
            bet_options = sorted(bet_options, key=lambda x: x['pay_total'],
                                 reverse=config['place_1way'].getboolean('sort_reverse'))

        num_bets = int(config['place_1way'][num_bets_options])

        return bet_options[0:num_bets]

    else:

        return []

