import configparser
import itertools


def calculate_win_bet_1way(prediction, budget, verbose=0, config=None):

    if config is None:

        config = configparser.ConfigParser()
        config.read('racing.ini')

    if prediction.empty:
        return []

    num_runners = len(prediction.index)

    if num_runners > int(config['win_1way']['max_runners']):
        return []

    if num_runners < int(config['win_1way']['min_runners']):
        return []

    if config['win_1way'].getboolean('number_rules'):

        minimum_prob_option = "minimum_prob_%d" % num_runners
        minimum_payout_option = "minimum_payout_%d" % num_runners
        num_bets_options = "number_bets_%d" % num_runners
        combination_limit_option = "combination_limit_%d" % num_runners
        sort_options_option = "sort_options_%d" % num_runners
        sort_reverse_option = "sort_reverse_%d" % num_runners

    else:

        minimum_prob_option = "minimum_prob"
        minimum_payout_option = "minimum_payout"
        num_bets_options = "number_bets"
        combination_limit_option = "combination_limit"
        sort_options_option = "sort_options"
        sort_reverse_option = "sort_reverse"

    prediction_culled = prediction[(prediction['prob_1'] > float(config['win_1way'][minimum_prob_option]))]

    if len(prediction_culled.index) < 1:
        if verbose >= 2:
            print("No 1 way win bets possible, not one runners selected!")
        return []

    bet_options = []

    combination_limit = int(config['win_1way'][combination_limit_option])

    if len(prediction_culled.index) < combination_limit:
        combination_limit = len(prediction_culled.index)

    combinations = list(itertools.combinations(range(0, combination_limit, 1), 2))

    for i in range(0, combination_limit):

        first_win = prediction_culled.iloc[i]['win_odds']

        if first_win == 0:
            continue

        first_runner = prediction_culled.iloc[i]['runner_name']

        first_number = prediction_culled.iloc[i]['runner_number']

        first_bet = budget

        first_pay = round((first_win * first_bet), 2)
        pay_total = first_pay

        min_payout = float(config['win_1way'][minimum_payout_option])

        if first_pay < (budget * min_payout):
            if verbose >= 2:
                print("Payout less that budget: No profitable bet possible!")

            continue

        bet = [
            {'runner': first_number,
             'bet': first_bet,
             'pay': first_pay,
             'name': first_runner,
             'win_odds': first_win,
             'pay_total': pay_total
             }
        ]

        if verbose >= 2:
            print("%d: %s - Win Bet: $%.2f Target: %.2f" % (first_number, first_runner, first_bet, first_pay))

        bet_options.append(bet)

    if len(bet_options) > 0:

        if config['place_1way'].getboolean(sort_options_option):
            bet_options = sorted(bet_options, key=lambda x: x['pay_total'],
                                 reverse=config['place_1way'].getboolean(sort_reverse_option))

        # num_bets = int(config['win_1way'][num_bets_options])

        return bet_options[0]

    else:

        return []

