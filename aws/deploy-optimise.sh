#!/usr/bin/env bash

sudo yum -y install python36
sudo pip-3.6 install pandas
sudo pip-3.6 install progressbar2

aws s3 cp s3://df-racing-optimisation/PyRacingPrediction.zip /home/ec2-user
cd /home/ec2-user
unzip PyRacingPrediction.zip
chown -R ec2-user:ec2-user PyRacingPrediction
