import pandas as pd
import sys
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import preprocessing

df = pd.read_csv(sys.argv[1])

standardised = preprocessing.scale(df['win_odds'])
df = df.assign(win_odds_std=standardised)

sns.set_style("whitegrid")
ax = sns.swarmplot(x='runner_position', y='win_odds', data=df, hue='runner_position', palette='rainbow')
ax.set(ylabel='Win Odds', xlabel='Position', title='Finish Position vs Win Odds')
ax.set_ylim(0,10)
plt.show()
