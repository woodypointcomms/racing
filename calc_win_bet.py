import configparser
import itertools


def calculate_win_bet(prediction, budget, verbose=2, config=None):

    if config is None:

        config = configparser.ConfigParser()
        config.read('racing.ini')

    if prediction.empty:
        return []

    num_runners = len(prediction.index)

    if num_runners > int(config['win_3way']['max_runners']):
        return []

    if num_runners < int(config['win_3way']['min_runners']):
        return []

    if config['win_3way'].getboolean('number_rules'):

        minimum_prob_option = "minimum_prob_%d" % num_runners
        minimum_payout_option = "minimum_payout_%d" % num_runners
        num_bets_options = "number_bets_%d" % num_runners
        combination_limit_option = "combination_limit_%d" % num_runners
        sort_options_option = "sort_options_%d" % num_runners
        sort_reverse_option = "sort_reverse_%d" % num_runners

    else:

        minimum_prob_option = "minimum_prob"
        minimum_payout_option = "minimum_payout"
        num_bets_options = "number_bets"
        combination_limit_option = "combination_limit"
        sort_options_option = "sort_options"
        sort_reverse_option = "sort_reverse"

    combination_limit = int(config['win_3way'][combination_limit_option])

    prediction_culled = prediction[(prediction['prob_1'] > float(config['win_3way'][minimum_prob_option]))]

    if len(prediction_culled.index) < 3:
        if verbose >= 3:
            print("No 3 way win bets possible, not three runners selected!")
        return []

    if len(prediction_culled.index) < combination_limit:
        combination_limit = len(prediction_culled.index)

    bet_options = []

    combinations = list(itertools.combinations(range(0, combination_limit, 1), 3))

    for combination in combinations:

        first_three = prediction_culled.iloc[list(combination)]

        first_win = first_three.iloc[0]['win_odds']
        second_win = first_three.iloc[1]['win_odds']
        third_win = first_three.iloc[2]['win_odds']

        if first_win == 0:
            continue

        if second_win == 0:
            continue

        if third_win == 0:
            continue

        first_runner = first_three.iloc[0]['runner_name']
        second_runner = first_three.iloc[1]['runner_name']
        third_runner = first_three.iloc[2]['runner_name']

        first_number = first_three.iloc[0]['runner_number']
        second_number = first_three.iloc[1]['runner_number']
        third_number = first_three.iloc[2]['runner_number']

        first_recip = 1 / first_win
        second_recip = 1 / second_win
        third_recip = 1 / third_win
        recip_total = first_recip + second_recip + third_recip

        first_bet = round((budget * first_recip) / recip_total)
        second_bet = round((budget * second_recip) / recip_total)
        third_bet = round((budget * third_recip) / recip_total)

        first_pay = round((first_win * first_bet), 2)
        second_pay = round((second_win * second_bet), 2)
        third_pay = round((third_win * third_bet), 2)
        pay_total = first_pay + second_pay

        min_payout = float(config['win_3way'][minimum_payout_option])

        if first_pay < (budget * min_payout) or second_pay < (budget * min_payout) or third_pay < (budget * min_payout):
            if verbose >= 3:
                print("Payout less that budget: No profitable bet possible!")

            continue

        bet = [
            {'runner': first_number,
             'bet': first_bet,
             'pay': first_pay,
             'name': first_runner,
             'win_odds': first_win,
             'pay_total': pay_total
             },
            {'runner': second_number,
             'bet': second_bet,
             'pay': second_pay,
             'name': second_runner,
             'win_odds': second_win,
             'pay_total': pay_total
             },
            {'runner': third_number,
             'bet': third_bet,
             'pay': third_pay,
             'name': third_runner,
             'win_odds': third_win,
             'pay_total': pay_total
             }
        ]

        if verbose >= 2:
            print("%d: %s - Win Bet: $%.2f Target: %.2f" % (first_number, first_runner, first_bet, first_pay))
            print("%d: %s - Win Bet: $%.2f Target: %.2f" % (second_number, second_runner, second_bet, second_pay))
            print("%d: %s - Win Bet: $%.2f Target: %.2f" % (third_number, third_runner, third_bet, third_pay))

        bet_options.append(bet)

    if len(bet_options) > 0:

        if config['win_3way'].getboolean('sort_options'):
            bet_options = sorted(bet_options, key=lambda x: x[0]['pay_total'],
                                 reverse=config['win_3way'].getboolean('sort_reverse'))

        return bet_options[0]

    else:

        return []
