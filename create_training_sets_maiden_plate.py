from datetime import *

import json
import os
import csv

position_first_points = "TOP3"
position_second_points = "TOP3"
position_third_points = "TOP3"
position_fourth_points = "NONE"
position_none_points = "NONE"

data_dir = "data"

csv_writer = csv.writer(open("training_set_maiden_plate.csv", "w"))

title_row = row = ["race_date", "meeting_code", "race_number", "race_name", "race_distance", "runner_number", "runner_name", "runner_position", "runner_points", "barrier", "weight", "form_c", "form_d", "form_t", "form_w", "last_start_1", "last_start_2", "last_start_3", "win_odds", "place_odds", "last_win_odds", "last_place_odds"]
csv_writer.writerow(title_row)

for root, dirs, files in os.walk(data_dir):

    for file_name in files:

        file_path = os.path.join(root, file_name)

        with open(file_path, 'r') as json_file:

            response_data = json.load(json_file)
            data_set = response_data['Data'][0]
            meeting = data_set['Meetings'][0]
            race = meeting['Races'][0]

            race_date = data_set['MeetingDate']
            meeting_code = meeting['MeetingCode']
            race_number = race['RaceNumber']
            race_name = race['RaceName']

            if "MAIDEN PLATE" not in race_name:
                continue

            race_distance = race['Distance']

            results = race['Results']
            runners = race['Runners']

            for runner in runners:

                if runner['Scratched']:
                    continue

                if runner['LateScratching']:
                    continue

                runner_number = runner['RunnerNumber']
                runner_name = runner['RunnerName']
                runner_position = 0
                runner_points = position_none_points

                for result in results:

                    result_runner = result['RunnerNumber']
                    result_position = result['Position']

                    if runner_number == result_runner:

                        runner_position = result_position

                if runner_position == 1:
                    runner_points = position_first_points
                elif runner_position == 2:
                    runner_points = position_second_points
                elif runner_position == 3:
                    runner_points = position_third_points
                elif runner_position == 4:
                    runner_points = position_fourth_points

                if len(runner['LastThreeStarts']) == 3:
                    last_start_1 = runner['LastThreeStarts'][0].upper()
                    last_start_2 = runner['LastThreeStarts'][1].upper()
                    last_start_3 = runner['LastThreeStarts'][2].upper()
                elif len(runner['LastThreeStarts']) == 2:
                    last_start_1 = runner['LastThreeStarts'][0].upper()
                    last_start_2 = runner['LastThreeStarts'][1].upper()
                    last_start_3 = "Z"
                elif len(runner['LastThreeStarts']) == 1:
                    last_start_1 = runner['LastThreeStarts'][0].upper()
                    last_start_2 = "Z"
                    last_start_3 = "Z"
                else:
                    last_start_1 = "Z"
                    last_start_2 = "Z"
                    last_start_3 = "Z"

                if "C" in runner['Form']:
                    form_c = "T"
                else:
                    form_c = "F"

                if "D" in runner['Form']:
                    form_d = "T"
                else:
                    form_d = "F"

                if "T" in runner['Form']:
                    form_t = "T"
                else:
                    form_t = "F"

                if "W" in runner['Form']:
                    form_w = "T"
                else:
                    form_w = "F"

                win_odds = runner['WinOdds']
                place_odds = runner['PlaceOdds']

                last_win_odds = runner['LastWinOdds']
                last_place_odds = runner['LastPlaceOdds']

                weight = runner['Weight']
                barrier = runner['Barrier']

                row = [race_date, meeting_code, race_number, race_name, race_distance, runner_number, runner_name, runner_position, runner_points, barrier, weight, form_c, form_d, form_t, form_w, last_start_1, last_start_2, last_start_3, win_odds, place_odds, last_win_odds, last_place_odds]

                csv_writer.writerow(row)


                print("%s, %s%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s" % (race_date, meeting_code, race_number, race_name, race_distance, runner_number, runner_name, runner_position, runner_points, barrier, weight, form_c, form_d, form_t, form_w, last_start_1, last_start_2, last_start_3, win_odds, place_odds, last_win_odds, last_place_odds))