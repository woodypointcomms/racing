[batch]
test_section = place_1way
test_type = place
description = 8_runners

[win_1way]
combination_limit = 1
minimum_prob = 0.85
minimum_payout = 1.9
sort_options = False
sort_reverse = False

[win_2way]
combination_limit = 2
minimum_prob = 0.76
minimum_payout = 2.4
sort_options = False
sort_reverse = False

[win_3way]
combination_limit = 3
min_runners = 9
max_runners = 9
minimum_prob = 0.5
minimum_payout = 1
sort_options = False
sort_reverse = False

[win_betting]
enable_1way = False
enable_2way = False
enable_3way = False

[place_1way]
number_rules = False
minimum_prob = [[[0.5, 0.6, 0.65, 0.7, 0.8]]]
minimum_payout = [[[1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2, 2.1, 2.2, 2.3, 2.4, 2.5]]]
number_bets = 1
combination_limit = [[[1, 2, 3]]]
min_runners = 8
max_runners = 8
sort_options = False
sort_reverse = False
ntd = True

[place_2way]
minimum_prob = 0.75
minimum_payout = 0.3
ntd = False

[place_3way]
combination_limit = 2
minimum_prob = 0.7
minimum_payout = 1.9
sort_options = False
sort_reverse = False

[place_betting]
enable_1way = True
enable_2way = False
enable_3way = False

[eachway_1way]
combination_limit = 1
minimum_prob = 0.75
minimum_win_payout = 0.8
minimum_place_payout = 1.5
sort_options = False
sort_reverse = False

[eachway_2way]
combination_limit = 2
minimum_prob = 0.7
minimum_win_payout = 1.3
minimum_place_payout = 1
sort_options = False
sort_reverse = False

[eachway_2way_double]
combination_limit = 1
minimum_prob = 0.75
minimum_win_payout = 0.8
minimum_place_payout = 1.5
sort_options = False
sort_reverse = False

[eachway_3way]
combination_limit = 1
minimum_prob = 0.5
minimum_win_payout = 1.2
minimum_place_payout = 1.1
sort_options = False
sort_reverse = False

[eachway_3way_triple]
combination_limit = 1
minimum_prob = 0.76
minimum_win_payout = 0.8
minimum_place_payout = 1.2
sort_options = False
sort_reverse = False

[eachway_betting]
enable_1way = False
enable_2way = False
enable_2way_double = False
enable_3way = False
enable_3way_triple = False

[accounting]
opening_balance = 14.50
target = 1000
stoploss = 5

[directories]
prediction_dir = predictions
result_dir = data

