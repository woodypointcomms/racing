import tensorflow as tf
import pandas as pd

import csv

test_set = "test_set.csv"

CSV_COLUMNS = ["race_date", "meeting_code", "race_number", "race_name", "race_distance", "runner_number", "runner_name", "runner_position", "runner_points", "barrier", "weight", "form_c", "form_d", "form_t", "form_w", "last_start_1", "last_start_2", "last_start_3", "win_odds", "place_odds", "last_win_odds", "last_place_odds"]

def input_fn(data_file, num_epochs, shuffle):

    df_data = pd.read_csv(
        tf.gfile.Open(data_file),
        names=CSV_COLUMNS,
        skipinitialspace=True,
        engine="python",
        skiprows=1
    )

    #df_data = df_data.droprows(how="any", axis=0)

    #print("num_classes: %s" % (len(set(df_data['runner_points']))))

    return tf.estimator.inputs.pandas_input_fn(
        x=df_data,
        y=df_data['runner_points'],
        batch_size=100,
        num_epochs=num_epochs,
        shuffle=shuffle,
        num_threads=5
    )

form_c = tf.feature_column.categorical_column_with_vocabulary_list("form_c", ["T", "F"])
form_d = tf.feature_column.categorical_column_with_vocabulary_list("form_d", ["T", "F"])
form_t = tf.feature_column.categorical_column_with_vocabulary_list("form_t", ["T", "F"])
form_w = tf.feature_column.categorical_column_with_vocabulary_list("form_w", ["T", "F"])

last_start_1 = tf.feature_column.categorical_column_with_vocabulary_list("last_start_1", ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "F", "L", "P", "N", "X", "Z"])
last_start_2 = tf.feature_column.categorical_column_with_vocabulary_list("last_start_2", ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "F", "L", "P", "N", "X", "Z"])
last_start_3 = tf.feature_column.categorical_column_with_vocabulary_list("last_start_3", ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "F", "L", "P", "N", "X", "Z"])

weight = tf.feature_column.numeric_column("weight")
barrier = tf.feature_column.numeric_column("barrier")

feature_columns = [form_c, form_d, form_t, form_w, last_start_1, last_start_2, last_start_3, weight, barrier]

model_dir = "models"
labels_output = ["TOP3", "NONE"]

m = tf.estimator.LinearClassifier(model_dir=model_dir, feature_columns=feature_columns, n_classes=2, label_vocabulary=labels_output)

X_input = pd.read_csv(test_set)

y_pred = m.predict(input_fn(data_file=test_set, num_epochs=1, shuffle=False))

out_file = "predictions.csv"

in_reader = csv.reader(open(test_set, "r"))
header = next(in_reader)
out_file = open(out_file, "w")
out_writer = csv.writer(out_file)

cur_row = 0

win_accuracy = 0
num_runners = 0

for pred in y_pred:

    output_label = pred
    num_runners = num_runners + 1

    try:
        in_data = next(in_reader)
        race_number = in_data[1] + in_data[2]
        runner_number = in_data[5]
        runner_name = in_data[6]

        in_label = in_data[8]
        out_label = str(output_label['classes'][0])
        out_label = out_label.replace('b', '')
        out_label = out_label.replace("'", '')

        if in_label == out_label:
            win_accuracy = win_accuracy + 1

        out_writer.writerow([race_number, runner_number, runner_name, in_label, out_label, output_label['probabilities'][0], output_label['probabilities'][1]])

    except(StopIteration) as err:

        out_file.close()

