import configparser
import itertools


def calculate_place_bet(prediction, budget, verbose=2, config=None):

    if config is None:

        config = configparser.ConfigParser()
        config.read('racing.ini')

    if prediction.empty:
        return []

    num_runners = len(prediction.index)

    if num_runners < 5:
        if verbose >= 3:
            print("No place betting available!")
        return []

    if not config['place_2way'].getboolean('ntd'):
        if num_runners < 8:
            if verbose >= 3:
                print("No betting on 2 place dividend races!")
            return []

    if num_runners > int(config['place_2way']['max_runners']):
        return []

    if num_runners < int(config['place_2way']['min_runners']):
        return []

    if config['place_2way'].getboolean('number_rules'):

        minimum_prob_option = "minimum_prob_%d" % num_runners
        minimum_payout_option = "minimum_payout_%d" % num_runners
        num_bets_options = "number_bets_%d" % num_runners
        combination_limit_option = "combination_limit_%d" % num_runners

    else:

        minimum_prob_option = "minimum_prob"
        minimum_payout_option = "minimum_payout"
        num_bets_options = "number_bets"
        combination_limit_option = "combination_limit"

    prediction_culled = prediction[(prediction['prob_1'] > float(config['place_2way'][minimum_prob_option]))]

    if len(prediction_culled.index) < 2:
        if verbose >= 3:
            print("No place bets possible, not two runners chosen!")
        return []

    bet_options = []

    combination_limit = int(config['place_2way'][combination_limit_option])

    if len(prediction_culled.index) < combination_limit:
        combination_limit = len(prediction_culled.index)

    combinations = list(itertools.combinations(range(0, combination_limit, 1), 2))

    for combination in combinations:

        first_two = prediction_culled.iloc[list(combination)]

        first_place = first_two.iloc[0]['place_odds']
        second_place = first_two.iloc[1]['place_odds']

        if first_place == 0:
            continue

        if second_place == 0:
            continue

        first_runner = first_two.iloc[0]['runner_name']
        second_runner = first_two.iloc[1]['runner_name']

        first_number = first_two.iloc[0]['runner_number']
        second_number = first_two.iloc[1]['runner_number']

        odd_totals = first_place + second_place
        first_prop = first_place / odd_totals
        second_prop = second_place / odd_totals

        first_bet = round((budget * second_prop))
        second_bet = round((budget * first_prop))

        first_pay = round((first_place * first_bet), 2)
        second_pay = round((second_place * second_bet), 2)
        pay_total = first_pay + second_pay

        min_payout = float(config['place_2way'][minimum_payout_option])

        if first_pay < (budget * min_payout) or second_pay < (budget * min_payout):
            continue

        bet = [
            {'runner': first_number,
             'bet': first_bet,
             'pay': first_pay,
             'name': first_runner,
             'place_odds': first_place,
             'pay_total': pay_total
             },
            {'runner': second_number,
             'bet': second_bet,
             'pay': second_pay,
             'name': second_runner,
             'place_odds': second_place,
             'pay_total': pay_total
             }
        ]

        if verbose >= 2:
            print("%d: %s - Place Bet: $%.2f Target: %.2f" % (first_number, first_runner, first_bet, first_pay))
            print("%d: %s - Place Bet: $%.2f Target: %.2f" % (second_number, second_runner, second_bet, second_pay))

        bet_options.append(bet)

    #print("Total number of options: %d" % len(bet_options))

    if len(bet_options) > 0:

        if config['place_2way'].getboolean('sort_options'):
            bet_options = sorted(bet_options, key=lambda x: x[0]['pay_total'], reverse=config['place_2way'].getboolean('sort_reverse'))

        return bet_options[0]

    else:

        return []
