import pandas as pd


def get_prediction_summary(prediction):

    prob_1_series = prediction['prob_1'].describe()

    summary = pd.DataFrame([[
        prob_1_series['count'],
        prob_1_series['mean'],
        prob_1_series['std'],
        prob_1_series['min'],
        prob_1_series['25%'],
        prob_1_series['50%'],
        prob_1_series['75%'],
        prob_1_series['max']
    ]], columns=['count', 'mean', 'std', 'min', '25', '50', '75', 'max'])

    return summary
