import configparser

config = configparser.ConfigParser()

def main():

    sort_enable = [True, False]
    sort_reverse = [True, False]
    enable_1way_win = [True, False]
    enable_2way_win = [True, False]
    enable_3way_win = [True, False]

    for option1 in enable_1way_win:

        config['win_betting']['enable_1way'] = option1

        for option2 in enable_2way_win:

            config['win_betting']['enable_2way'] = option2

            for option3 in enable_3way_win:

                config['win_betting']['enable_3way'] = option3

                if option1:

                    for sort_1way_enable in sort_enable:

                        config['win_betting']['sort_enable'] = sort_1way_enable

                        if sort_1way_enable:

                            for sort_1way_reverse_enable in sort_reverse:

                                config['win_betting']['sort_enable'] = sort_1way_reverse_enable


def gen_file(section):

    minimum_probs = [0.5, 0.6, 0.65, 0.7, 0.75]
    minimum_payouts = [1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2]

    if section == "win_2way":

        combination_limit = [2, 3, 4]

    elif section == "win_3way":

        combination_limit = [3, 4]

    else:

        combination_limit = [1, 2, 3, 4]

    for prob in minimum_probs:

        for payout in minimum_payouts:

            for combo in combination_limit:

                config['section']['minimum_prob'] = prob
                config['section']['minimum_payout'] = payout
                config['section']['combination_limit'] = combo

