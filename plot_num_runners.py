import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import sys
import os
import json

pos_max = 4

selected_meetings = ['BR', 'ER', 'CR', 'SR', 'OR', 'MR', 'AR', 'PR', 'TR', 'DR', 'XR', 'ZR', 'QR', 'NR', 'VR', 'CR']

plot_data = pd.DataFrame(columns=['Number of Runners', 'Win'])

num_days = len(sys.argv) - 1
days = sys.argv[1:]

for day in days:

    print("Processing %s." % day)

    prediction_dir = "predictions/%s/" % day
    data_dir = "data/%s/" % day

    for root, dirs, files in os.walk(prediction_dir):

        for file_name in files:

            number_of_runners = 0
            places = []

            if file_name.split('.')[1] != "csv":
                continue

            race_code = file_name.split('.')[0]
            json_path = "%s/%s.json" % (data_dir, race_code)

            cur_pred = pd.read_csv(prediction_dir + file_name)

            with open(json_path, 'r') as json_file:

                response_data = json.load(json_file)
                data_set = response_data['Data'][0]
                meeting = data_set['Meetings'][0]
                race = meeting['Races'][0]

                race_name = race['RaceName']

                results = race['Results']
                runners = race['Runners']

                track_rating = race['TrackRating']
                weather = race['WeatherConditionLevel']

                for runner in runners:

                    if not runner['Scratched']:

                        number_of_runners = number_of_runners + 1

                #number_of_runners = 1

                for result in results:
                    result_runner = result['RunnerNumber']
                    result_position = result['Position']

                    if 2 > result_position > 0:

                        places.append(result_runner)

            top_4_pred = cur_pred.head(3)
            top_4_pred_list = top_4_pred['runner_number']
            num_matches = len(set(top_4_pred_list) & set(places))

            if num_matches > 0:
                win = 1
            else:
                win = 0

            plot_row = {
                "Number of Runners": number_of_runners,
                "Track Rating": track_rating,
                "Weather": weather,
                "Win": win
            }

            #if "MAIDEN" in race_name:

            i = len(plot_data.index)

            for key in plot_row.keys():
                plot_data.loc[i, key] = plot_row[key]

sns.set(style="whitegrid")

print(plot_data.head())

# Draw a nested barplot to show survival for class and sex
sns.countplot(x="Weather", hue="Win", data=plot_data, palette="muted")

plt.show()
