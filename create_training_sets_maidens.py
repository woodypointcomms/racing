from datetime import *

import json
import os
import csv

import pandas as pd

position_first_points = 1
position_second_points = 1
position_third_points = 1
position_fourth_points = 1
position_none_points = 0

data_dir = "data"

output_array = []

for root, dirs, files in os.walk(data_dir):

    for file_name in files:

        file_ext = file_name.split('.')[1]

        if file_ext != "json":
            continue

        file_path = os.path.join(root, file_name)

        print("Handling %s" % file_path)

        with open(file_path, 'r') as json_file:

            response_data = json.load(json_file)
            data_set = response_data['Data'][0]
            meeting = data_set['Meetings'][0]
            race = meeting['Races'][0]

            race_date = data_set['MeetingDate']
            meeting_code = meeting['MeetingCode']
            race_number = race['RaceNumber']
            race_name = race['RaceName']

            if "MAIDEN" not in race_name:
                continue

            if "HURDLE" in race_name:
                continue

            if "SAND" in race_name:
                continue

            if "POLY" in race_name:
                continue

            if "WEIGHT" in race_name:
                continue

            if "WFA" in race_name:
                continue

            if "PRO-RIDE" in race_name:
                continue

            race_distance = race['Distance']

            results = race['Results']
            runners = race['Runners']

            track_rating = race['TrackRating']
            weather = race['WeatherConditionLevel']

            for runner in runners:

                if runner['Scratched']:
                    continue

                if runner['LateScratching']:
                    continue

                runner_number = runner['RunnerNumber']
                runner_name = runner['RunnerName']
                runner_position = 0
                runner_points = position_none_points

                for result in results:

                    result_runner = result['RunnerNumber']
                    result_position = result['Position']

                    if runner_number == result_runner:

                        runner_position = result_position

                if runner_position == 1:
                    runner_points = position_first_points
                elif runner_position == 2:
                    runner_points = position_second_points
                elif runner_position == 3:
                    runner_points = position_third_points
                elif runner_position == 4:
                    runner_points = position_fourth_points

                if len(runner['LastThreeStarts']) == 3:
                    last_start_1 = runner['LastThreeStarts'][0].upper()
                    last_start_2 = runner['LastThreeStarts'][1].upper()
                    last_start_3 = runner['LastThreeStarts'][2].upper()
                elif len(runner['LastThreeStarts']) == 2:
                    last_start_1 = runner['LastThreeStarts'][0].upper()
                    last_start_2 = runner['LastThreeStarts'][1].upper()
                    last_start_3 = "Z"
                elif len(runner['LastThreeStarts']) == 1:
                    last_start_1 = runner['LastThreeStarts'][0].upper()
                    last_start_2 = "Z"
                    last_start_3 = "Z"
                else:
                    last_start_1 = "Z"
                    last_start_2 = "Z"
                    last_start_3 = "Z"

                if "C" in runner['Form']:
                    form_c = "1"
                else:
                    form_c = "0"

                if "D" in runner['Form']:
                    form_d = "1"
                else:
                    form_d = "0"

                if "T" in runner['Form']:
                    form_t = "1"
                else:
                    form_t = "0"

                if "W" in runner['Form']:
                    form_w = "1"
                else:
                    form_w = "0"

                win_odds = runner['WinOdds']
                place_odds = runner['PlaceOdds']

                last_win_odds = runner['LastWinOdds']
                last_place_odds = runner['LastPlaceOdds']

                weight = runner['Weight']
                barrier = runner['Barrier']

                row = [race_date, meeting_code, race_number, race_name, int(race_distance), runner_number, runner_name, runner_position, runner_points, barrier, weight, form_c, form_d, form_t, form_w, last_start_1, last_start_2, last_start_3, win_odds, place_odds, last_win_odds, last_place_odds, track_rating, weather]

                output_array.append(row)

pool_df = pd.DataFrame(output_array, columns=['race_date', 'meeting_code', 'race_number', 'race_name', 'race_distance', 'runner_number', 'runner_name', 'runner_position', 'runner_points', 'barrier', 'weight', 'form_c', 'form_d', 'form_t', 'form_w', 'last_start_1', 'last_start_2', 'last_start_3', 'win_odds', 'place_odds', 'last_win_odds', 'last_place_odds', 'track_rating', 'weather'])

dates_and_races = pool_df[['race_date', 'meeting_code', 'race_number']]
dates_and_races.drop_duplicates(inplace=True)

number_of_races = len(dates_and_races.index)
test_set_size = round(number_of_races * 0.2)

print("Training set size: %d  Test set size: %d" % ((number_of_races - test_set_size), test_set_size))

test_set_races = dates_and_races.sample(test_set_size)

test_set = pool_df.merge(test_set_races)
training_set = pd.concat([pool_df, test_set]).drop_duplicates(keep=False)

today = date.today()
year = today.strftime('%Y')
month = today.strftime('%m')
day = today.strftime('%d')

training_file = "training_set_maidens_%s-%s-%s.csv" % (year, month, day)
test_file = "test_set_maidens_%s-%s-%s.csv" % (year, month, day)

training_set.to_csv(training_file, index=False)
test_set.to_csv(test_file, index=False)

