from datetime import *
import pandas as pd
import json
import os
import sys
import configparser
import time
from pathlib import Path

from calc_place_bet_1way import calculate_place_bet_1way
from calc_place_bet import calculate_place_bet
from calc_place_bet_3way import calculate_place_bet_3way

from calc_win_bet import calculate_win_bet
from calc_win_bet_2way import calculate_win_bet_2way
from calc_win_bet_1way import calculate_win_bet_1way

from calc_eachway_bet_1way import calculate_eachway_bet_1way
from calc_eachway_bet_2way_double import calculate_eachway_bet_2way_double
from calc_eachway_bet_3way_triple import calculate_eachway_bet_3way_triple

from check_meta_winner_top2 import check_meta_winner_top2
from check_meta_winner_top3 import check_meta_winner_top3

from race_result import RaceResult


def get_pool_size(race_details, pool_code):

    pools = race_details['Pools']
    pool_size = 0

    for pool in pools:

        if pool['PoolType'] == pool_code:

            pool_size = pool['PoolTotal']
            return pool_size

    return pool_size


def main(indate, verbose=1):

    today = datetime.strptime(indate, '%Y-%m-%d')

    config = configparser.ConfigParser()
    config.read('racing.ini')

    place_budget = 5
    place_max_budget = 5
    place_balance = 100
    place_starting_balance = place_balance

    win_budget = 5
    win_max_budget = 5
    win_balance = 100
    win_starting_balance = win_balance

    eachway_budget = 5
    eachway_max_budget = 5
    eachway_balance = 100
    eachway_starting_balance = eachway_balance

    total_races = 0
    places_bet_on = 0
    win_bet_on = 0
    eachway_bet_on = 0

    plot_df = pd.DataFrame()

    year = today.strftime('%Y')
    month = today.strftime('%-m')
    month_pad = today.strftime('%m')
    day = today.strftime('%-d')
    day_pad = today.strftime('%d')

    prediction_dir = "predictions/%s-%s-%s/" % (year, month_pad, day_pad)
    data_dir = "data/%s-%s-%s/" % (year, month_pad, day_pad)

    selected_meetings = ['BR', 'ER', 'CR', 'SR', 'OR', 'MR', 'AR', 'PR', 'TR', 'DR', 'XR', 'ZR', 'QR', 'NR', 'VR', 'CR']

    cumulative_loss = 0

    first_wins = 0

    consecutive_win_losses = {}
    consecutive_place_losses = {}
    consecutive_loss_limit = 100

    for root, dirs, files in os.walk(prediction_dir):

        for file_name in sorted(files):

            place_profit_loss_this_race = 0
            win_profit_loss_this_race = 0
            eachway_profit_loss_this_race = 0

            number_of_runners = 0

            if file_name.split('.')[1] != "csv":
                continue

            file_path = os.path.join(root, file_name)
            race_code = file_name.split('.')[0]
            meeting_code = race_code[:2]

            if race_code[1] != "R":
                continue

            if verbose >= 3:
                print("Opening %s" % file_path)
            prediction = pd.read_csv(file_path, index_col=0)

            if verbose >= 3:
                print(race_code)

            json_path = "%s/%s.json" % (data_dir, race_code)

            if not Path(json_path).is_file():
                continue

            rr = RaceResult()

            with open(json_path, 'r') as json_file:

                response_data = json.load(json_file)
                data_set = response_data['Data'][0]
                meeting = data_set['Meetings'][0]
                race = meeting['Races'][0]

                # Check pool size
                win_pool = get_pool_size(race, 'WW')
                place_pool = get_pool_size(race, 'PP')

                if win_pool < 1000 or place_pool < 500:
                    continue

                rr.init_from_race_dict(race)

            place_bet = []
            win_bet = []
            eachway_bet = []

            if config['eachway_betting'].getboolean('enable_3way_triple'):

                eachway_bet = calculate_eachway_bet_3way_triple(prediction, eachway_budget, verbose)

            if len(eachway_bet) == 0:

                if config['eachway_betting'].getboolean('enable_2way_double'):
                    eachway_bet = calculate_eachway_bet_2way_double(prediction, eachway_budget, verbose)

            if len(eachway_bet) == 0:

                if config['eachway_betting'].getboolean('enable_1way'):
                    eachway_bet = calculate_eachway_bet_1way(prediction, eachway_budget, verbose)

            if config['place_betting'].getboolean('enable_3way'):

                place_bet = calculate_place_bet_3way(prediction, place_budget, verbose)

            if len(place_bet) == 0:

                if config['place_betting'].getboolean('enable_2way'):
                    place_bet = calculate_place_bet(prediction, place_budget, verbose)

            if len(place_bet) == 0:

                if config['place_betting'].getboolean('enable_1way'):
                    place_bet = calculate_place_bet_1way(prediction, place_budget, verbose)

                    num_bets = len(place_bet)

                    if num_bets > 0:
                        if verbose >= 3:
                            print("Received %d place bet options" % num_bets)

            if config['win_betting'].getboolean('enable_3way'):
                win_bet = calculate_win_bet(prediction, win_budget, verbose)

            if len(win_bet) == 0:

                if config['win_betting'].getboolean('enable_2way'):
                    win_bet = calculate_win_bet_2way(prediction, win_budget, verbose)

            if len(win_bet) == 0:

                if config['win_betting'].getboolean('enable_1way'):
                    win_bet = calculate_win_bet_1way(prediction, win_budget, verbose)

            total_races = total_races + 1

            if meeting_code in consecutive_place_losses:

                if consecutive_place_losses[meeting_code] >= consecutive_loss_limit:

                    place_bet = []
                    print("%s: Not betting because place consecutive race loss limit exceeded!" % race_code)

            if len(place_bet) == 0:
                if verbose >= 3:
                    print("%s: No place bets recommended for race %s" % (race_code, race_code))
            else:

                places_bet_on = places_bet_on + 1

                for place_bet_item in place_bet:
                    bet = place_bet_item['bet']

                    place_balance = place_balance - bet
                    place_profit_loss_this_race = place_profit_loss_this_race - bet

                for place_bet_item in place_bet:
                    runner_number = place_bet_item['runner']
                    bet = place_bet_item['bet']

                    place_dividend = rr.get_place_dividend(runner_number)

                    paid = bet * place_dividend

                    if verbose >= 2:
                        print("%s: Place bet on runner %d for amount $%.2f paid $%.2f" % (race_code, runner_number, bet, paid))

                    place_balance = place_balance + paid
                    place_profit_loss_this_race = place_profit_loss_this_race + paid

                if verbose >= 2:
                    print("Place Balance: $%.2f" % place_balance)

                    print("Place Profit this race: $%.2f" % place_profit_loss_this_race)

                print("%.2f %.2f" % (place_profit_loss_this_race, -place_budget))

                if place_profit_loss_this_race > (-place_budget):
                    place = 1

                    if meeting_code in consecutive_place_losses:

                        consecutive_place_losses[meeting_code] = 0
                        print("reset consecutive place losses")

                else:
                    place = 0

                    if meeting_code in consecutive_place_losses:

                        consecutive_place_losses[meeting_code] += 1

                    else:

                        consecutive_place_losses[meeting_code] = 1

                    print("add one to consecutive place losses")

            if meeting_code in consecutive_win_losses:

                if consecutive_win_losses[meeting_code] >= consecutive_loss_limit:

                    win_bet = []
                    print("%s: Not betting because win consecutive race loss limit exceeded!" % race_code)

            if len(win_bet) == 0:

                if verbose >= 3:
                    print("%s: No win bets recommended for race %s" % (race_code, race_code))

            else:

                win_bet_on = win_bet_on + 1

                for win_bet_item in win_bet:
                    bet = win_bet_item['bet']

                    win_balance = win_balance - bet

                    win_profit_loss_this_race = win_profit_loss_this_race - bet

                    runner_number = win_bet_item['runner']
                    bet = win_bet_item['bet']

                    win_dividend = rr.get_win_dividend(runner_number)

                    paid = bet * win_dividend

                    if verbose >= 2:
                        print("%s: Win bet on runner %d for amount $%.2f paid $%.2f" % (race_code, runner_number, bet, paid))

                    win_balance = win_balance + paid
                    win_profit_loss_this_race = win_profit_loss_this_race + paid

                if verbose >= 2:
                    print("Win Balance: $%.2f" % win_balance)

                    print("Win Profit this race: $%.2f" % win_profit_loss_this_race)

                if win_profit_loss_this_race > (-win_budget):
                    win = 1

                    if meeting_code in consecutive_win_losses:

                        consecutive_win_losses[meeting_code] = 0

                else:
                    win = 0

                    if meeting_code in consecutive_win_losses:

                        consecutive_win_losses[meeting_code] += 1

                    else:

                        consecutive_win_losses[meeting_code] = 1

            if len(eachway_bet) == 0:

                if verbose >= 3:
                    print("%s: No each way bets recommended for race %s" % (race_code, race_code))

            else:

                eachway_bet_on = eachway_bet_on + 1

                for eachway_bet_item in eachway_bet:

                    win_bet = eachway_bet_item['win_bet']
                    place_bet = eachway_bet_item['place_bet']

                    eachway_balance = eachway_balance - win_bet - place_bet

                    eachway_profit_loss_this_race = eachway_profit_loss_this_race - win_bet - place_bet

                    runner_number = eachway_bet_item['runner']
                    win_bet = eachway_bet_item['win_bet']
                    place_bet = eachway_bet_item['place_bet']

                    win_dividend = rr.get_win_dividend(runner_number)
                    place_dividend = rr.get_place_dividend(runner_number)

                    win_paid = win_bet * win_dividend
                    place_paid = place_bet * place_dividend

                    if verbose >= 2:
                        print("%s: Each Way Win bet on runner %d for amount $%.2f paid $%.2f" % (
                        race_code, runner_number, win_bet, win_paid))
                        print("%s: Each Way Place bet on runner %d for amount $%.2f paid $%.2f" % (
                            race_code, runner_number, place_bet, place_paid))

                    eachway_balance = eachway_balance + win_paid + place_paid
                    eachway_profit_loss_this_race = eachway_profit_loss_this_race + win_paid + place_paid
                    # print("Win Profit this race: $%.2f" % win_profit_loss_this_race)

                if verbose >= 2:
                    print("Each Way Balance: $%.2f" % eachway_balance)

                    print("Each Way Profit this race: $%.2f" % eachway_profit_loss_this_race)

                if eachway_profit_loss_this_race > (-eachway_budget):
                    win = 1
                else:
                    win = 0

    if verbose >= 1:
        print("Race Date: %s" % today)
        print("Total Races: %d" % total_races)
        print("Places bet on: %d" % places_bet_on)
        print("Wins bet on: %d" % win_bet_on)
        print("Each Way bet on: %d" % eachway_bet_on)

    place_profit = place_balance - place_starting_balance

    if places_bet_on == 0:
        place_avg_profit = 0
        place_avg_total_profit = 0
    else:
        place_avg_profit = place_profit / places_bet_on
        place_avg_total_profit = place_profit / total_races

    if verbose >= 1:
        print("Place Profit: $%.2f" % place_profit)
        print("Place Average Profit: $%.2f" % place_avg_profit)
        print("Place Total Average Profit: $%.2f" % place_avg_total_profit)

    win_profit = win_balance - win_starting_balance

    if win_bet_on == 0:
        win_avg_profit = 0
        win_avg_total_profit = 0
    else:
        win_avg_profit = win_profit / win_bet_on
        win_avg_total_profit = win_profit / total_races

    if verbose >= 1:
        print("Win Profit: $%.2f" % win_profit)
        print("Win Average Profit: $%.2f" % win_avg_profit)
        print("Win Total Average Profit: $%.2f" % win_avg_total_profit)

    eachway_profit = eachway_balance - eachway_starting_balance

    if eachway_bet_on == 0:
        eachway_avg_profit = 0
        eachway_avg_total_profit = 0
    else:
        eachway_avg_profit = eachway_profit / eachway_bet_on
        eachway_avg_total_profit = eachway_profit / total_races

    if verbose >= 1:
        print("Eachway Profit: $%.2f" % eachway_profit)
        print("Eachway Average Profit: $%.2f" % eachway_avg_profit)
        print("Eachway Total Average Profit: $%.2f" % eachway_avg_total_profit)

    return total_races, win_bet_on, win_profit, win_avg_profit, places_bet_on, place_profit, place_avg_profit, eachway_bet_on, eachway_profit, eachway_avg_profit


if __name__ == "__main__":

    starttime = time.time()

    if len(sys.argv) > 1:

        race_date = sys.argv[1]
        main(race_date, verbose=3)

    else:

        evaluate_date = date.today() - timedelta(days=1)

        main(evaluate_date)

    endtime = time.time()
    executiontime = endtime - starttime

    print ("Execution time: %f seconds" % executiontime)

#plot_df.to_csv("evaluate_plot.csv")
