import pandas as pd
from pathlib import Path
import json

from race_result_runner import RaceResultRunner


class RaceResult:

    def __init__(self):

        self.places = []
        self.race_code = ""
        self.race_number = 0
        self.status = ""
        self.results = {}
        self.pools = {}

    def init_from_race_dict(self, race):

        self.race_number = race['RaceNumber']
        self.status = race['Status']

        if self.status != "PAYING":
            return

        self.results = race['Results']
        self.pools = race['Pools']

        for result in self.results:

            position = result['Position']
            number = result['RunnerNumber']

            win, place = self.get_dividends_from_pools(number)

            rr = RaceResultRunner(position, number, win, place)
            self.places.append(rr)

    def init_from_result_df(self, df):

        self.status = "PAYING"

        runner_numbers = df['Runner'].unique()

        for number in runner_numbers:

            runner_df = df[df['Runner'] == number]

            win_total = runner_df['Win'].sum()
            place_total = runner_df['Place'].sum()

            runner_item = RaceResultRunner(0, number, win_total, place_total)

            self.places.append(runner_item)

    def init_from_json(self, json_path):

        if not Path(json_path).is_file():
            return False

        with open(json_path, 'r') as json_file:

            response_data = json.load(json_file)
            data_set = response_data['Data'][0]
            meeting = data_set['Meetings'][0]
            race = meeting['Races'][0]

            self.init_from_race_dict(race)

            return True

    def get_dividends_from_pools(self, number):

        win_dividend = 0
        place_dividend = 0

        for pool in self.pools:

            if pool['Status'] != "PAYING":
                continue

            if pool['PoolType'] == "WW":

                dividends = pool['Dividends']

                for dividend in dividends:

                    amount = dividend['Amount']
                    dividend_results = dividend['DividendResults']

                    for dividend_result in dividend_results:

                        dividend_number = dividend_result['RunnerNumber']

                        if dividend_number == number:

                            win_dividend = amount

            if pool['PoolType'] == "PP":

                dividends = pool['Dividends']

                for dividend in dividends:

                    amount = dividend['Amount']
                    dividend_results = dividend['DividendResults']

                    for dividend_result in dividend_results:

                        dividend_number = dividend_result['RunnerNumber']

                        if dividend_number == number:

                            place_dividend = amount

        return win_dividend, place_dividend

    def print_results(self):

        for runner in self.places:

            if runner.position == 1 or runner.dividend_win != 0:

                print("%2d Win: $%.2f Place: $%.2f" % (runner.number, runner.dividend_win, runner.dividend_place))

            else:

                print("%2d            Place: $%.2f" % (runner.number, runner.dividend_place))

    def get_places(self):

        return self.places

    def get_win_dividend(self, number):

        for runner in self.places:

            if runner.number == number:

                return runner.dividend_win

        return 0

    def get_place_dividend(self, number):

        for runner in self.places:

            if runner.number == number:
                return runner.dividend_place

        return 0

    def get_eachway_dividend(self, number):

        win = self.get_win_dividend(number)
        place = self.get_place_dividend(number)

        return win, place

    def get_runner_position(self, number):

        found = 0

        for runner in self.places:

            if runner.number == number:

                found = runner.position

        return found

    def get_quinella_dividend(self, runner_number1=0, runner_number2=0, runners=[]):

        quinella_dividend = 0

        if runner_number1 != 0 and runner_number2 != 0:

            runners = [runner_number1, runner_number2]

        for pool in self.pools:

            if pool['Status'] != "PAYING":
                continue

            if pool['PoolType'] == "QN":

                dividends = pool['Dividends']

                for dividend in dividends:

                    amount = dividend['Amount']
                    dividend_results = dividend['DividendResults']

                    correct = 0

                    for dividend_result in dividend_results:

                        dividend_number = dividend_result['RunnerNumber']

                        if dividend_number in runners:
                            correct += 1

                    if correct == 2:
                        quinella_dividend = amount

        return quinella_dividend
