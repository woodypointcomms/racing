import sys
import pandas as pd
import tensorflow as tf

import seaborn as sns

test_set_file = sys.argv[1]

CSV_COLUMNS = ["race_date", "meeting_code", "race_number", "race_name", "race_distance", "runner_number", "runner_name", "runner_position", "runner_points", "barrier", "weight", "form_c", "form_d", "form_t", "form_w", "last_start_1", "last_start_2", "last_start_3", "win_odds", "place_odds", "last_win_odds", "last_place_odds"]


def input_fn(data_file, num_epochs, shuffle):

    df_data = pd.read_csv(
        tf.gfile.Open(data_file),
        names=CSV_COLUMNS,
        skipinitialspace=True,
        engine="python",
        skiprows=1
    )

    return tf.estimator.inputs.pandas_input_fn(
        x=df_data,
        y=df_data['runner_points'],
        batch_size=100,
        num_epochs=num_epochs,
        shuffle=shuffle,
        num_threads=5
    )


form_c = tf.feature_column.categorical_column_with_vocabulary_list("form_c", ["T", "F"])
form_d = tf.feature_column.categorical_column_with_vocabulary_list("form_d", ["T", "F"])
form_t = tf.feature_column.categorical_column_with_vocabulary_list("form_t", ["T", "F"])
form_w = tf.feature_column.categorical_column_with_vocabulary_list("form_w", ["T", "F"])

last_start_1 = tf.feature_column.categorical_column_with_vocabulary_list("last_start_1", ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "F", "L", "P", "N", "X", "Z"])
last_start_2 = tf.feature_column.categorical_column_with_vocabulary_list("last_start_2", ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "F", "L", "P", "N", "X", "Z"])
last_start_3 = tf.feature_column.categorical_column_with_vocabulary_list("last_start_3", ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "F", "L", "P", "N", "X", "Z"])

weight = tf.feature_column.numeric_column("weight")
barrier = tf.feature_column.numeric_column("barrier")

feature_columns = [form_c, form_d, form_t, form_w, last_start_1, last_start_2, last_start_3, weight, barrier]

model_dir = "models/no-maiden-price"
labels_output = ["TOP4", "NONE"]

m = tf.estimator.LinearClassifier(model_dir=model_dir, feature_columns=feature_columns, n_classes=2, label_vocabulary=labels_output)

X_input = pd.read_csv(test_set_file)

y_pred = m.predict(input_fn(data_file=test_set_file, num_epochs=1, shuffle=False))

cur_row = 0
num_rows = len(X_input.index)

prediction_set = []

for pred in y_pred:

    if cur_row >= num_rows:
        break

    output_label = pred

    race_date = X_input['race_date'][cur_row]
    race_number = X_input['meeting_code'][cur_row] + str(X_input['race_number'][cur_row])
    runner_number = X_input['runner_number'][cur_row]
    runner_name = X_input['runner_name'][cur_row]

    runner_position = X_input['runner_position'][cur_row]
    runner_points = X_input['runner_points'][cur_row]
    out_label = str(output_label['classes'][0])
    out_label = out_label.replace('b', '')
    out_label = out_label.replace("'", '')

    runner_win_odds = X_input['win_odds'][cur_row]
    runner_place_odds = X_input['place_odds'][cur_row]

    prediction_set.append([race_date, race_number, runner_number, runner_name, runner_position, runner_points, out_label, output_label['probabilities'][0], output_label['probabilities'][1], runner_win_odds, runner_place_odds])

    cur_row = cur_row + 1

prediction_df = pd.DataFrame(prediction_set, columns=['race_date', 'race_number', 'runner_number', 'runner_name', 'runner_position', 'runner_points', 'out_label', 'probability_top3', 'probability_none', 'runner_win_odds', 'runner_place_odds'])
