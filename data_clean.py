import pandas as pd
import numpy as np
import sys

if len(sys.argv) < 2:
    print("Requires CSV File as argument")
    exit()

data_file = sys.argv[1]

df = pd.read_csv(data_file)

dates_and_races = df[['race_date', 'meeting_code', 'race_number']]
dates_and_races.drop_duplicates(inplace=True)

number_of_races = 0
races_dropped = 0

for index, race in dates_and_races.iterrows():

    number_of_races = number_of_races + 1

    race_date = race['race_date']
    meeting_code = race['meeting_code']
    race_number = race['race_number']

    print("Race Date: %s Race Number: %s%s" % (race_date, meeting_code, race_number))

    runners = df[((df['race_date'] == race_date) & (df['meeting_code'] == meeting_code) & (df['race_number'] == race_number))]

    median_win_odds = runners['win_odds'].median()
    median_place_odds = runners['place_odds'].median()

    print("Median Win Odds: %.2f" % median_win_odds)
    print("Median Place Odds: %.2f" % median_place_odds)

    mean_win_odds = runners['win_odds'].mean()
    mean_place_odds = runners['place_odds'].mean()

    print("Mean Win Odds: %.2f" % mean_win_odds)
    print("Mean Place Odds: %.2f" % mean_place_odds)

    describe_win = runners['win_odds'].describe(include = [np.number])
    print(describe_win)

    describe_place = runners['place_odds'].describe(include = [np.number])
    print(describe_place)

    places = runners[runners['runner_position'] > 0]

    drop_race = False

    for places_index, places_row in places.iterrows():

        if places_row['place_odds'] > mean_place_odds:

            drop_race = True

    if drop_race:

        races_dropped = races_dropped + 1


print("Total Races in Set: %d" % number_of_races)
print("Number of Races Dropped: %d" % races_dropped)
