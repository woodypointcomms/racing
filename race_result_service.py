import configparser
import os

from race_result import RaceResult
from pprint import pprint


class RaceResultService:

    def __init__(self):

        self.results = {}
        self.race_dates = []

        config = configparser.ConfigParser()
        config.read('racing.ini')
        self.result_dir = config['directories']['result_dir']

    def load_results_for_dates(self, dates):

        for race_date in dates:

            # Don't reload the results for this date if we've already loaded them
            if race_date in self.race_dates:
                return

            self.results[race_date] = {}

            result_date_dir = "%s/%s" % (self.result_dir, race_date)

            for root, dirs, files in os.walk(result_date_dir):

                for file_name in files:

                    if file_name.split('.')[1] != "json":
                        continue

                    json_path = "%s/%s" % (result_date_dir, file_name)

                    race_code = file_name.split('.')[0]

                    current_result = RaceResult()
                    current_result.init_from_json(json_path)

                    self.results[race_date][race_code] = current_result

            self.race_dates.append(race_date)

        # pprint(self.results)

    def get_result(self, race_date, race_code):

        if race_date in self.results:
            if race_code in self.results[race_date]:
                return self.results[race_date][race_code]
            else:
                return None
        else:
            return None


result_service = RaceResultService()
