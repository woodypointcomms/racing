import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pickle

from sklearn.model_selection import train_test_split
from sklearn import svm
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import matthews_corrcoef

df = pd.read_csv("meta_set.csv")

df_culled = df.drop(['winner_first', 'winner_top2', 'winner_top4', 'first_pred_top2', 'count_top2_top2',
         'first_pred_top3', 'count_top3_top2',
         'count_top2_top3', 'count_top3_top3'], axis=1)


X_train, X_test, y_train, y_test = train_test_split(df_culled.drop(['winner_top3'], axis=1),
                                                    df_culled['winner_top3'], test_size=0.3, random_state=101)

logmodel = svm.SVC(C=2000, probability=True, kernel='rbf')
logmodel.fit(X_train, y_train)

s = pickle.dump(logmodel, open("meta_winner_top3_svm.p", "wb"))

predictions = logmodel.predict(X_test)
predictions_prob = logmodel.predict_proba(X_test)

#print(y_test)

#print(predictions)

#print(predictions_prob)

X_out = X_test

X_out['winner_top3'] = y_test
X_out['prediction'] = pd.Series(predictions, index=X_test.index)
X_out['prob_1'] = pd.Series(predictions_prob[:,1], index=X_test.index)

print(classification_report(y_test,predictions))

print(confusion_matrix(y_test, predictions))

print(matthews_corrcoef(y_test, predictions))

X_above = X_out[X_out['prob_1'] > 0.68]

X_above_pred = X_above['prediction']

X_above_test = X_above['winner_top3']

print(classification_report(X_above_test,X_above_pred))

print(confusion_matrix(X_above_test, X_above_pred))

print(matthews_corrcoef(X_above_test, X_above_pred))


#print(logmodel.coef_)

# sns.set_style("whitegrid")
# ax = sns.swarmplot('winner_top3', 'prob_1', data=X_out, hue='prediction', palette='rainbow')
# ax.set(ylabel='Prediction', xlabel='Actual', title='OMFGWTFBBQ')
# ax.set_ylim(.2, .8)
# plt.show()
