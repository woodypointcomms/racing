import configparser
import itertools
import math


def calculate_eachway_bet_2way_double(prediction, budget, verbose=2):

    config = configparser.ConfigParser()
    config.read('racing.ini')

    if prediction.empty:
        return []

    num_runners = len(prediction.index)

    if num_runners < 5:
        if verbose >= 2:
            print("No place betting available!")
        return []

    combination_limit = int(config['eachway_2way_double']['combination_limit'])

    prediction_culled = prediction[(prediction['prob_1'] > float(config['eachway_2way_double']['minimum_prob']))]

    if len(prediction_culled.index) < 2:
        if verbose >= 2:
            print("No 2 way each way double bets possible, not two runners selected!")
        return []

    first_three = prediction_culled.head(3)
    first_win = first_three.iloc[0]['win_odds']
    first_place = first_three.iloc[0]['place_odds']
    first_runner = first_three.iloc[0]['runner_name']
    first_number = first_three.iloc[0]['runner_number']
    first_total = first_win + first_place

    second_win = first_three.iloc[1]['win_odds']
    second_place = first_three.iloc[1]['place_odds']
    second_runner = first_three.iloc[1]['runner_name']
    second_number = first_three.iloc[1]['runner_number']
    second_total = second_win + second_place

    #print("First win odds: %.2f First Place odds: %.2f" % (first_win, first_place))
    #print("Second win odds: %.2f Second Place odds: %.2f" % (second_win, second_place))

    if first_place == 0 or second_place == 0:
        if verbose >= 2:
            print("First or second place odds are 0")
        return []

    first_place_bet = math.ceil((budget * float(config['eachway_2way_double']['minimum_place_payout'])) / first_place)
    first_win_bet = math.floor(budget - first_place_bet)

    second_place_bet = math.ceil((budget * float(config['eachway_2way_double']['minimum_place_payout'])) / second_place)
    second_win_bet = math.floor(budget - second_place_bet)

    budget = 5
    first_win_bet = 1
    second_win_bet = 1

    first_place_bet = 4
    second_place_bet = 4

    # if first_win_bet < 1 or second_win_bet < 1:
    #     continue

    first_win_pay = round((first_win * first_win_bet), 2)
    first_place_pay = round((first_place * first_place_bet), 2)
    first_pay_total = first_win_pay + first_place_pay

    second_win_pay = round((second_win * second_win_bet), 2)
    second_place_pay = round((second_place * second_place_bet), 2)
    second_pay_total = second_win_pay + second_place_pay

    #print("First win pay: %.2f Second win pay: %.2f" % (first_win_pay, second_win_pay))
    #print("First place pay: %.2f Second place pay: %.2f" % (first_place_pay, second_place_pay))

    min_win_payout = float(config['eachway_2way_double']['minimum_win_payout'])
    min_place_payout = float(config['eachway_2way_double']['minimum_place_payout'])

    if first_win_pay < (budget * min_win_payout) or first_place_pay < (budget * min_place_payout) \
            or second_win_pay < (budget * min_win_payout) or second_place_pay < (budget * min_place_payout):
        if verbose >= 2:
            print("2 Runner Each way double Payout less that budget: No profitable bet possible!")

        return []

    bet = [
        {'runner': first_number,
         'win_bet': first_win_bet,
         'place_bet': first_place_bet,
         'name': first_runner,
         'win_odds': first_win,
         'win_pay': first_win_pay,
         'place_odds': first_place,
         'place_pay': first_place_pay,
         'pay_total': first_pay_total
         },
        {
            'runner': second_number,
            'win_bet': second_win_bet,
            'place_bet': second_place_bet,
            'name': second_runner,
            'win_odds': second_win,
            'win_pay': second_win_pay,
            'place_odds': second_place,
            'place_pay': second_place_pay,
            'pay_total': second_pay_total
        }
    ]

    if verbose >= 2:
        print("2 Runner Each Way Double %d: %s - Win Bet: $%.2f Place Bet: $%.2f" % (first_number, first_runner, first_win_bet, first_place_bet))
        print("2 Runner Each Way Double %d: %s - Win Bet: $%.2f Place Bet: $%.2f" % (second_number, second_runner, second_win_bet, second_place_bet))

    return bet
