import json
import requests
import threading

class SlackNotifier:

    def __init__(self):
        self.url = "https://hooks.slack.com/services/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"

    def send_text(self, text):

        slack_data = {'text': text}

        response = requests.post(
            self.url, data=json.dumps(slack_data),
            headers={'Content-Type': 'application/json'}
        )

        if response.status_code != 200:
            raise ValueError(
                'Request to slack returned an error %s, the response is:\n%s' % (response.status_code, response.text))

    def send_prediction(self, prediction, race_time=""):

        meeting_code = prediction.iloc[0]['meeting_code']
        race_number = prediction.iloc[0]['race_number']

        fallback = "Race Prediction for %s%s at %s" % (meeting_code, race_number, race_time)
        title = fallback
        text = fallback

        fields = []

        prediction = prediction[(prediction['prob_1'] > 0.5)]
        ordinal = 1

        for index,row in prediction.iterrows():

            runner_number = row['runner_number']
            runner_name = row['runner_name']
            runner_prob = row['prob_1']
            runner_win = row['win_odds']
            runner_place = row['place_odds']

            field_title = "Predicted %d" % ordinal
            field_value = "%.2f - %2d: %s (%.2f, %.2f)" % (runner_prob, runner_number, runner_name, runner_win, runner_place)

            field = {
                "title": field_title,
                "value": field_value,
                "short": True
            }

            fields.append(field)

            ordinal = ordinal + 1

        color = 'good'

        attachment = {
            "author": "Racing Daemon",
            "fallback": fallback,
            "title": title,
            "title_link": "http://www.ubet.com",
            "color": color,
            "fields": fields,
            "text": text
        }

        response = requests.post(
            self.url, data=json.dumps(attachment),
            headers={'Content-Type': 'application/json'}
        )

        if response.status_code != 200:
            raise ValueError(
                'Request to slack returned an error %s, the response is:\n%s' % (response.status_code, response.text))

