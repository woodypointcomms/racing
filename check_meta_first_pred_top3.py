import pickle
import pandas as pd

from get_prediction_summary import get_prediction_summary


def check_meta_first_pred_top3(predictions):

    if len(predictions.index) > 1:

        logmodel = pickle.load(open("meta_first_pred_top3.p", "rb"))
        summary = get_prediction_summary(predictions)
        meta_pred = logmodel.predict(summary)

        return meta_pred[0]

    else:

        return 0
