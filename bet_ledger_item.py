class BetLedgerItem:

    def __init__(self, race_code, runner_number, type, bet_odds, amount):

        self.race_code = race_code
        self.runner_number = runner_number
        self.type = type
        self.bet_odds = bet_odds
        self.amount = amount
        self.final_odds = 0
        self.paid = 0

    def close_bet(self, final_odds, paid):

        self.final_odds = final_odds
        self.paid = paid
