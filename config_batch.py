import os
from shutil import copyfile, rmtree
import preconfig
import configparser
import glob

from evaluate_config import EvaluateConfig
from prediction_service import prediction_service
from race_result_service import result_service

class ConfigBatch:

    def __init__(self):

        self.config_templates_dir = "config_templates"
        self.configs_dir = "configs"

    def batchrun(self, race_dates):

        prediction_service.load_predictions_for_dates(race_dates)
        result_service.load_results_for_dates(race_dates)

        num_files = len(glob.glob1(self.config_templates_dir, "*.tpl"))
        files_done = 1

        for root, dirs, files in os.walk(self.config_templates_dir):

            for file in files:

                print("Processing %s(%d/%d)" % (file, files_done, num_files))
                files_done += 1

                # Remove config files from the configs dir
                rmtree(self.configs_dir)
                os.mkdir(self.configs_dir)

                copyfile("%s/%s" % (self.config_templates_dir, file), "%s/%s" % (self.configs_dir, file))

                preconfig.parse(template=("%s/%s" % (self.configs_dir, file)), destination=self.configs_dir)

                os.unlink("%s/%s" % (self.configs_dir, file))

                config = configparser.ConfigParser()
                config.read("%s/%s" % (self.config_templates_dir, file))

                test_section = config['batch']['test_section']
                test_type = config['batch']['test_type']
                description = config['batch']['description']

                ec = EvaluateConfig(test_section, test_type, race_dates, description)
                ec.evaluate_config()


if __name__ == "__main__":

    dates = ["2018-07-08", "2018-07-09", "2018-07-10", "2018-07-11", "2018-07-12", "2018-07-13",
             "2018-07-14", "2018-07-15", "2018-07-16", "2018-07-18", "2018-07-19", "2018-07-20",
             "2018-07-21", "2018-07-22", "2018-07-23", "2018-07-24", "2018-07-25", "2018-07-26",
             "2018-07-27", "2018-07-28", "2018-07-29", "2018-07-30", "2018-07-31", "2018-08-01",
             "2018-08-03", "2018-08-04", "2018-08-05", "2018-08-06", "2018-08-07", "2018-08-08",
             "2018-08-09", "2018-08-10", "2018-08-11", "2018-08-12", "2018-08-13", "2018-08-14",
             "2018-08-15", "2018-08-16", "2018-08-17", "2018-08-18", "2018-08-19", "2018-08-20",
             "2018-08-21", "2018-08-22", "2018-08-23", "2018-08-24", "2018-08-25", "2018-08-26",
             "2018-08-27", "2018-08-28", "2018-08-29", "2018-08-30", "2018-09-01", "2018-09-02",
             "2018-09-03", "2018-09-04", "2018-09-05", "2018-09-06", "2018-09-07", "2018-09-08",
             "2018-09-09", "2018-09-10", "2018-09-11", "2018-09-12", "2018-09-13", "2018-09-14",
             "2018-09-15"]

    cb = ConfigBatch()
    cb.batchrun(dates)
