import configparser
import itertools
import math


def calculate_eachway_bet_1way(prediction, budget, verbose=2):

    config = configparser.ConfigParser()
    config.read('racing.ini')

    if prediction.empty:
        return []

    num_runners = len(prediction.index)

    if num_runners < 5:
        if verbose >= 3:
            print("No place betting available!")
        return []

    combination_limit = int(config['eachway_1way']['combination_limit'])

    prediction_culled = prediction[(prediction['prob_1'] > float(config['eachway_1way']['minimum_prob']))]

    if len(prediction_culled.index) < 1:
        if verbose >= 3:
            print("No 1 way each way bets possible, not one runners selected!")
        return []

    if len(prediction_culled.index) < combination_limit:
        combination_limit = len(prediction_culled.index)

    bet_options = []

    combinations = list(itertools.combinations(range(0, combination_limit, 1), 1))

    for combination in combinations:

        first_three = prediction_culled.iloc[list(combination)]
        first_win = first_three.iloc[0]['win_odds']
        first_place = first_three.iloc[0]['place_odds']
        first_runner = first_three.iloc[0]['runner_name']
        first_number = first_three.iloc[0]['runner_number']
        first_total = first_win + first_place

        place_bet = math.ceil((budget * float(config['eachway_1way']['minimum_place_payout'])) / first_place)
        win_bet = math.floor(budget - place_bet)

        budget = 5
        place_bet = 4
        win_bet = 1

        first_win_pay = round((first_win * win_bet), 2)
        first_place_pay = round((first_place * place_bet), 2)
        pay_total = first_win_pay + first_place_pay

        min_win_payout = float(config['eachway_1way']['minimum_win_payout'])
        min_place_payout = float(config['eachway_1way']['minimum_place_payout'])

        if first_win_pay < (budget * min_win_payout) or first_place_pay < (budget * min_place_payout):
            if verbose >= 3:
                print("Payout less that budget: No profitable bet possible!")

            return []

        bet = [
            {'runner': first_number,
             'win_bet': win_bet,
             'place_bet': place_bet,
             'name': first_runner,
             'win_odds': first_win,
             'win_pay': first_win_pay,
             'place_odds': first_place,
             'place_pay': first_place_pay,
             'pay_total': pay_total
             }
        ]

        if verbose >= 2:
            print("%d: %s - Win Bet: $%.2f Place Bet: $%.2f" % (first_number, first_runner, win_bet, place_bet))

        bet_options.append(bet)

    if len(bet_options) > 0:

        if config['eachway_1way'].getboolean('sort_options'):
            bet_options = sorted(bet_options, key=lambda x: x[0]['pay_total'],
                                 reverse=config['eachway_1way'].getboolean('sort_reverse'))

        return bet_options[0]

    else:

        return []
