import tensorflow as tf
import pandas as pd

import tempfile

training_set = "training_set_maiden_plate.csv"
test_set = "test_set_maiden_plate.csv"

CSV_COLUMNS = ["race_date", "meeting_code", "race_number", "race_name", "race_distance", "runner_number", "runner_name", "runner_position", "runner_points", "barrier", "weight", "form_c", "form_d", "form_t", "form_w", "last_start_1", "last_start_2", "last_start_3", "win_odds", "place_odds", "last_win_odds", "last_place_odds"]

def input_fn(data_file, num_epochs, shuffle):

    df_data = pd.read_csv(
        tf.gfile.Open(data_file),
        names=CSV_COLUMNS,
        skipinitialspace=True,
        engine="python",
        skiprows=1
    )

    #df_data = df_data.droprows(how="any", axis=0)

    print("num_classes: %s" % (len(set(df_data['runner_points']))))

    return tf.estimator.inputs.pandas_input_fn(
        x=df_data,
        y=df_data['runner_points'],
        batch_size=100,
        num_epochs=num_epochs,
        shuffle=shuffle,
        num_threads=5
    )

form_c = tf.feature_column.categorical_column_with_vocabulary_list("form_c", ["T", "F"])
form_d = tf.feature_column.categorical_column_with_vocabulary_list("form_d", ["T", "F"])
form_t = tf.feature_column.categorical_column_with_vocabulary_list("form_t", ["T", "F"])
form_w = tf.feature_column.categorical_column_with_vocabulary_list("form_w", ["T", "F"])

last_start_1 = tf.feature_column.categorical_column_with_vocabulary_list("last_start_1", ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "F", "L", "P", "N", "X", "Z"])
last_start_2 = tf.feature_column.categorical_column_with_vocabulary_list("last_start_2", ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "F", "L", "P", "N", "X", "Z"])
last_start_3 = tf.feature_column.categorical_column_with_vocabulary_list("last_start_3", ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "F", "L", "P", "N", "X", "Z"])

weight = tf.feature_column.numeric_column("weight")
barrier = tf.feature_column.numeric_column("barrier")

win = tf.feature_column.numeric_column("win_odds")
place = tf.feature_column.numeric_column("place_odds")

feature_columns = [form_c, form_d, form_t, form_w, last_start_1, last_start_2, last_start_3, weight, barrier, win, place]

model_dir = "models/maiden-plate-price"

labels_output = ["TOP3", "NONE"]

m = tf.estimator.LinearClassifier(model_dir=model_dir, feature_columns=feature_columns, n_classes=2, label_vocabulary=labels_output)

m.train(input_fn(data_file=training_set, num_epochs=1, shuffle=True), steps=20)

results = m.evaluate(
    input_fn=input_fn(test_set, num_epochs=1, shuffle=True),
    steps=None)

print("model directory = %s" % model_dir)

for key in sorted(results):
    print("%s: %s" % (key, results[key]))


