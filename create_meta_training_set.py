from datetime import *
from get_prediction_summary import get_prediction_summary
from pathlib import Path

from race_result import RaceResult

import json
import os
import csv

import pandas as pd
import numpy as np

verbose = 2

prediction_dir = "predictions"
data_dir = "data"
dates = ["2018-07-08", "2018-07-09", "2018-07-10", "2018-07-11", "2018-07-12", "2018-07-13", "2018-07-14",
         "2018-07-15", "2018-07-16", "2018-07-18",
         "2018-07-19", "2018-07-20", "2018-07-21", "2018-07-22", "2018-07-23", "2018-07-24", "2018-07-25", "2018-07-26",
         "2018-07-27", "2018-07-28", "2018-07-29", "2018-07-30", "2018-07-31",
         "2018-08-01", "2018-08-03", "2018-08-04", "2018-08-05"]
output_array = []

for race_date in dates:

    prediction_date_dir = "%s/%s" % (prediction_dir, race_date)

    for root, dirs, files in os.walk(prediction_date_dir):

        for file_name in files:

            if file_name.split('.')[1] != "csv":
                continue

            file_path = os.path.join(root, file_name)
            race_code = file_name.split('.')[0]

            if verbose >= 2:
                print("Opening %s" % file_path)

            prediction = pd.read_csv(file_path)

            # Don't look at predictions with less than 5 runners
            if len(prediction.index) < 5:
                continue

            if 'prob_1' in prediction.columns:

                summary = prediction['prob_1'].describe()

                json_path = "%s/%s/%s.json" % (data_dir, race_date, race_code)

                rr = RaceResult()
                if rr.init_from_json(json_path):

                    # Number of runners in top 2 predictions that are place 1 or 2
                    count_top2_top2 = 0

                    # Number of runners in top 2 predictions that are place 1, 2 or 3
                    count_top2_top3 = 0

                    # Number of runners in top 3 predictions that are place 1 or 2
                    count_top3_top2 = 0

                    # Number of runners in top 3 predictions that are place 1, 2 or 3
                    count_top3_top3 = 0

                    # Winner counts
                    winner_first = 0
                    winner_top2 = 0
                    winner_top3 = 0
                    winner_top4 = 0

                    first_pred_top2 = 0
                    first_pred_top3 = 0

                    if len(prediction.index) > 1:
                        first_pred_actual = rr.get_runner_position(prediction.iloc[0]['runner_number'])
                    else:
                        first_pred_actual = 0

                    if len(prediction.index) > 2:
                        second_pred_actual = rr.get_runner_position(prediction.iloc[1]['runner_number'])
                    else:
                        second_pred_actual = 0

                    if len(prediction.index) > 3:
                        third_pred_actual = rr.get_runner_position(prediction.iloc[2]['runner_number'])
                    else:
                        third_pred_actual = 0

                    if len(prediction.index) > 4:
                        fourth_pred_actual = rr.get_runner_position(prediction.iloc[3]['runner_number'])
                    else:
                        fourth_pred_actual = 0

                    if first_pred_actual == 1:
                        winner_first = 1

                    if first_pred_actual == 1 or second_pred_actual == 1:
                        winner_top2 = 1

                    if first_pred_actual == 1 or second_pred_actual == 1 or third_pred_actual == 1:
                        winner_top3 = 1

                    if first_pred_actual == 1 or second_pred_actual == 1 or third_pred_actual == 1 or fourth_pred_actual == 1:
                        winner_top4 = 1

                    if first_pred_actual <= 3 and first_pred_actual != 0:
                        first_pred_top3 = 1

                    if first_pred_actual <= 2 and first_pred_actual != 0:
                        first_pred_top2 = 1
                        print("First pred is greater than or equal to 2")
                        count_top2_top2 = count_top2_top2 + 1

                    if second_pred_actual <= 2 and second_pred_actual != 0:

                        print("Second pred is greater than or equal to 2")

                        count_top2_top2 = count_top2_top2 + 1

                    print("Count top 2 value %d" % count_top2_top2)

                    row = [
                        summary['count'],
                        summary['mean'],
                        summary['std'],
                        summary['min'],
                        summary['25%'],
                        summary['50%'],
                        summary['75%'],
                        summary['max'],
                        winner_first,
                        winner_top2,
                        winner_top3,
                        winner_top4,
                        first_pred_top2,
                        count_top2_top2,
                        count_top3_top2,
                        first_pred_top3,
                        count_top2_top3,
                        count_top3_top3
                    ]

                    output_array.append(row)

output_df = pd.DataFrame(output_array, columns=['count', 'mean', 'std', 'min', '25', '50', '75', 'max', 'winner_first',
                                                'winner_top2', 'winner_top3', 'winner_top4', 'first_pred_top2',
                                                'count_top2_top2', 'count_top3_top2', 'first_pred_top3',
                                                'count_top2_top3', 'count_top3_top3'])

output_df.to_csv("meta_set.csv", index=False)

