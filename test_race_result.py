import unittest
import json
import pandas as pd
from race_result import RaceResult


class MyTestCase(unittest.TestCase):

    def test_win_3places(self):

        json_path = "data/2018-07-21/BR5.json"

        with open(json_path, 'r') as json_file:
            response_data = json.load(json_file)
            data_set = response_data['Data'][0]
            meeting = data_set['Meetings'][0]
            race = meeting['Races'][0]

            rr = RaceResult()
            rr.init_from_race_dict(race)

            # Winner
            self.assertEqual(rr.get_win_dividend(3), 4.7)
            self.assertEqual(rr.get_place_dividend(3), 1.9)

            # 2nd place
            self.assertEqual(rr.get_win_dividend(18), 0)
            self.assertEqual(rr.get_place_dividend(18), 3.8)

            # 3rd place
            self.assertEqual(rr.get_win_dividend(9), 0)
            self.assertEqual(rr.get_place_dividend(9), 1.5)

            # 4th place
            self.assertEqual(rr.get_win_dividend(13), 0)
            self.assertEqual(rr.get_place_dividend(13), 0)

            # Non-placer
            self.assertEqual(rr.get_win_dividend(2), 0)
            self.assertEqual(rr.get_place_dividend(2), 0)


    def test_load_df(self):

        csv_path = "results/2018-07-22/AR3.csv"

        results_df = pd.read_csv(csv_path)

        rr = RaceResult()
        rr.init_from_result_df(results_df)

        # rr.print_results()

        self.assertEqual(rr.get_win_dividend(2), 2.5)
        self.assertEqual(rr.get_place_dividend(2), 1.4)

        self.assertEqual(rr.get_win_dividend(3), 0)
        self.assertEqual(rr.get_place_dividend(3), 1.4)

        self.assertEqual(rr.get_win_dividend(8), 0)
        self.assertEqual(rr.get_place_dividend(8), 7.9)


if __name__ == '__main__':
    unittest.main()
